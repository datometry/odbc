#!/usr/bin/env python

from string import Template
import os

def render(file_name, render_hash):
    fin = open(file_name)
    src = Template(fin.read())
    txt = src.substitute(render_hash)

    out_name = '.'.join(file_name.split('.')[:-1])
    fout = open(out_name,"w+")
    fout.write(txt)
    fout.close()


build_number = os.getenv('BUILD_NUMBER',"0")
fversion = open('VERSION')
simple_version = fversion.read().strip()
version_n_build = '.'.join([simple_version, build_number])
comma_version = version_n_build.replace('.', ',')
sub_dict = {'DTMODBC_VERSION': version_n_build,
            'COMMA_DTMODBC_VERSION': comma_version}

render('version.h.template', sub_dict)
render('configure.ac.template', sub_dict)
