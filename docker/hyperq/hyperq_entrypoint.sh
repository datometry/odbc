#!/usr/bin/env bash

# This file is copied in to the Hyper-Q container image and runs automatically
# when the Hyper-Q service is started.

# This script will attempt to install Hyper-Q from an RPM from a specific branch.
# If that a hyper-q RPM is not found matching that branch name, it will default
# to installing from the latest master build.

set -ex

function get_pkg {
  pkg=$1
  branch="master"
  if [ ! $(ls -1 *.rpm | perl -lne 'print if /hyperq-\d+\.\d+\.\d+-\d+\.x86_64\.rpm/') ]; then
    branch=$(ls -1 *.rpm | \
      perl -lne 'print $1 if /hyperq-(.*)-\d+\.\d+\.\d+-\d+\.x86_64\.rpm/' | \
      perl -e '@_=sort{length($a) <=> length($b)}<>; print "$_[0]"'
    )
  fi
  if [ "${branch}" = "master" ]; then
    br=""
  else
    br="${branch}-"
  fi
  ver=$(ls -1 *.rpm | perl -lne 'print $1 if /hyperq-'${br}'(\d+\.\d+\.\d+)-\d+\.x86_64\.rpm/')
  build=$(ls -1 *.rpm | perl -lne 'print $1 if /hyperq-'${br}''${ver}'-(\d+)\.x86_64\.rpm/')
  if [ "$pkg" = "main" ]; then
    echo "hyperq-${br}${ver}-${build}.x86_64.rpm"
  else
    echo "hyperq-${br}${pkg}-${ver}-${build}.x86_64.rpm"
  fi
}

cd rpm_hyperq
echo "###### Installing Hyper-Q from RPM ######"
ACCEPT_DTM_LICENSE=y yum -y install $(get_pkg main) $(get_pkg license)
cd ..

echo "###### Create link for odbcinst.ini ######"
[ -f "/opt/datometry/config/odbcinst.ini" ] && mv /opt/datometry/config/odbcinst.ini /opt/datometry/config/odbcinst.ini.$(date +%s)
ln -fs /etc/odbcinst.ini /opt/datometry/config/odbcinst.ini

if [ -n "${WAIT_FOR_HOST}" ] && [ -n "${WAIT_FOR_PORT}" ]; then
  echo "###### Waiting for ${WAIT_FOR_HOST} to come up ######"
  wait-for-it.sh -h ${WAIT_FOR_HOST} -p ${WAIT_FOR_PORT} -s -- echo "${WAIT_FOR_HOST} available on port ${WAIT_FOR_PORT}"
fi

echo "###### Starting Hyper-Q ######"
/opt/datometry/dtm/bin/dtm start --foreground --pidfile=/opt/datometry/dtm.pid --logdir=/opt/datometry/config/logs --confdir=/opt/datometry/config --log debug -t 11110
