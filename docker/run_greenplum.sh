#!/bin/bash

# This script is used to start Greenplum and is mounted inside the greenplum
# container using docker compose.

set -x


start_gpdb() {
  su gpadmin -l -c  'export MASTER_DATA_DIRECTORY=/gpdata/master/gpseg-1
  source /usr/local/greenplum-db/greenplum_path.sh
  gpstart -a --verbose'
}

stop_gpdb() {
  su gpadmin -l -c  'export MASTER_DATA_DIRECTORY=/gpdata/master/gpseg-1
  source /usr/local/greenplum-db/greenplum_path.sh
  gpstop -m'
}

echo "127.0.0.1 $(cat ~/orig_hostname)" >> /etc/hosts
service sshd start
su gpadmin -l -c 'echo "host all all 0.0.0.0/0 md5" >> /gpdata/master/gpseg-1/pg_hba.conf'
start_gpdb

output=$(su gpadmin -l -c 'source /usr/local/greenplum-db/greenplum_path.sh
  psql -h greenplum -p 5432 -c "\l" -U gpadmin template1' 2>&1)

while [ $? -ne 0 ]; do
  sleep 1;
  if [[ "${output}" == *"started in master-only utility mode"* ]]; then
    stop_gpdb; sleep 5; start_gpdb;
  fi
  output=$(su gpadmin -l -c 'source /usr/local/greenplum-db/greenplum_path.sh
    psql -h greenplum -p 5432 -c "\l" -U gpadmin template1' 2>&1)
done

sleep 2678400
