
-- test MD store tables

-- CREATE DATABASE gpadmin;
-- GRANT ALL PRIVILEGES ON DATABASE gpadmin TO gpadmin;

\c template1

CREATE USER tduser WITH PASSWORD 'tduser';
CREATE DATABASE tduser;
ALTER DATABASE tduser OWNER TO tduser;

create user gpadmin_with_password with password 'clients helms austrian gear';
alter user gpadmin_with_password with superuser;
create user password_be with password 'password' ;
create user md5_be with password 'md5';
create user qmdi;
create user gp_mdi;
create user quser;
grant password_be to quser;
grant password_be to tduser;

create schema dtm_schema;
GRANT ALL ON SCHEMA dtm_schema TO PUBLIC;

create database qdb;

\c tduser
create schema "__DTM_MDSTORE";
GRANT ALL ON SCHEMA "__DTM_MDSTORE" TO PUBLIC;
create table "__DTM_MDSTORE"."__DTM_MDSTORE_TEST_TABLE"(
  schemaname varchar(256),
  objectname varchar(256),
  colname varchar(256),
  propname varchar(256),
  propvalue varchar(64000),
  seqno int);

GRANT ALL ON TABLE "__DTM_MDSTORE"."__DTM_MDSTORE_TEST_TABLE" TO tduser;


-- Do this last since the wait_for_postgres.sh script attempts to login with
-- gpadmin on template1 as an indication of Postgres init completion
\c template1
alter user gpadmin with superuser;
create user ready_user;
