/*--------
 * Module:			info.c
 *
 * Description:		This module contains routines related to
 *					ODBC informational functions.
 *
 * Classes:			n/a
 *
 * API functions:	SQLGetInfo, SQLGetTypeInfo, SQLGetFunctions,
 *					SQLTables, SQLColumns, SQLStatistics, SQLSpecialColumns,
 *					SQLPrimaryKeys, SQLForeignKeys,
 *					SQLProcedureColumns, SQLProcedures,
 *					SQLTablePrivileges, SQLColumnPrivileges(NI)
 *
 * Comments:		See "readme.txt" for copyright and license information.
 *--------
 */

#include "psqlodbc.h"
#include "unicode_support.h"

#include <string.h>
#include <stdio.h>

#ifndef WIN32
#include <ctype.h>
#endif

#include "tuple.h"
#include "pgtypes.h"
#include "dlg_specific.h"

#include "environ.h"
#include "connection.h"
#include "statement.h"
#include "qresult.h"
#include "bind.h"
#include "misc.h"
#include "pgtypes.h"
#include "pgapifunc.h"
#include "multibyte.h"
#include "catfunc.h"

/*	Trigger related stuff for SQLForeign Keys */
#define TRIGGER_SHIFT 3
#define TRIGGER_MASK   0x03
#define TRIGGER_DELETE 0x01
#define TRIGGER_UPDATE 0x02

#define NULL_TO_EMPTY_STRING(sz) ((sz) == NULL ? (char *)"" : (char *)(sz))

RETCODE		SQL_API
PGAPI_GetInfo(HDBC hdbc,
			  SQLUSMALLINT fInfoType,
			  PTR rgbInfoValue,
			  SQLSMALLINT cbInfoValueMax,
			  SQLSMALLINT * pcbInfoValue)
{
	CSTR func = "PGAPI_GetInfo";
	ConnectionClass *conn = (ConnectionClass *) hdbc;
	ConnInfo   *ci;
	const char   *p = NULL;
	char		tmp[MAX_INFO_STRING];
	SQLULEN			len = 0,
				value = 0;
	RETCODE		ret = SQL_ERROR;
	char		odbcver[16];

	MYLOG(0, "entering...fInfoType=%d\n", fInfoType);

	if (!conn)
	{
		CC_log_error(func, NULL_STRING, NULL);
		return SQL_INVALID_HANDLE;
	}

	ci = &(conn->connInfo);

	switch (fInfoType)
	{
		case SQL_ACCESSIBLE_PROCEDURES: /* ODBC 1.0 */
			p = "N";
			break;

		case SQL_ACCESSIBLE_TABLES:		/* ODBC 1.0 */
			p = CC_accessible_only(conn) ? "Y" : "N";
			break;

		case SQL_MAX_DRIVER_CONNECTIONS:	/* ODBC 1.0 */
			// Renamed from SQL_ACTIVE_CONNECTIONS in ODBC 3.0
			len = 2;
			value = 0;
			break;

		case SQL_MAX_CONCURRENT_ACTIVITIES:		/* ODBC 1.0 */
			// Renamed from SQL_ACTIVE_STATEMENTS in ODBC 3.0
			len = 2;
			value = 0;
			break;

		case SQL_ALTER_TABLE:	/* ODBC 2.0 */
			len = 4;
			value = SQL_AT_ADD_COLUMN
					| SQL_AT_DROP_COLUMN
					| SQL_AT_ADD_COLUMN_SINGLE
					| SQL_AT_ADD_CONSTRAINT
					| SQL_AT_ADD_TABLE_CONSTRAINT
					| SQL_AT_CONSTRAINT_INITIALLY_DEFERRED
					| SQL_AT_CONSTRAINT_INITIALLY_IMMEDIATE
					| SQL_AT_CONSTRAINT_DEFERRABLE
					| SQL_AT_DROP_TABLE_CONSTRAINT_RESTRICT
					| SQL_AT_DROP_TABLE_CONSTRAINT_CASCADE
					| SQL_AT_DROP_COLUMN_RESTRICT
					| SQL_AT_DROP_COLUMN_CASCADE;
			break;

		case SQL_BOOKMARK_PERSISTENCE:	/* ODBC 2.0 */
			/* very simple bookmark support */
			len = 4;
			value = SQL_BP_SCROLL | SQL_BP_DELETE | SQL_BP_UPDATE | SQL_BP_TRANSACTION;
			break;

		case SQL_COLUMN_ALIAS:	/* ODBC 2.0 */
			p = "Y";
			break;

		case SQL_CONCAT_NULL_BEHAVIOR:	/* ODBC 1.0 */
			len = 2;
			value = SQL_CB_NON_NULL;
			break;

		case SQL_CONVERT_INTEGER:
		case SQL_CONVERT_SMALLINT:
		case SQL_CONVERT_TINYINT:
		case SQL_CONVERT_BIT:
		case SQL_CONVERT_VARCHAR:		/* ODBC 1.0 */
			len = sizeof(SQLUINTEGER);
			value = SQL_CVT_BIT | SQL_CVT_INTEGER;
MYLOG(0, "SQL_CONVERT_ mask=" FORMAT_ULEN "\n", value);
			break;
		case SQL_CONVERT_BIGINT:
		case SQL_CONVERT_DECIMAL:
		case SQL_CONVERT_DOUBLE:
		case SQL_CONVERT_FLOAT:
		case SQL_CONVERT_NUMERIC:
		case SQL_CONVERT_REAL:
		case SQL_CONVERT_DATE:
		case SQL_CONVERT_TIME:
		case SQL_CONVERT_TIMESTAMP:
		case SQL_CONVERT_BINARY:
		case SQL_CONVERT_LONGVARBINARY:
		case SQL_CONVERT_VARBINARY:		/* ODBC 1.0 */
		case SQL_CONVERT_CHAR:
		case SQL_CONVERT_LONGVARCHAR:
#ifdef UNICODE_SUPPORT
		case SQL_CONVERT_WCHAR:
		case SQL_CONVERT_WLONGVARCHAR:
		case SQL_CONVERT_WVARCHAR:
#endif /* UNICODE_SUPPORT */
			len = sizeof(SQLUINTEGER);
			value = 0;	/* CONVERT is unavailable */
			break;

		case SQL_CONVERT_FUNCTIONS:		/* ODBC 1.0 */
			len = sizeof(SQLUINTEGER);
			value = SQL_FN_CVT_CONVERT;
MYLOG(0, "CONVERT_FUNCTIONS=" FORMAT_ULEN "\n", value);
			break;

		case SQL_CORRELATION_NAME:		/* ODBC 1.0 */

			/*
			 * Saying no correlation name makes Query not work right.
			 * value = SQL_CN_NONE;
			 */
			len = 2;
			value = SQL_CN_ANY;
			break;

		case SQL_CURSOR_COMMIT_BEHAVIOR:		/* ODBC 1.0 */
			len = 2;
			value = SQL_CB_PRESERVE;
			break;

		case SQL_CURSOR_ROLLBACK_BEHAVIOR:		/* ODBC 1.0 */
			len = 2;
			if (!ci->drivers.use_declarefetch)
				value = SQL_CB_PRESERVE;
			else
				value = SQL_CB_CLOSE;
			break;

		case SQL_DATA_SOURCE_NAME:		/* ODBC 1.0 */
			p = CC_get_DSN(conn);
			break;

		case SQL_DATA_SOURCE_READ_ONLY: /* ODBC 1.0 */
			p = CC_is_onlyread(conn) ? "Y" : "N";
			break;

		case SQL_DATABASE_NAME:	/* Support for old ODBC 1.0 Apps */

			/*
			 * Returning the database name causes problems in MS Query. It
			 * generates query like: "SELECT DISTINCT a FROM byronnbad3
			 * bad3"
			 *
			 * p = CC_get_database(conn);
			 */
			p = CurrCatString(conn);
			break;

		case SQL_DBMS_NAME:		/* ODBC 1.0 */
			if (CC_fake_mss(conn))
				p = "Microsoft SQL Server";
			else
				p = PQparameterStatus(conn->pqconn, "server_version");
			break;

		case SQL_DBMS_VER:		/* ODBC 1.0 */
			/*
			 * The ODBC spec wants ##.##.####
			 */
			STRCPY_FIXED(tmp, conn->pg_version);
			p = tmp;
			break;

		case SQL_DEFAULT_TXN_ISOLATION: /* ODBC 1.0 */
			len = 4;
			if (0 == conn->default_isolation)
				conn->isolation = CC_get_isolation(conn);
			value = conn->default_isolation;
			break;

		case SQL_DRIVER_NAME:	/* ODBC 1.0 */
			p = DRIVER_FILE_NAME;
			break;

		case SQL_DRIVER_ODBC_VER:
			SPRINTF_FIXED(odbcver, "%02x.%02x", ODBCVER / 256, ODBCVER % 256);
			/* p = DRIVER_ODBC_VER; */
			p = odbcver;
			break;

		case SQL_DRIVER_VER:	/* ODBC 1.0 */
			p = POSTGRESDRIVERVERSION;
			break;

		case SQL_EXPRESSIONS_IN_ORDERBY:		/* ODBC 1.0 */
			p = "Y";
			break;

		case SQL_FETCH_DIRECTION:		/* ODBC 1.0 */
			len = 4;
			value = (SQL_FD_FETCH_NEXT |
					 SQL_FD_FETCH_FIRST |
					 SQL_FD_FETCH_LAST |
					 SQL_FD_FETCH_PRIOR |
					 SQL_FD_FETCH_ABSOLUTE |
					 SQL_FD_FETCH_RELATIVE |
					 SQL_FD_FETCH_BOOKMARK);
			break;

		case SQL_FILE_USAGE:	/* ODBC 2.0 */
			len = 2;
			value = SQL_FILE_NOT_SUPPORTED;
			break;

		case SQL_GETDATA_EXTENSIONS:	/* ODBC 2.0 */
			len = 4;
			value = (SQL_GD_ANY_COLUMN | SQL_GD_ANY_ORDER | SQL_GD_BOUND | SQL_GD_BLOCK);
			break;

		case SQL_GROUP_BY:		/* ODBC 2.0 */
			len = 2;
			value = SQL_GB_GROUP_BY_EQUALS_SELECT;
			break;

		case SQL_IDENTIFIER_CASE:		/* ODBC 1.0 */

			/*
			 * are identifiers case-sensitive (yes, but only when quoted.
			 * If not quoted, they default to lowercase)
			 */
			len = 2;
			value = SQL_IC_LOWER;
			break;

		case SQL_IDENTIFIER_QUOTE_CHAR: /* ODBC 1.0 */
			/* the character used to quote "identifiers" */
			p = "\"";
			break;

		case SQL_KEYWORDS:		/* ODBC 2.0 */
			p = NULL_STRING;
			break;

		case SQL_LIKE_ESCAPE_CLAUSE:	/* ODBC 2.0 */

			/*
			 * is there a character that escapes '%' and '_' in a LIKE
			 * clause? not as far as I can tell
			 */
			p = "N";
			break;

		case SQL_LOCK_TYPES:	/* ODBC 2.0 */
			len = 4;
			value = ci->drivers.lie ? (SQL_LCK_NO_CHANGE | SQL_LCK_EXCLUSIVE | SQL_LCK_UNLOCK) : SQL_LCK_NO_CHANGE;
			break;

		case SQL_MAX_BINARY_LITERAL_LEN:		/* ODBC 2.0 */
			len = 4;
			value = 0;
			break;

		case SQL_MAX_CHAR_LITERAL_LEN:	/* ODBC 2.0 */
			len = 4;
			value = 0;
			break;

		case SQL_MAX_COLUMN_NAME_LEN:	/* ODBC 1.0 */
			len = 2;
			value = CC_get_max_idlen(conn);
			if (0 == value)
				value = NAMEDATALEN_V73 - 1;
			break;

		case SQL_MAX_COLUMNS_IN_GROUP_BY:		/* ODBC 2.0 */
			len = 2;
			value = 0;
			break;

		case SQL_MAX_COLUMNS_IN_INDEX:	/* ODBC 2.0 */
			len = 2;
			value = 0;
			break;

		case SQL_MAX_COLUMNS_IN_ORDER_BY:		/* ODBC 2.0 */
			len = 2;
			value = 0;
			break;

		case SQL_MAX_COLUMNS_IN_SELECT: /* ODBC 2.0 */
			len = 2;
			value = 0;
			break;

		case SQL_MAX_COLUMNS_IN_TABLE:	/* ODBC 2.0 */
			len = 2;
			value = 0;
			break;

		case SQL_MAX_CURSOR_NAME_LEN:	/* ODBC 1.0 */
			len = 2;
			value = MAX_CURSOR_LEN;
			break;

		case SQL_MAX_INDEX_SIZE:		/* ODBC 2.0 */
			len = 4;
			value = 0;
			break;

		case SQL_MAX_SCHEMA_NAME_LEN:	/* ODBC 1.0 */
			// Renamed from SQL_MAX_OWNER_NAME_LEN in ODBC 3.0
			len = 2;
			value = CC_get_max_idlen(conn);
			if (0 == value)
				value = NAMEDATALEN_V73 - 1;
			break;

		case SQL_MAX_PROCEDURE_NAME_LEN:		/* ODBC 1.0 */
			len = 2;
			value = 0;
			break;

		case SQL_MAX_CATALOG_NAME_LEN:		/* ODBC 1.0 */
			// Renamed from SQL_MAX_QUALIFIER_NAME_LEN in ODBC 3.0
			len = 2;
			value = 0;
			break;

		case SQL_MAX_ROW_SIZE:	/* ODBC 2.0 */
			len = 4;
			/* No limit with tuptoaster in 7.1+ */
			value = 0;
			break;

		case SQL_MAX_STATEMENT_LEN:		/* ODBC 2.0 */
			len = 4;
			value = 0;
			break;

		case SQL_MAX_TABLE_NAME_LEN:	/* ODBC 1.0 */
			len = 2;
			value = CC_get_max_idlen(conn);
			if (0 == value)
				value = NAMEDATALEN_V73 - 1;
			break;

		case SQL_MAX_TABLES_IN_SELECT:	/* ODBC 2.0 */
			len = 2;
			value = 0;
			break;

		case SQL_MAX_USER_NAME_LEN:
			len = 2;
			value = 0;
			break;

		case SQL_MULT_RESULT_SETS:		/* ODBC 1.0 */
			/* Don't support multiple result sets but say yes anyway? */
			p = "Y";
			break;

		case SQL_MULTIPLE_ACTIVE_TXN:	/* ODBC 1.0 */
			p = "Y";
			break;

		case SQL_NEED_LONG_DATA_LEN:	/* ODBC 2.0 */

			/*
			 * Don't need the length, SQLPutData can handle any size and
			 * multiple calls
			 */
			p = "N";
			break;

		case SQL_NON_NULLABLE_COLUMNS:	/* ODBC 1.0 */
			len = 2;
			value = SQL_NNC_NON_NULL;
			break;

		case SQL_NULL_COLLATION:		/* ODBC 2.0 */
			/* where are nulls sorted? */
			len = 2;
			value = SQL_NC_HIGH;
			break;

		case SQL_NUMERIC_FUNCTIONS:		/* ODBC 1.0 */
			len = 4;
			value = 0;
			break;

		case SQL_ODBC_API_CONFORMANCE:	/* ODBC 1.0 */
			len = 2;
			value = SQL_OAC_LEVEL1;
			break;

		case SQL_ODBC_SAG_CLI_CONFORMANCE:		/* ODBC 1.0 */
			len = 2;
			value = SQL_OSCC_NOT_COMPLIANT;
			break;

		case SQL_ODBC_SQL_CONFORMANCE:	/* ODBC 1.0 */
			len = 2;
			value = SQL_OSC_CORE;
			break;

		case SQL_INTEGRITY:		/* ODBC 1.0 */
			// Renamed from SQL_ODBC_SQL_OPT_IEF in ODBC 3.0
			p = "N";
			break;

		case SQL_OJ_CAPABILITIES:		/* ODBC 2.01 */
			len = 4;
			value = (SQL_OJ_LEFT |
					 SQL_OJ_RIGHT |
					 SQL_OJ_FULL |
					 SQL_OJ_NESTED |
					 SQL_OJ_NOT_ORDERED |
					 SQL_OJ_INNER |
					 SQL_OJ_ALL_COMPARISON_OPS);
			break;

		case SQL_ORDER_BY_COLUMNS_IN_SELECT:	/* ODBC 2.0 */
			p = "Y";
			break;

		case SQL_OUTER_JOINS:	/* ODBC 1.0 */
			p = "Y";
			break;

		case SQL_SCHEMA_TERM:	/* ODBC 1.0 */
			// Renamed from SQL_OWNER_TERM in ODBC 3.0
			p = "schema";
			break;

		case SQL_SCHEMA_USAGE:	/* ODBC 2.0 */
			// Renamed from SQL_OWNER_USAGE in ODBC 3.0
			len = 4;
			value = SQL_OU_DML_STATEMENTS
					| SQL_OU_TABLE_DEFINITION
					| SQL_OU_INDEX_DEFINITION
					| SQL_OU_PRIVILEGE_DEFINITION;
			break;

		case SQL_POS_OPERATIONS:		/* ODBC 2.0 */
			len = 4;
			value = (SQL_POS_POSITION | SQL_POS_REFRESH);
			if (0 != ci->updatable_cursors)
				value |= (SQL_POS_UPDATE | SQL_POS_DELETE | SQL_POS_ADD);
			break;

		case SQL_POSITIONED_STATEMENTS: /* ODBC 2.0 */
			len = 4;
			value = ci->drivers.lie ? (SQL_PS_POSITIONED_DELETE |
									   SQL_PS_POSITIONED_UPDATE |
									   SQL_PS_SELECT_FOR_UPDATE) : 0;
			break;

		case SQL_PROCEDURE_TERM:		/* ODBC 1.0 */
			p = "procedure";
			break;

		case SQL_PROCEDURES:	/* ODBC 1.0 */
			p = "Y";
			break;

		case SQL_CATALOG_LOCATION:	/* ODBC 2.0 */
			// Renamed from SQL_QUALIFIER_LOCATION in ODBC 3.0
			len = 2;
			if (CurrCat(conn))
				value = SQL_QL_START;
			else
				value = 0;
			break;

		case SQL_CATALOG_NAME_SEPARATOR:		/* ODBC 1.0 */
			// Renamed from SQL_QUALIFIER_NAME_SEPARATOR in ODBC 3.0
			if (CurrCat(conn))
				p = ".";
			else
				p = NULL_STRING;
			break;

		case SQL_CATALOG_TERM:		/* ODBC 1.0 */
			// Renamed from SQL_QUALIFIER_TERM in ODBC 3.0
			if (CurrCat(conn))
				p = "catalog";
			else
				p = NULL_STRING;
			break;

		case SQL_CATALOG_USAGE:		/* ODBC 2.0 */
			// Renamed from SQL_QUALIFIER_USAGE in ODBC 3.0
			len = 4;
			if (CurrCat(conn))
				value = SQL_CU_DML_STATEMENTS;
			else
				value = 0;
			break;

		case SQL_QUOTED_IDENTIFIER_CASE:		/* ODBC 2.0 */
			/* are "quoted" identifiers case-sensitive?  YES! */
			len = 2;
			value = SQL_IC_SENSITIVE;
			break;

		case SQL_ROW_UPDATES:	/* ODBC 1.0 */

			/*
			 * Driver doesn't support keyset-driven or mixed cursors, so
			 * not much point in saying row updates are supported
			 */
			p = (0 != ci->updatable_cursors) ? "Y" : "N";
			break;

		case SQL_SCROLL_CONCURRENCY:	/* ODBC 1.0 */
			len = 4;
			value = SQL_SCCO_READ_ONLY;
			if (0 != ci->updatable_cursors)
				value |= SQL_SCCO_OPT_ROWVER;
			if (ci->drivers.lie)
				value |= (SQL_SCCO_LOCK | SQL_SCCO_OPT_VALUES);
			break;

		case SQL_SCROLL_OPTIONS:		/* ODBC 1.0 */
			len = 4;
			value = SQL_SO_FORWARD_ONLY | SQL_SO_STATIC;
			if (0 != (ci->updatable_cursors & ALLOW_KEYSET_DRIVEN_CURSORS))
				value |= SQL_SO_KEYSET_DRIVEN;
			if (ci->drivers.lie)
				value |= (SQL_SO_DYNAMIC | SQL_SO_MIXED);
			break;

		case SQL_SEARCH_PATTERN_ESCAPE: /* ODBC 1.0 */
			p = "\\";
			break;

		case SQL_SERVER_NAME:	/* ODBC 1.0 */
			p = CC_get_server(conn);
			break;

		case SQL_SPECIAL_CHARACTERS:	/* ODBC 2.0 */
			p = "_";
			break;

		case SQL_STATIC_SENSITIVITY:	/* ODBC 2.0 */
			len = 4;
			value = 0;
			if (0 != ci->updatable_cursors)
				value |= (SQL_SS_ADDITIONS | SQL_SS_DELETIONS | SQL_SS_UPDATES);
			break;

		case SQL_STRING_FUNCTIONS:		/* ODBC 1.0 */
			len = 4;
			value = (SQL_FN_STR_CONCAT |
					 SQL_FN_STR_LCASE |
					 SQL_FN_STR_LENGTH |
					 SQL_FN_STR_LOCATE |
					 SQL_FN_STR_LTRIM |
					 SQL_FN_STR_RTRIM |
					 SQL_FN_STR_SUBSTRING |
					 SQL_FN_STR_UCASE);
			break;

		case SQL_SUBQUERIES:	/* ODBC 2.0 */
			/* postgres 6.3 supports subqueries */
			len = 4;
			value = (SQL_SQ_QUANTIFIED |
					 SQL_SQ_IN |
					 SQL_SQ_EXISTS |
					 SQL_SQ_COMPARISON);
			break;

		case SQL_SYSTEM_FUNCTIONS:		/* ODBC 1.0 */
			len = 4;
			value = 0;
			break;

		case SQL_TABLE_TERM:	/* ODBC 1.0 */
			p = "table";
			break;

		case SQL_TIMEDATE_ADD_INTERVALS:		/* ODBC 2.0 */
			len = 4;
			value = 0;
			break;

		case SQL_TIMEDATE_DIFF_INTERVALS:		/* ODBC 2.0 */
			len = 4;
			value = 0;
			break;

		case SQL_TIMEDATE_FUNCTIONS:	/* ODBC 1.0 */
			len = 4;
			value = (SQL_FN_TD_NOW);
			break;

		case SQL_TXN_CAPABLE:	/* ODBC 1.0 */

			/*
			 * Postgres can deal with create or drop table statements in a
			 * transaction
			 */
			len = 2;
			value = SQL_TC_ALL;
			break;

		case SQL_TXN_ISOLATION_OPTION:	/* ODBC 1.0 */
			len = 4;
			value = SQL_TXN_READ_UNCOMMITTED | SQL_TXN_READ_COMMITTED |
				SQL_TXN_REPEATABLE_READ | SQL_TXN_SERIALIZABLE;
			break;

		case SQL_UNION: /* ODBC 2.0 */
			/* unions with all supported in postgres 6.3 */
			len = 4;
			value = (SQL_U_UNION | SQL_U_UNION_ALL);
			break;

		case SQL_USER_NAME:		/* ODBC 1.0 */
			p = CC_get_username(conn);
			break;

		/* Keys for ODBC 3.0 */
		case SQL_DYNAMIC_CURSOR_ATTRIBUTES1:
			len = 4;
			value = 0;
			break;
		case SQL_DYNAMIC_CURSOR_ATTRIBUTES2:
			len = 4;
			value = 0;
			break;
		case SQL_FORWARD_ONLY_CURSOR_ATTRIBUTES1:
			len = 4;
			value = SQL_CA1_NEXT; /* others aren't allowed in ODBC spec */
			break;
		case SQL_FORWARD_ONLY_CURSOR_ATTRIBUTES2:
			len = 4;
			value = SQL_CA2_READ_ONLY_CONCURRENCY;
			if (!ci->drivers.use_declarefetch || ci->drivers.lie)
				value |= SQL_CA2_CRC_EXACT;
			break;
		case SQL_KEYSET_CURSOR_ATTRIBUTES1:
			len = 4;
			value = SQL_CA1_NEXT | SQL_CA1_ABSOLUTE
				| SQL_CA1_RELATIVE | SQL_CA1_BOOKMARK
				| SQL_CA1_LOCK_NO_CHANGE | SQL_CA1_POS_POSITION
				| SQL_CA1_POS_REFRESH;
			if (0 != (ci->updatable_cursors & ALLOW_KEYSET_DRIVEN_CURSORS))
				value |= (SQL_CA1_POS_UPDATE | SQL_CA1_POS_DELETE
				| SQL_CA1_BULK_ADD
				| SQL_CA1_BULK_UPDATE_BY_BOOKMARK
				| SQL_CA1_BULK_DELETE_BY_BOOKMARK
				| SQL_CA1_BULK_FETCH_BY_BOOKMARK
				);
			if (ci->drivers.lie)
				value |= (SQL_CA1_LOCK_EXCLUSIVE
				| SQL_CA1_LOCK_UNLOCK
				| SQL_CA1_POSITIONED_UPDATE
				| SQL_CA1_POSITIONED_DELETE
				| SQL_CA1_SELECT_FOR_UPDATE
				);
			break;
		case SQL_KEYSET_CURSOR_ATTRIBUTES2:
			len = 4;
			value = SQL_CA2_READ_ONLY_CONCURRENCY;
			if (0 != (ci->updatable_cursors & ALLOW_KEYSET_DRIVEN_CURSORS))
				value |= (SQL_CA2_OPT_ROWVER_CONCURRENCY
				/*| SQL_CA2_CRC_APPROXIMATE*/
				);
			if (0 != (ci->updatable_cursors & SENSE_SELF_OPERATIONS))
				value |= (SQL_CA2_SENSITIVITY_DELETIONS
				| SQL_CA2_SENSITIVITY_UPDATES
				| SQL_CA2_SENSITIVITY_ADDITIONS
				);
			if (!ci->drivers.use_declarefetch || ci->drivers.lie)
				value |= SQL_CA2_CRC_EXACT;
			if (ci->drivers.lie)
				value |= (SQL_CA2_LOCK_CONCURRENCY
				| SQL_CA2_OPT_VALUES_CONCURRENCY
				| SQL_CA2_MAX_ROWS_SELECT
				| SQL_CA2_MAX_ROWS_INSERT
				| SQL_CA2_MAX_ROWS_DELETE
				| SQL_CA2_MAX_ROWS_UPDATE
				| SQL_CA2_MAX_ROWS_CATALOG
				| SQL_CA2_MAX_ROWS_AFFECTS_ALL
				| SQL_CA2_SIMULATE_NON_UNIQUE
				| SQL_CA2_SIMULATE_TRY_UNIQUE
				| SQL_CA2_SIMULATE_UNIQUE
				);
			break;

		case SQL_STATIC_CURSOR_ATTRIBUTES1:
			len = 4;
			value = SQL_CA1_NEXT | SQL_CA1_ABSOLUTE
				| SQL_CA1_RELATIVE | SQL_CA1_BOOKMARK
				| SQL_CA1_LOCK_NO_CHANGE | SQL_CA1_POS_POSITION
				| SQL_CA1_POS_REFRESH;
			if (0 != (ci->updatable_cursors & ALLOW_STATIC_CURSORS))
				value |= (SQL_CA1_POS_UPDATE | SQL_CA1_POS_DELETE
				);
			if (0 != (ci->updatable_cursors & ALLOW_BULK_OPERATIONS))
				value |= (SQL_CA1_BULK_ADD
				| SQL_CA1_BULK_UPDATE_BY_BOOKMARK
				| SQL_CA1_BULK_DELETE_BY_BOOKMARK
				| SQL_CA1_BULK_FETCH_BY_BOOKMARK
				);
			break;
		case SQL_STATIC_CURSOR_ATTRIBUTES2:
			len = 4;
			value = SQL_CA2_READ_ONLY_CONCURRENCY;
			if (0 != (ci->updatable_cursors & ALLOW_STATIC_CURSORS))
				value |= (SQL_CA2_OPT_ROWVER_CONCURRENCY
				);
			if (0 != (ci->updatable_cursors & SENSE_SELF_OPERATIONS))
				value |= (SQL_CA2_SENSITIVITY_DELETIONS
				| SQL_CA2_SENSITIVITY_UPDATES
				| SQL_CA2_SENSITIVITY_ADDITIONS
				);
			if (!ci->drivers.use_declarefetch || ci->drivers.lie)
				value |= (SQL_CA2_CRC_EXACT
				);
			break;

		case SQL_ODBC_INTERFACE_CONFORMANCE:
			len = 4;
			value = SQL_OIC_CORE;
			if (ci->drivers.lie)
				value = SQL_OIC_LEVEL2;
			break;
		case SQL_ACTIVE_ENVIRONMENTS:
			len = 2;
			value = 0;
			break;
		case SQL_AGGREGATE_FUNCTIONS:
			len = 4;
			value = SQL_AF_ALL;
			break;
		case SQL_ALTER_DOMAIN:
			len = 4;
			value = 0;
			break;
		case SQL_ASYNC_MODE:
			len = 4;
			value = SQL_AM_NONE;
			break;
		case SQL_BATCH_ROW_COUNT:
			len = 4;
			value = SQL_BRC_EXPLICIT;
			break;
		case SQL_BATCH_SUPPORT:
			len = 4;
			value = SQL_BS_SELECT_EXPLICIT | SQL_BS_ROW_COUNT_EXPLICIT;
			break;
		case SQL_CATALOG_NAME:
			if (CurrCat(conn))
				p = "Y";
			else
				p = "N";
			break;
		case SQL_COLLATION_SEQ:
			p = "";
			break;
		case SQL_CREATE_ASSERTION:
			len = 4;
			value = 0;
			break;
		case SQL_CREATE_CHARACTER_SET:
			len = 4;
			value = 0;
			break;
		case SQL_CREATE_COLLATION:
			len = 4;
			value = 0;
			break;
		case SQL_CREATE_DOMAIN:
			len = 4;
			value = 0;
			break;
		case SQL_CREATE_SCHEMA:
			len = 4;
			value = SQL_CS_CREATE_SCHEMA | SQL_CS_AUTHORIZATION;
			break;
		case SQL_CREATE_TABLE:
			len = 4;
			value = SQL_CT_CREATE_TABLE
				| SQL_CT_COLUMN_CONSTRAINT
				| SQL_CT_COLUMN_DEFAULT
				| SQL_CT_GLOBAL_TEMPORARY
				| SQL_CT_TABLE_CONSTRAINT
				| SQL_CT_CONSTRAINT_NAME_DEFINITION
				| SQL_CT_CONSTRAINT_INITIALLY_DEFERRED
				| SQL_CT_CONSTRAINT_INITIALLY_IMMEDIATE
				| SQL_CT_CONSTRAINT_DEFERRABLE;
			break;
		case SQL_CREATE_TRANSLATION:
			len = 4;
			value = 0;
			break;
		case SQL_CREATE_VIEW:
			len = 4;
			value = SQL_CV_CREATE_VIEW;
			break;
		case SQL_DDL_INDEX:
			len = 4;
			value = SQL_DI_CREATE_INDEX | SQL_DI_DROP_INDEX;
			break;
		case SQL_DESCRIBE_PARAMETER:
			p = "N";
			break;
		case SQL_DROP_ASSERTION:
			len = 4;
			value = 0;
			break;
		case SQL_DROP_CHARACTER_SET:
			len = 4;
			value = 0;
			break;
		case SQL_DROP_COLLATION:
			len = 4;
			value = 0;
			break;
		case SQL_DROP_DOMAIN:
			len = 4;
			value = 0;
			break;
		case SQL_DROP_SCHEMA:
			len = 4;
			value = SQL_DS_DROP_SCHEMA | SQL_DS_RESTRICT | SQL_DS_CASCADE;
			break;
		case SQL_DROP_TABLE:
			len = 4;
			value = SQL_DT_DROP_TABLE;
			value |= (SQL_DT_RESTRICT | SQL_DT_CASCADE);
			break;
		case SQL_DROP_TRANSLATION:
			len = 4;
			value = 0;
			break;
		case SQL_DROP_VIEW:
			len = 4;
			value = SQL_DV_DROP_VIEW;
			value |= (SQL_DV_RESTRICT | SQL_DV_CASCADE);
			break;
		case SQL_INDEX_KEYWORDS:
			len = 4;
			value = SQL_IK_NONE;
			break;
		case SQL_INFO_SCHEMA_VIEWS:
			len = 4;
			value = 0;
			break;
		case SQL_INSERT_STATEMENT:
			len = 4;
			value = SQL_IS_INSERT_LITERALS | SQL_IS_INSERT_SEARCHED | SQL_IS_SELECT_INTO;
			break;
		case SQL_MAX_IDENTIFIER_LEN:
			len = 2;
			value = CC_get_max_idlen(conn);
			if (0 == value)
				value = NAMEDATALEN_V73 - 1;
			break;
		case SQL_MAX_ROW_SIZE_INCLUDES_LONG:
			p = "Y";
			break;
		case SQL_PARAM_ARRAY_ROW_COUNTS:
			len = 4;
			value = SQL_PARC_BATCH;
			break;
		case SQL_PARAM_ARRAY_SELECTS:
			len = 4;
			value = SQL_PAS_BATCH;
			break;
		case SQL_SQL_CONFORMANCE:
			len = 4;
			value = SQL_SC_SQL92_ENTRY;
			break;
		case SQL_SQL92_DATETIME_FUNCTIONS:
			len = 4;
			value = SQL_SDF_CURRENT_DATE | SQL_SDF_CURRENT_TIME | SQL_SDF_CURRENT_TIMESTAMP;
			break;
		case SQL_SQL92_FOREIGN_KEY_DELETE_RULE:
			len = 4;
			value = SQL_SFKD_CASCADE | SQL_SFKD_NO_ACTION | SQL_SFKD_SET_DEFAULT | SQL_SFKD_SET_NULL;
			break;
		case SQL_SQL92_FOREIGN_KEY_UPDATE_RULE:
			len = 4;
			value = SQL_SFKU_CASCADE | SQL_SFKU_NO_ACTION | SQL_SFKU_SET_DEFAULT | SQL_SFKU_SET_NULL;
			break;
		case SQL_SQL92_GRANT:
			len = 4;
			value = SQL_SG_DELETE_TABLE | SQL_SG_INSERT_TABLE | SQL_SG_REFERENCES_TABLE | SQL_SG_SELECT_TABLE | SQL_SG_UPDATE_TABLE;
			break;
		case SQL_SQL92_NUMERIC_VALUE_FUNCTIONS:
			len = 4;
			value = SQL_SNVF_BIT_LENGTH | SQL_SNVF_CHAR_LENGTH
				| SQL_SNVF_CHARACTER_LENGTH | SQL_SNVF_EXTRACT
				| SQL_SNVF_OCTET_LENGTH | SQL_SNVF_POSITION;
			break;
		case SQL_SQL92_PREDICATES:
			len = 4;
			value = SQL_SP_BETWEEN | SQL_SP_COMPARISON
				| SQL_SP_EXISTS | SQL_SP_IN
				| SQL_SP_ISNOTNULL | SQL_SP_ISNULL
				| SQL_SP_LIKE | SQL_SP_OVERLAPS
				| SQL_SP_QUANTIFIED_COMPARISON;
			break;
		case SQL_SQL92_RELATIONAL_JOIN_OPERATORS:
			len = 4;
			value = SQL_SRJO_CROSS_JOIN | SQL_SRJO_EXCEPT_JOIN
				| SQL_SRJO_FULL_OUTER_JOIN | SQL_SRJO_INNER_JOIN
				| SQL_SRJO_INTERSECT_JOIN | SQL_SRJO_LEFT_OUTER_JOIN
				| SQL_SRJO_NATURAL_JOIN | SQL_SRJO_RIGHT_OUTER_JOIN
				| SQL_SRJO_UNION_JOIN;
			break;
		case SQL_SQL92_REVOKE:
			len = 4;
			value = SQL_SR_DELETE_TABLE | SQL_SR_INSERT_TABLE | SQL_SR_REFERENCES_TABLE | SQL_SR_SELECT_TABLE | SQL_SR_UPDATE_TABLE;
			break;
		case SQL_SQL92_ROW_VALUE_CONSTRUCTOR:
			len = 4;
			value = SQL_SRVC_VALUE_EXPRESSION | SQL_SRVC_NULL;
			break;
		case SQL_SQL92_STRING_FUNCTIONS:
			len = 4;
			value = SQL_SSF_CONVERT | SQL_SSF_LOWER
				| SQL_SSF_UPPER | SQL_SSF_SUBSTRING
				| SQL_SSF_TRANSLATE | SQL_SSF_TRIM_BOTH
				| SQL_SSF_TRIM_LEADING | SQL_SSF_TRIM_TRAILING;
			break;
		case SQL_SQL92_VALUE_EXPRESSIONS:
			len = 4;
			value = SQL_SVE_CASE | SQL_SVE_CAST | SQL_SVE_COALESCE | SQL_SVE_NULLIF;
			break;
#ifdef SQL_DTC_TRANSACTION_COST
		case SQL_DTC_TRANSACTION_COST:
#else
		case 1750:
#endif
			len = 4;
			break;
		/* The followings aren't implemented yet */
		case SQL_DATETIME_LITERALS:
			len = 4;
		case SQL_DM_VER:
			len = 0;
		case SQL_DRIVER_HDESC:
			len = 4;
		case SQL_MAX_ASYNC_CONCURRENT_STATEMENTS:
			len = 4;
		case SQL_STANDARD_CLI_CONFORMANCE:
			len = 4;
		case SQL_XOPEN_CLI_YEAR:
			len = 0;

		default:
			/* unrecognized key */
			CC_set_error(conn, CONN_NOT_IMPLEMENTED_ERROR, "Unrecognized key passed to PGAPI_GetInfo.", NULL);
			goto cleanup;
	}

	ret = SQL_SUCCESS;

	MYLOG(0, "p='%s', len=" FORMAT_ULEN ", value=" FORMAT_ULEN ", cbMax=%d\n", p ? p : "<NULL>", len, value, cbInfoValueMax);

	/*
	 * NOTE, that if rgbInfoValue is NULL, then no warnings or errors
	 * should result and just pcbInfoValue is returned, which indicates
	 * what length would be required if a real buffer had been passed in.
	 */
	if (p)
	{
		/* char/binary data */
		len = strlen(p);

		if (rgbInfoValue)
		{
#ifdef	UNICODE_SUPPORT
			if (CC_is_in_unicode_driver(conn))
			{
				len = utf8_to_ucs2(p, len, (SQLWCHAR *) rgbInfoValue, cbInfoValueMax / WCLEN);
				len *= WCLEN;
			}
			else
#endif /* UNICODE_SUPPORT */
			strncpy_null((char *) rgbInfoValue, p, (size_t) cbInfoValueMax);

			if (len >= cbInfoValueMax)
			{
				ret = SQL_SUCCESS_WITH_INFO;
				CC_set_error(conn, CONN_TRUNCATED, "The buffer was too small for the InfoValue.", func);
			}
		}
#ifdef	UNICODE_SUPPORT
		else if (CC_is_in_unicode_driver(conn))
			len *= WCLEN;
#endif /* UNICODE_SUPPORT */
	}
	else
	{
		/* numeric data */
		if (rgbInfoValue)
		{
			if (len == sizeof(SQLSMALLINT))
				*((SQLUSMALLINT *) rgbInfoValue) = (SQLUSMALLINT) value;
			else if (len == sizeof(SQLINTEGER))
				*((SQLUINTEGER *) rgbInfoValue) = (SQLUINTEGER) value;
		}
	}

	if (pcbInfoValue)
		*pcbInfoValue = (SQLSMALLINT) len;
cleanup:

	return ret;
}

/*
 *	macros for pgtype_xxxx() calls which have PG_ATP_UNSET parameters
 */
#define PGTYPE_COLUMN_SIZE(conn, pgType) pgtype_attr_column_size(conn, pgType, PG_ATP_UNSET, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_TO_CONCISE_TYPE(conn, pgType) pgtype_attr_to_concise_type(conn, pgType, PG_ATP_UNSET, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_TO_SQLDESCTYPE(conn, pgType) pgtype_attr_to_sqldesctype(conn, pgType, PG_ATP_UNSET, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_BUFFER_LENGTH(conn, pgType) pgtype_attr_buffer_length(conn, pgType, PG_ATP_UNSET, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_DECIMAL_DIGITS(conn, pgType) pgtype_attr_decimal_digits(conn, pgType, PG_ATP_UNSET, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_TRANSFER_OCTET_LENGTH(conn, pgType) pgtype_attr_transfer_octet_length(conn, pgType, PG_ATP_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_TO_NAME(conn, pgType, auto_increment) pgtype_attr_to_name(conn, pgType, PG_ATP_UNSET, auto_increment)
#define PGTYPE_TO_DATETIME_SUB(conn, pgtype) pgtype_attr_to_datetime_sub(conn, pgtype, PG_ATP_UNSET)


RETCODE		SQL_API
PGAPI_GetTypeInfo(HSTMT hstmt,
				  SQLSMALLINT fSqlType)
{
	CSTR func = "PGAPI_GetTypeInfo";
	StatementClass *stmt = (StatementClass *) hstmt;
	QResultClass	*res = NULL;
	int			result_cols;

	/* Int4 type; */
	RETCODE		ret = SQL_ERROR, result;

	MYLOG(0, "entering...fSqlType=%d\n", fSqlType);

	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

	if (res = QR_Constructor(), !res)
	{
		SC_set_error(stmt, STMT_INTERNAL_ERROR, "Error creating result.", func);
		return SQL_ERROR;
	}
	SC_set_Result(stmt, res);

#define	return	DONT_CALL_RETURN_FROM_HERE???
	result_cols = NUM_OF_GETTYPE_FIELDS;
	extend_column_bindings(SC_get_ARDF(stmt), result_cols);

	stmt->catalog_result = TRUE;
	QR_set_num_fields(res, result_cols);
	QR_set_field_info_v(res, GETTYPE_TYPE_NAME, "TYPE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, GETTYPE_DATA_TYPE, "DATA_TYPE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_COLUMN_SIZE, "COLUMN_SIZE", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, GETTYPE_LITERAL_PREFIX, "LITERAL_PREFIX", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, GETTYPE_LITERAL_SUFFIX, "LITERAL_SUFFIX", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, GETTYPE_CREATE_PARAMS, "CREATE_PARAMS", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, GETTYPE_NULLABLE, "NULLABLE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_CASE_SENSITIVE, "CASE_SENSITIVE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_SEARCHABLE, "SEARCHABLE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_UNSIGNED_ATTRIBUTE, "UNSIGNED_ATTRIBUTE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_FIXED_PREC_SCALE, "FIXED_PREC_SCALE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_AUTO_UNIQUE_VALUE, "AUTO_UNIQUE_VALUE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_LOCAL_TYPE_NAME, "LOCAL_TYPE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, GETTYPE_MINIMUM_SCALE, "MINIMUM_SCALE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_MAXIMUM_SCALE, "MAXIMUM_SCALE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_SQL_DATA_TYPE, "SQL_DATA_TYPE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_SQL_DATETIME_SUB, "SQL_DATETIME_SUB", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, GETTYPE_NUM_PREC_RADIX, "NUM_PREC_RADIX", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, GETTYPE_INTERVAL_PRECISION, "INTERVAL_PRECISION", PG_TYPE_INT2, 2);

	/* Teradata ODBC additional columns */
	QR_set_field_info_v(res, GETTYPE_TDODBC_DATA_TYPE, "TDODBC_DATA_TYPE", PG_TYPE_INT2, 2);

	/*
	*  TODO: HQ-6692 Implement results for SQLGetTypeInfo
	*/

	ret = SQL_SUCCESS;


#undef	return
	/*
	 * also, things need to think that this statement is finished so the
	 * results can be retrieved.
	 */
	stmt->status = STMT_FINISHED;
	stmt->currTuple = -1;
	if (SQL_SUCCEEDED(ret))
		SC_set_rowset_start(stmt, -1, FALSE);
	else
		SC_set_Result(stmt, NULL);
	SC_set_current_col(stmt, -1);

	return ret;
}


RETCODE		SQL_API
PGAPI_GetFunctions(HDBC hdbc,
				   SQLUSMALLINT fFunction,
				   SQLUSMALLINT * pfExists)
{
	ConnectionClass *conn = (ConnectionClass *) hdbc;
	ConnInfo   *ci = &(conn->connInfo);

	MYLOG(0, "entering...%u\n", fFunction);

	if (fFunction == SQL_API_ALL_FUNCTIONS)
	{
		memset(pfExists, 0, sizeof(pfExists[0]) * 100);

		/* ODBC core functions */
		pfExists[SQL_API_SQLALLOCCONNECT] = TRUE;
		pfExists[SQL_API_SQLALLOCENV] = TRUE;
		pfExists[SQL_API_SQLALLOCSTMT] = TRUE;
		pfExists[SQL_API_SQLBINDCOL] = TRUE;
		pfExists[SQL_API_SQLCANCEL] = TRUE;
		pfExists[SQL_API_SQLCOLATTRIBUTES] = TRUE;
		pfExists[SQL_API_SQLCONNECT] = TRUE;
		pfExists[SQL_API_SQLDESCRIBECOL] = TRUE;	/* partial */
		pfExists[SQL_API_SQLDISCONNECT] = TRUE;
		pfExists[SQL_API_SQLERROR] = TRUE;
		pfExists[SQL_API_SQLEXECDIRECT] = TRUE;
		pfExists[SQL_API_SQLEXECUTE] = TRUE;
		pfExists[SQL_API_SQLFETCH] = TRUE;
		pfExists[SQL_API_SQLFREECONNECT] = TRUE;
		pfExists[SQL_API_SQLFREEENV] = TRUE;
		pfExists[SQL_API_SQLFREESTMT] = TRUE;
		pfExists[SQL_API_SQLGETCURSORNAME] = TRUE;
		pfExists[SQL_API_SQLNUMRESULTCOLS] = TRUE;
		pfExists[SQL_API_SQLPREPARE] = TRUE;		/* complete? */
		pfExists[SQL_API_SQLROWCOUNT] = TRUE;
		pfExists[SQL_API_SQLSETCURSORNAME] = TRUE;
		pfExists[SQL_API_SQLSETPARAM] = FALSE;		/* odbc 1.0 */
		pfExists[SQL_API_SQLTRANSACT] = TRUE;

		/* ODBC level 1 functions */
		pfExists[SQL_API_SQLBINDPARAMETER] = TRUE;
		pfExists[SQL_API_SQLCOLUMNS] = TRUE;
		pfExists[SQL_API_SQLDRIVERCONNECT] = TRUE;
		pfExists[SQL_API_SQLGETCONNECTOPTION] = TRUE;		/* partial */
		pfExists[SQL_API_SQLGETDATA] = TRUE;
		pfExists[SQL_API_SQLGETFUNCTIONS] = TRUE;
		pfExists[SQL_API_SQLGETINFO] = TRUE;
		pfExists[SQL_API_SQLGETSTMTOPTION] = TRUE;	/* partial */
		pfExists[SQL_API_SQLGETTYPEINFO] = TRUE;
		pfExists[SQL_API_SQLPARAMDATA] = TRUE;
		pfExists[SQL_API_SQLPUTDATA] = TRUE;
		pfExists[SQL_API_SQLSETCONNECTOPTION] = TRUE;		/* partial */
		pfExists[SQL_API_SQLSETSTMTOPTION] = TRUE;
		pfExists[SQL_API_SQLSPECIALCOLUMNS] = TRUE;
		pfExists[SQL_API_SQLSTATISTICS] = TRUE;
		pfExists[SQL_API_SQLTABLES] = TRUE;

		/* ODBC level 2 functions */
		pfExists[SQL_API_SQLBROWSECONNECT] = FALSE;
		pfExists[SQL_API_SQLCOLUMNPRIVILEGES] = FALSE;
		pfExists[SQL_API_SQLDATASOURCES] = FALSE;	/* only implemented by
													 * DM */
		if (SUPPORT_DESCRIBE_PARAM(ci))
			pfExists[SQL_API_SQLDESCRIBEPARAM] = TRUE;
		else
			pfExists[SQL_API_SQLDESCRIBEPARAM] = FALSE; /* not properly
													 * implemented */
		pfExists[SQL_API_SQLDRIVERS] = FALSE;		/* only implemented by
													 * DM */
		pfExists[SQL_API_SQLEXTENDEDFETCH] = TRUE;
		pfExists[SQL_API_SQLFOREIGNKEYS] = TRUE;
		pfExists[SQL_API_SQLMORERESULTS] = TRUE;
		pfExists[SQL_API_SQLNATIVESQL] = TRUE;
		pfExists[SQL_API_SQLNUMPARAMS] = TRUE;
		pfExists[SQL_API_SQLPARAMOPTIONS] = TRUE;
		pfExists[SQL_API_SQLPRIMARYKEYS] = TRUE;
		pfExists[SQL_API_SQLPROCEDURECOLUMNS] = TRUE;
		pfExists[SQL_API_SQLPROCEDURES] = TRUE;
		pfExists[SQL_API_SQLSETPOS] = TRUE;
		pfExists[SQL_API_SQLSETSCROLLOPTIONS] = TRUE;		/* odbc 1.0 */
		pfExists[SQL_API_SQLTABLEPRIVILEGES] = TRUE;
		if (0 == ci->updatable_cursors)
			pfExists[SQL_API_SQLBULKOPERATIONS] = FALSE;
		else
			pfExists[SQL_API_SQLBULKOPERATIONS] = TRUE;
	}
	else
	{
		if (ci->drivers.lie)
			*pfExists = TRUE;
		else
		{
			switch (fFunction)
			{
				case SQL_API_SQLBINDCOL:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLCANCEL:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLCOLATTRIBUTE:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLCONNECT:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLDESCRIBECOL:
					*pfExists = TRUE;
					break;		/* partial */
				case SQL_API_SQLDISCONNECT:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLEXECDIRECT:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLEXECUTE:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLFETCH:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLFREESTMT:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLGETCURSORNAME:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLNUMRESULTCOLS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLPREPARE:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLROWCOUNT:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLSETCURSORNAME:
					*pfExists = TRUE;
					break;

					/* ODBC level 1 functions */
				case SQL_API_SQLBINDPARAMETER:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLCOLUMNS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLDRIVERCONNECT:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLGETDATA:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLGETFUNCTIONS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLGETINFO:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLGETTYPEINFO:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLPARAMDATA:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLPUTDATA:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLSPECIALCOLUMNS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLSTATISTICS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLTABLES:
					*pfExists = TRUE;
					break;

					/* ODBC level 2 functions */
				case SQL_API_SQLBROWSECONNECT:
					*pfExists = FALSE;
					break;
				case SQL_API_SQLCOLUMNPRIVILEGES:
					*pfExists = FALSE;
					break;
				case SQL_API_SQLDATASOURCES:
					*pfExists = FALSE;
					break;		/* only implemented by DM */
				case SQL_API_SQLDESCRIBEPARAM:
					if (SUPPORT_DESCRIBE_PARAM(ci))
						*pfExists = TRUE;
					else
						*pfExists = FALSE;
					break;		/* not properly implemented */
				case SQL_API_SQLDRIVERS:
					*pfExists = FALSE;
					break;		/* only implemented by DM */
				case SQL_API_SQLEXTENDEDFETCH:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLFOREIGNKEYS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLMORERESULTS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLNATIVESQL:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLNUMPARAMS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLPRIMARYKEYS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLPROCEDURECOLUMNS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLPROCEDURES:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLSETPOS:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLTABLEPRIVILEGES:
					*pfExists = TRUE;
					break;
				case SQL_API_SQLBULKOPERATIONS:	/* 24 */
				case SQL_API_SQLALLOCHANDLE:	/* 1001 */
				case SQL_API_SQLBINDPARAM:	/* 1002 */
				case SQL_API_SQLCLOSECURSOR:	/* 1003 */
				case SQL_API_SQLENDTRAN:	/* 1005 */
				case SQL_API_SQLFETCHSCROLL:	/* 1021 */
				case SQL_API_SQLFREEHANDLE:	/* 1006 */
				case SQL_API_SQLGETCONNECTATTR:	/* 1007 */
				case SQL_API_SQLGETDESCFIELD:	/* 1008 */
				case SQL_API_SQLGETDIAGFIELD:	/* 1010 */
				case SQL_API_SQLGETDIAGREC:	/* 1011 */
				case SQL_API_SQLGETENVATTR:	/* 1012 */
				case SQL_API_SQLGETSTMTATTR:	/* 1014 */
				case SQL_API_SQLSETCONNECTATTR:	/* 1016 */
				case SQL_API_SQLSETDESCFIELD:	/* 1017 */
				case SQL_API_SQLSETENVATTR:	/* 1019 */
				case SQL_API_SQLSETSTMTATTR:	/* 1020 */
					*pfExists = TRUE;
					break;
				case SQL_API_SQLGETDESCREC:	/* 1009 */
				case SQL_API_SQLSETDESCREC:	/* 1018 */
				case SQL_API_SQLCOPYDESC:	/* 1004 */
					*pfExists = FALSE;
					break;
				default:
					*pfExists = FALSE;
					break;
			}
		}
	}
	return SQL_SUCCESS;
}


char *
identifierEscape(const SQLCHAR *src, SQLLEN srclen, const ConnectionClass *conn, char *buf, size_t bufsize, BOOL double_quote)
{
	int	i, outlen;
	UCHAR	tchar;
	char	*dest = NULL, escape_ch = CC_get_escape(conn);
	encoded_str	encstr;

	if (!src || srclen == SQL_NULL_DATA)
		return dest;
	else if (srclen == SQL_NTS)
		srclen = (SQLLEN) strlen((char *) src);
	if (srclen <= 0)
		return dest;
MYLOG(0, "entering in=%s(" FORMAT_LEN ")\n", src, srclen);
	if (NULL != buf && bufsize > 0)
		dest = buf;
	else
	{
		bufsize = 2 * srclen + 1;
		dest = malloc(bufsize);
	}
	if (!dest) return NULL;
	encoded_str_constr(&encstr, conn->ccsc, (char *) src);
	outlen = 0;
	if (double_quote)
		dest[outlen++] = IDENTIFIER_QUOTE;
	for (i = 0, tchar = encoded_nextchar(&encstr); i < srclen && outlen < bufsize - 1; i++, tchar = encoded_nextchar(&encstr))
	{
                if (MBCS_NON_ASCII(encstr))
                {
                        dest[outlen++] = tchar;
                        continue;
                }
		if (LITERAL_QUOTE == tchar ||
		    escape_ch == tchar)
			dest[outlen++] = tchar;
		else if (double_quote &&
			 IDENTIFIER_QUOTE == tchar)
			dest[outlen++] = tchar;
		dest[outlen++] = tchar;
	}
	if (double_quote)
		dest[outlen++] = IDENTIFIER_QUOTE;
	dest[outlen] = '\0';
MYLOG(0, "leaving output=%s(%d)\n", dest, outlen);
	return dest;
}

static char *
simpleCatalogEscape(const SQLCHAR *src, SQLLEN srclen, const ConnectionClass *conn)
{
	return identifierEscape(src, srclen, conn, NULL, -1, FALSE);
}

/*
 *	PostgreSQL needs 2 '\\' to escape '_' and '%'.
 */
static char	*
adjustLikePattern(const SQLCHAR *src, int srclen, const ConnectionClass *conn)
{
	int	i, outlen;
	UCHAR	tchar;
	char	*dest = NULL, escape_in_literal = CC_get_escape(conn);
	BOOL	escape_in = FALSE;
	encoded_str	encstr;

	if (!src || srclen == SQL_NULL_DATA)
		return dest;
	else if (srclen == SQL_NTS)
		srclen = (int) strlen((char *) src);
	/* if (srclen <= 0) */
	if (srclen < 0)
		return dest;
MYLOG(0, "entering in=%.*s(%d)\n", srclen, src, srclen);
	encoded_str_constr(&encstr, conn->ccsc, (char *) src);
	dest = malloc(4 * srclen + 1);
	if (!dest) return NULL;
	for (i = 0, outlen = 0; i < srclen; i++)
	{
		tchar = encoded_nextchar(&encstr);
		if (MBCS_NON_ASCII(encstr))
		{
			dest[outlen++] = tchar;
			continue;
		}
		if (escape_in)
		{
			switch (tchar)
			{
				case '%':
				case '_':
					break;
				default:
					if (SEARCH_PATTERN_ESCAPE == escape_in_literal)
						dest[outlen++] = escape_in_literal;
					dest[outlen++] = SEARCH_PATTERN_ESCAPE;
					break;
			}
		}
		if (tchar == SEARCH_PATTERN_ESCAPE)
		{
			escape_in = TRUE;
			if (SEARCH_PATTERN_ESCAPE == escape_in_literal)
				dest[outlen++] = escape_in_literal; /* insert 1 more LEXER escape */
		}
		else
		{
			escape_in = FALSE;
			if (LITERAL_QUOTE == tchar)
				dest[outlen++] = tchar;
		}
		dest[outlen++] = tchar;
	}
	if (escape_in)
	{
		if (SEARCH_PATTERN_ESCAPE == escape_in_literal)
			dest[outlen++] = escape_in_literal;
		dest[outlen++] = SEARCH_PATTERN_ESCAPE;
	}
	dest[outlen] = '\0';
MYLOG(0, "leaving output=%s(%d)\n", dest, outlen);
	return dest;
}

static char *
form_dtm_argument(PQExpBufferData *str, char **val) {

	char *buffer = NULL;

	if (*val == NULL) {
		return "null";
	}

	short len = strlen(*val);
	if (len != 0 && (*val)[0] == '\'' && (*val)[len - 1] == '\'') {
		/* Skip the formatting if the value is quoted */
		return *val;
	}

	buffer = (char *) malloc(len + 3);

	if (buffer == NULL) {
		/* reset the buffer data len to skip the formatting and throw memory allocation failure */
		str->maxlen = 0;
		return NULL;
	}

	buffer[0] = '\0';
	sprintf(buffer, "'%s'", *val);

	if (*val) {
		free(*val);
	}

	*val = buffer;
	return *val;
}

RETCODE		SQL_API
PGAPI_Tables(HSTMT hstmt,
			 const SQLCHAR * szTableQualifier, /* PV X*/
			 SQLSMALLINT cbTableQualifier,
			 const SQLCHAR * szTableOwner, /* PV E*/
			 SQLSMALLINT cbTableOwner,
			 const SQLCHAR * szTableName, /* PV E*/
			 SQLSMALLINT cbTableName,
			 const SQLCHAR * szTableType,
			 SQLSMALLINT cbTableType,
			 UWORD	flag)
{
	CSTR func = "PGAPI_Tables";
	StatementClass *stmt = (StatementClass *) hstmt;
	StatementClass *tbl_stmt = NULL;
	QResultClass	*res;
	TupleField	*tuple;
	RETCODE		ret = SQL_ERROR, result;
	int		result_cols;
	char		*tableType = NULL;
	PQExpBufferData		tables_query = {0};
	char		table_catalog[MAX_INFO_STRING],
				table_schema[MAX_INFO_STRING],
				table_name[MAX_INFO_STRING],
				table_type[MAX_INFO_STRING],
				remarks[MAX_INFO_STRING];
	ConnectionClass *conn;
	char	*escCatName = NULL, *escSchemaName = NULL, *escTableName = NULL;
	SQLSMALLINT		internal_asis_type = SQL_C_CHAR, cbSchemaName;
	const SQLCHAR *szSchemaName;
	BOOL		search_pattern;
	EnvironmentClass *env;
	SQLLEN catalog_len, schema_len, table_len, type_len, remarks_len;

	MYLOG(0, "entering...stmt=%p scnm=%p len=%d\n", stmt, szTableOwner, cbTableOwner);

	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

	conn = SC_get_conn(stmt);
	env = CC_get_env(conn);

	result = PGAPI_AllocStmt(conn, (HSTMT *) &tbl_stmt, 0);
	if (!SQL_SUCCEEDED(result))
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate statement for PGAPI_Tables result.", func);
		return SQL_ERROR;
	}
	szSchemaName = szTableOwner;
	cbSchemaName = cbTableOwner;

#define	return	DONT_CALL_RETURN_FROM_HERE???
	search_pattern = (0 == (flag & PODBC_NOT_SEARCH_PATTERN));
	if (search_pattern)
	{
		escCatName = adjustLikePattern(szTableQualifier, cbTableQualifier, conn);
		escTableName = adjustLikePattern(szTableName, cbTableName, conn);
		escSchemaName = adjustLikePattern(szSchemaName, cbSchemaName, conn);
	}
	else
	{
		escCatName = simpleCatalogEscape(szTableQualifier, cbTableQualifier, conn);
		escTableName = simpleCatalogEscape(szTableName, cbTableName, conn);
		escSchemaName = simpleCatalogEscape(szSchemaName, cbSchemaName, conn);
	}

	/*
	 * Create the query to find out the tables
	 */
	/* make_string mallocs memory */
	tableType = make_string(szTableType, cbTableType, NULL, 0);

	initPQExpBuffer(&tables_query);
#define	return	DONT_CALL_RETURN_FROM_HERE???

	printfPQExpBuffer(&tables_query,
		"dtm odbc sqltables %s %s %s %s",
		form_dtm_argument(&tables_query, &escCatName),
		form_dtm_argument(&tables_query, &escSchemaName),
		form_dtm_argument(&tables_query, &escTableName),
		form_dtm_argument(&tables_query, &tableType));

	if (PQExpBufferDataBroken(tables_query))
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Out of memory in PGAPI_Tables()", func);
		goto cleanup;
	}
	result = PGAPI_ExecDirect(tbl_stmt, (SQLCHAR *) tables_query.data, SQL_NTS, PODBC_RDONLY);
	if (!SQL_SUCCEEDED(result))
	{
		SC_full_error_copy(stmt, tbl_stmt, FALSE);
		goto cleanup;
	}

#ifdef	UNICODE_SUPPORT
	if (CC_is_in_unicode_driver(conn))
		internal_asis_type = INTERNAL_ASIS_TYPE;
#endif /* UNICODE_SUPPORT */

	// Column 1: table_catalog - string
	result = PGAPI_BindCol(tbl_stmt, 1, internal_asis_type,
			table_catalog, MAX_INFO_STRING, &catalog_len);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 2: table_schema - string
	result = PGAPI_BindCol(tbl_stmt, 2, internal_asis_type,
						   table_schema, MAX_INFO_STRING, &schema_len);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 3: table_name - string
	result = PGAPI_BindCol(tbl_stmt, 3, internal_asis_type,
			table_name, MAX_INFO_STRING, &table_len);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 4: table_type - string
	result = PGAPI_BindCol(tbl_stmt, 4, internal_asis_type,
						   table_type, MAX_INFO_STRING, &type_len);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 5: remarks - string
	result = PGAPI_BindCol(tbl_stmt, 5, internal_asis_type,
						   remarks, MAX_INFO_STRING, &remarks_len);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	if (res = QR_Constructor(), !res)
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for PGAPI_Tables result.", func);
		goto cleanup;
	}
	SC_set_Result(stmt, res);

	/* the binding structure for a statement is not set up until */

	/*
	 * a statement is actually executed, so we'll have to do this
	 * ourselves.
	 */
	result_cols = NUM_OF_TABLES_FIELDS;
	extend_column_bindings(SC_get_ARDF(stmt), result_cols);

	stmt->catalog_result = TRUE;
	/* set the field names */
	QR_set_num_fields(res, result_cols);
	if (EN_is_odbc3(env))
	{
		QR_set_field_info_v(res, TABLES_CATALOG_NAME, "TABLE_CAT", PG_TYPE_VARCHAR, MAX_INFO_STRING);
		QR_set_field_info_v(res, TABLES_SCHEMA_NAME, "TABLE_SCHEM", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	}
	else
	{
		QR_set_field_info_v(res, TABLES_CATALOG_NAME, "TABLE_QUALIFIER", PG_TYPE_VARCHAR, MAX_INFO_STRING);
		QR_set_field_info_v(res, TABLES_SCHEMA_NAME, "TABLE_OWNER", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	}
	QR_set_field_info_v(res, TABLES_TABLE_NAME, "TABLE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, TABLES_TABLE_TYPE, "TABLE_TYPE", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, TABLES_REMARKS, "REMARKS", PG_TYPE_VARCHAR, INFO_VARCHAR_SIZE);

	/* add the tuples */
	result = PGAPI_Fetch(tbl_stmt);
	while (SQL_SUCCEEDED(result))
	{

		MYLOG(0, "table_catalog='%s', table_schema='%s', table_name = '%s', table_type='%s'\n",
			table_catalog, table_schema, table_name, table_type);

		tuple = QR_AddNew(res);
		if (catalog_len != SQL_NULL_DATA)
 			set_tuplefield_string(&tuple[TABLES_CATALOG_NAME], table_catalog);

		if (schema_len != SQL_NULL_DATA)
 			set_tuplefield_string(&tuple[TABLES_SCHEMA_NAME], table_schema);

		if (table_len != SQL_NULL_DATA)
 			set_tuplefield_string(&tuple[TABLES_TABLE_NAME], table_name);

		if (type_len != SQL_NULL_DATA)
 			set_tuplefield_string(&tuple[TABLES_TABLE_TYPE], table_type);

		if (remarks_len != SQL_NULL_DATA)
		    set_tuplefield_string(&tuple[TABLES_REMARKS], remarks);

		result = PGAPI_Fetch(tbl_stmt);
	}
	if (result != SQL_NO_DATA_FOUND)
	{
		SC_full_error_copy(stmt, tbl_stmt, FALSE);
		goto cleanup;
	}
	ret = SQL_SUCCESS;

cleanup:
#undef	return
	/*
	 * also, things need to think that this statement is finished so the
	 * results can be retrieved.
	 */
	stmt->status = STMT_FINISHED;

	if (!SQL_SUCCEEDED(ret) && 0 >= SC_get_errornumber(stmt))
		SC_error_copy(stmt, tbl_stmt, TRUE);
	if (!PQExpBufferDataBroken(tables_query))
		termPQExpBuffer(&tables_query);
	if (escCatName)
		free(escCatName);
	if (escSchemaName)
		free(escSchemaName);
	if (escTableName)
		free(escTableName);
	if (tableType)
		free(tableType);
	/* set up the current tuple pointer for SQLFetch */
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	SC_set_current_col(stmt, -1);

	if (tbl_stmt)
		PGAPI_FreeStmt(tbl_stmt, SQL_DROP);

	MYLOG(0, "leaving stmt=%p, ret=%d\n", stmt, ret);
	return ret;
}

/*
 *	macros for pgtype_attr_xxxx() calls which have
 *		PG_ADT_UNSET or PG_UNKNOWNS_UNSET parameters
 */
#define PGTYPE_ATTR_COLUMN_SIZE(conn, pgType, atttypmod) pgtype_attr_column_size(conn, pgType, atttypmod, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_ATTR_TO_CONCISE_TYPE(conn, pgType, atttypmod) pgtype_attr_to_concise_type(conn, pgType, atttypmod, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_ATTR_TO_SQLDESCTYPE(conn, pgType, atttypmod) pgtype_attr_to_sqldesctype(conn, pgType, atttypmod, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_ATTR_DISPLAY_SIZE(conn, pgType, atttypmod) pgtype_attr_display_size(conn, pgType, atttypmod, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_ATTR_BUFFER_LENGTH(conn, pgType, atttypmod) pgtype_attr_buffer_length(conn, pgType, atttypmod, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_ATTR_DECIMAL_DIGITS(conn, pgType, atttypmod) pgtype_attr_decimal_digits(conn, pgType, atttypmod, PG_ADT_UNSET, PG_UNKNOWNS_UNSET)
#define PGTYPE_ATTR_TRANSFER_OCTET_LENGTH(conn, pgType, atttypmod) pgtype_attr_transfer_octet_length(conn, pgType, atttypmod, PG_UNKNOWNS_UNSET)
// Since (I think) we are using UTF8 to communicate the characters in our
// network protocol, the drivers should probably report 4x the character length
// as the maximum octet size for string types.
// HQ-6699
#define BYTES_PER_CHARACTER 4

/*----------
 * Some Notes about Postgres Data Types:
 *
 * VARCHAR - the length is stored in the pg_attribute.atttypmod field
 * BPCHAR  - the length is also stored as varchar is
 *
 * NUMERIC - the decimal_digits is stored in atttypmod as follows:
 *
 *	column_size =((atttypmod - VARHDRSZ) >> 16) & 0xffff
 *	decimal_digits	 = (atttypmod - VARHDRSZ) & 0xffff
 *
 *----------
 */
Int4 build_mod_length(Int4 character_length, SQLLEN cbCharacterLength, OID odbc_type, Int4 numeric_precision, SQLLEN cbNumericPrecision, Int4 numeric_scale, SQLLEN cbNumericScale)
{
	Int4 mod_length;
	Int4 precision_segment, scale_segment;

	switch(odbc_type)
	{
		case SQL_NUMERIC:
		case SQL_DECIMAL:
			if (SQL_NULL_DATA == cbNumericPrecision || SQL_NULL_DATA == cbNumericScale)
				mod_length = -1;
			else
			{
				precision_segment = (numeric_precision & 0xFFFF) << 16;
				scale_segment = numeric_scale & 0XFFFF;
				mod_length = precision_segment | scale_segment;
			}
			break;

#ifdef	UNICODE_SUPPORT
		case SQL_WCHAR:
		case SQL_WVARCHAR:
		case SQL_WLONGVARCHAR:
#endif /* UNICODE_SUPPORT */
		case SQL_CHAR:
		case SQL_VARCHAR:
		case SQL_LONGVARCHAR:
			if (cbCharacterLength == SQL_NULL_DATA)
				mod_length = -1;
			else
				mod_length = character_length;

			break;
		default:
			mod_length = -1;
	}

	return mod_length;
}

RETCODE		SQL_API
PGAPI_Columns(HSTMT hstmt,
			  const SQLCHAR * szTableQualifier, /* OA X*/
			  SQLSMALLINT cbTableQualifier,
			  const SQLCHAR * szTableOwner, /* PV E*/
			  SQLSMALLINT cbTableOwner,
			  const SQLCHAR * szTableName, /* PV E*/
			  SQLSMALLINT cbTableName,
			  const SQLCHAR * szColumnName, /* PV E*/
			  SQLSMALLINT cbColumnName,
			  UWORD	flag)
{
	CSTR func = "PGAPI_Columns";
	StatementClass *stmt = (StatementClass *) hstmt;
	QResultClass	*res;
	TupleField	*tuple;
	StatementClass *col_stmt = NULL;
	PQExpBufferData		columns_query = {0};
	RETCODE		ret = SQL_ERROR, result;
	char		table_catalog[MAX_INFO_STRING],
				table_schema[MAX_INFO_STRING],
				table_name[MAX_INFO_STRING],
				column_name[MAX_INFO_STRING],
				data_type_name[MAX_INFO_STRING],
				is_nullable[MAX_INFO_STRING];
	Int2		result_cols;
	Int4		mod_length,
				ordinal_position,
				numeric_precision,
				numeric_scale,
				character_length,
				binary_length;
	SQLLEN 		cbNumericPrecision,
				cbNumericScale,
				cbBinaryLength,
				cbCharacterLength;
	OID			odbc_type, pgtype;
	char	*escSchemaName = NULL, *escTableName = NULL, *escColumnName = NULL;
	BOOL	search_pattern = TRUE;
	ConnectionClass *conn;
	SQLSMALLINT	internal_asis_type = SQL_C_CHAR, cbSchemaName;
	const SQLCHAR *szSchemaName;

	MYLOG(0, "entering...stmt=%p scnm=%p len=%d columnOpt=%x\n", stmt, szTableOwner, cbTableOwner, flag);

	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

	conn = SC_get_conn(stmt);
#ifdef	UNICODE_SUPPORT
	if (CC_is_in_unicode_driver(conn))
		internal_asis_type = INTERNAL_ASIS_TYPE;
#endif /* UNICODE_SUPPORT */

#define	return	DONT_CALL_RETURN_FROM_HERE???
	search_pattern = ((flag & PODBC_NOT_SEARCH_PATTERN) == 0);

	szSchemaName = szTableOwner;
	cbSchemaName = cbTableOwner;
	/*
	 *	TableName or ColumnName is ordinarily an pattern value,
	 */
	if (search_pattern)
	{
		escTableName = adjustLikePattern(szTableName, cbTableName, conn);
		escColumnName = adjustLikePattern(szColumnName, cbColumnName, conn);
		escSchemaName = adjustLikePattern(szSchemaName, cbSchemaName, conn);
	}
	else
	{
		escTableName = simpleCatalogEscape(szTableName, cbTableName, conn);
		escColumnName = simpleCatalogEscape(szColumnName, cbColumnName, conn);
		escSchemaName = simpleCatalogEscape(szSchemaName, cbSchemaName, conn);
	}

	initPQExpBuffer(&columns_query);
#define	return	DONT_CALL_RETURN_FROM_HERE???
	/*
	 * Create the query to find out the columns
	 */
	printfPQExpBuffer(&columns_query,
		"dtm odbc sqlcolumns '%s' '%s' '%s' '%s'",
		NULL_TO_EMPTY_STRING(szTableQualifier),
		NULL_TO_EMPTY_STRING(escSchemaName),
		NULL_TO_EMPTY_STRING(escTableName),
		NULL_TO_EMPTY_STRING(escColumnName));

	if (PQExpBufferDataBroken(columns_query))
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Out of memory in PGAPI_Columns()", func);
		goto cleanup;
	}

	result = PGAPI_AllocStmt(conn, (HSTMT *) &col_stmt, 0);
	if (!SQL_SUCCEEDED(result))
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate statement for PGAPI_Columns result.", func);
		goto cleanup;
	}

	MYLOG(0, "col_stmt = %p\n", col_stmt);

	result = PGAPI_ExecDirect(col_stmt, (SQLCHAR *) columns_query.data, SQL_NTS, PODBC_RDONLY);
	if (!SQL_SUCCEEDED(result))
	{
		SC_full_error_copy(stmt, col_stmt, FALSE);
		goto cleanup;
	}

	// Column 1: table_catalog - string
	result = PGAPI_BindCol(col_stmt, 1, internal_asis_type,
						   table_catalog, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 2: table_schema - string
	result = PGAPI_BindCol(col_stmt, 2, internal_asis_type,
						   table_schema, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 3: table_name - string
	result = PGAPI_BindCol(col_stmt, 3, internal_asis_type,
						   table_name, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 4: column_name - string
	result = PGAPI_BindCol(col_stmt, 4, internal_asis_type,
						   column_name, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 5: data_type - string
	result = PGAPI_BindCol(col_stmt, 5, internal_asis_type,
						   data_type_name, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 6: is_nullable - YES/NO
	result = PGAPI_BindCol(col_stmt, 6, internal_asis_type,
						   is_nullable, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 7: column_default - String (nullable)
	/* Since the length can be quite big...
	   we can find the extracting code in a few lines
	*/

	// Column 8: numeric_precision - integer (nullable)
	result = PGAPI_BindCol(col_stmt, 8, SQL_C_LONG,
						   &numeric_precision, MAX_INFO_STRING, &cbNumericPrecision);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 9: numeric_scale - integer (nullable)
	result = PGAPI_BindCol(col_stmt, 9, SQL_C_LONG,
						   &numeric_scale, MAX_INFO_STRING, &cbNumericScale);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 10: character_length - integer (nullable)
	result = PGAPI_BindCol(col_stmt, 10, SQL_C_LONG,
						   &character_length, MAX_INFO_STRING,
						   &cbCharacterLength);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 11: binary_length - integer (nullable)
	result = PGAPI_BindCol(col_stmt, 11, SQL_C_LONG,
						   &binary_length, MAX_INFO_STRING,
						   &cbBinaryLength);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 12: ordinal_position - integer
	result = PGAPI_BindCol(col_stmt, 12, SQL_C_LONG,
						   &ordinal_position, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	// Column 13: odbc_type - OID(integer)
	result = PGAPI_BindCol(col_stmt, 13, SQL_C_LONG,
						   &odbc_type, MAX_INFO_STRING, NULL);

	if (!SQL_SUCCEEDED(result))
	{
		goto cleanup;
	}

	if (res = QR_Constructor(), !res)
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for PGAPI_Columns result.", func);
		goto cleanup;
	}
	SC_set_Result(stmt, res);

	/*
	 * the binding structure for a statement is not set up until
	 * a statement is actually executed, so we'll have to do this
	 * ourselves.
	 */
	result_cols = NUM_OF_COLUMNS_FIELDS;
	extend_column_bindings(SC_get_ARDF(stmt), result_cols);

	/*
	 * Setting catalog_result here affects the behavior of
	 * pgtype_xxx() functions. So set it later.
	 * stmt->catalog_result = TRUE;
	 */
	/* set the field names */
	QR_set_num_fields(res, result_cols);
	QR_set_field_info_v(res, COLUMNS_CATALOG_NAME, "TABLE_CAT", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLUMNS_SCHEMA_NAME, "TABLE_SCHEM", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLUMNS_TABLE_NAME, "TABLE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLUMNS_COLUMN_NAME, "COLUMN_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLUMNS_DATA_TYPE, "DATA_TYPE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, COLUMNS_TYPE_NAME, "TYPE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLUMNS_SIZE, "COLUMN_SIZE", PG_TYPE_INT4, 4); /* COLUMN_SIZE */
	QR_set_field_info_v(res, COLUMNS_BUFFER_LENGTH, "BUFFER_LENGTH", PG_TYPE_INT4, 4); /* BUFFER_LENGTH */
	QR_set_field_info_v(res, COLUMNS_DECIMAL_DIGITS, "DECIMAL_DIGITS", PG_TYPE_INT2, 2); /* DECIMAL_DIGITS ***/
	QR_set_field_info_v(res, COLUMNS_NUM_PREC_RADIX, "NUM_PREC_RADIX", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, COLUMNS_NULLABLE, "NULLABLE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, COLUMNS_REMARKS, "REMARKS", PG_TYPE_VARCHAR, INFO_VARCHAR_SIZE);
	QR_set_field_info_v(res, COLUMNS_COLUMN_DEF, "COLUMN_DEF", PG_TYPE_VARCHAR, INFO_VARCHAR_SIZE);
	QR_set_field_info_v(res, COLUMNS_SQL_DATA_TYPE, "SQL_DATA_TYPE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, COLUMNS_SQL_DATETIME_SUB, "SQL_DATETIME_SUB", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, COLUMNS_CHAR_OCTET_LENGTH, "CHAR_OCTET_LENGTH", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, COLUMNS_ORDINAL_POSITION, "ORDINAL_POSITION", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, COLUMNS_IS_NULLABLE, "IS_NULLABLE", PG_TYPE_VARCHAR, INFO_VARCHAR_SIZE);

	/* PG defined fields, required by other parts of the driver */
	QR_set_field_info_v(res, COLUMNS_DISPLAY_SIZE, "DISPLAY_SIZE", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, COLUMNS_PG_TYPE, "PG_TYPE", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, COLUMNS_AUTO_INCREMENT, "AUTO_INCREMENT", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, COLUMNS_ATTTYPMOD, "TYPMOD", PG_TYPE_INT4, 4);

	result = PGAPI_Fetch(col_stmt);
	while (SQL_SUCCEEDED(result))
	{
		SQLLEN	att_default_len_needed;
		char	*attdef = malloc(1);
        if (!attdef)
        {
            SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for attdef.", func);
            goto cleanup;
        }
		// Column 7: column_default - String (nullable)
		*attdef = '\0';
		PGAPI_SetPos(col_stmt, 1, SQL_POSITION, 0);
		PGAPI_GetData(col_stmt, 7, internal_asis_type, NULL, 0, &att_default_len_needed);
		if (att_default_len_needed > 0)
		{
            free(attdef);
			MYLOG(0, "att_default_len_needed=" FORMAT_LEN "\n", att_default_len_needed);
			if( att_default_len_needed <= INFO_VARCHAR_SIZE)
			{
				attdef = malloc(att_default_len_needed + 1);
				if (!attdef)
				{
					SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for attdef.", func);
					goto cleanup;
				}
				PGAPI_GetData(col_stmt, 7, internal_asis_type, attdef, att_default_len_needed + 1, &att_default_len_needed);
			}
			MYLOG(0, " and the data=%s\n", attdef);
		}

		tuple = QR_AddNew(res);

		mod_length = build_mod_length(character_length, cbCharacterLength, odbc_type, numeric_precision, cbNumericPrecision, numeric_scale, cbNumericScale);
		pgtype = sqltype_to_pgtype(conn, odbc_type);
		MYLOG(0, "table_name='%s',column_name='%s',odbc_type=%d,pgtype=%d,data_type_name='%s'\n",
			 table_name, column_name, odbc_type, pgtype, data_type_name);

		set_tuplefield_string(&tuple[COLUMNS_CATALOG_NAME], table_catalog);
		/* see note in SQLTables() */
		set_tuplefield_string(&tuple[COLUMNS_SCHEMA_NAME], table_schema);
		set_tuplefield_string(&tuple[COLUMNS_TABLE_NAME], table_name);
		set_tuplefield_string(&tuple[COLUMNS_COLUMN_NAME], column_name);
		set_tuplefield_int2(&tuple[COLUMNS_DATA_TYPE], odbc_type);
		set_tuplefield_string(&tuple[COLUMNS_TYPE_NAME], data_type_name);
		set_tuplefield_int4(&tuple[COLUMNS_SIZE], PGTYPE_ATTR_COLUMN_SIZE(conn, pgtype, mod_length));
		set_tuplefield_int4(&tuple[COLUMNS_BUFFER_LENGTH], PGTYPE_ATTR_BUFFER_LENGTH(conn, pgtype, mod_length));
		set_nullfield_int2(&tuple[COLUMNS_DECIMAL_DIGITS], PGTYPE_ATTR_DECIMAL_DIGITS(conn, pgtype, mod_length));
		set_nullfield_int2(&tuple[COLUMNS_NUM_PREC_RADIX], pgtype_radix(conn, pgtype));
		set_tuplefield_int2(&tuple[COLUMNS_NULLABLE], (Int2) (strcmp(is_nullable, "YES") != 0 ? SQL_NO_NULLS : SQL_NULLABLE));
		set_tuplefield_string(&tuple[COLUMNS_REMARKS], NULL_STRING);
		if (att_default_len_needed > INFO_VARCHAR_SIZE)
			set_tuplefield_string(&tuple[COLUMNS_COLUMN_DEF], "TRUNCATE");
		else
			set_tuplefield_string(&tuple[COLUMNS_COLUMN_DEF], attdef);
		set_tuplefield_int2(&tuple[COLUMNS_SQL_DATA_TYPE], odbc_type);
		set_nullfield_int2(&tuple[COLUMNS_SQL_DATETIME_SUB], pgtype_attr_to_datetime_sub(conn, pgtype, mod_length));
		if (character_length == SQL_NULL_DATA)
			set_tuplefield_null(&tuple[COLUMNS_CHAR_OCTET_LENGTH]);
		else
			set_tuplefield_int4(&tuple[COLUMNS_CHAR_OCTET_LENGTH], character_length);
		set_tuplefield_int4(&tuple[COLUMNS_ORDINAL_POSITION], ordinal_position);
		set_tuplefield_string(&tuple[COLUMNS_IS_NULLABLE], is_nullable);
		set_tuplefield_int4(&tuple[COLUMNS_DISPLAY_SIZE], PGTYPE_ATTR_DISPLAY_SIZE(conn, pgtype, mod_length));
		set_tuplefield_int4(&tuple[COLUMNS_PG_TYPE], pgtype);
		// HQ-6703 We don't have a way to know (yet) if the column is auto incremented
		set_tuplefield_int4(&tuple[COLUMNS_AUTO_INCREMENT], 0);
		set_tuplefield_int4(&tuple[COLUMNS_ATTTYPMOD], mod_length);

		result = PGAPI_Fetch(col_stmt);
		if (attdef)
			free(attdef);
	}
	if (result != SQL_NO_DATA_FOUND)
	{
		SC_full_error_copy(stmt, col_stmt, FALSE);
		goto cleanup;
	}

	ret = SQL_SUCCESS;

cleanup:
#undef	return
	/*
	 * also, things need to think that this statement is finished so the
	 * results can be retrieved.
	 */
	stmt->status = STMT_FINISHED;
	stmt->catalog_result = TRUE;

	if (!SQL_SUCCEEDED(ret) && 0 >= SC_get_errornumber(stmt))
		SC_error_copy(stmt, col_stmt, TRUE);
	/* set up the current tuple pointer for SQLFetch */
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	SC_set_current_col(stmt, -1);

	if (!PQExpBufferDataBroken(columns_query))
		termPQExpBuffer(&columns_query);
	if (escSchemaName)
		free(escSchemaName);
	if (escTableName)
		free(escTableName);
	if (escColumnName)
		free(escColumnName);
	if (col_stmt)
		PGAPI_FreeStmt(col_stmt, SQL_DROP);
	MYLOG(0, "leaving stmt=%p\n", stmt);
	return ret;
}


RETCODE		SQL_API
PGAPI_SpecialColumns(HSTMT hstmt,
					 SQLUSMALLINT fColType,
					 const SQLCHAR * szTableQualifier,
					 SQLSMALLINT cbTableQualifier,
					 const SQLCHAR * szTableOwner, /* OA E*/
					 SQLSMALLINT cbTableOwner,
					 const SQLCHAR * szTableName, /* OA(R) E*/
					 SQLSMALLINT cbTableName,
					 SQLUSMALLINT fScope,
					 SQLUSMALLINT fNullable)
{
	CSTR func = "PGAPI_SpecialColumns";
	StatementClass *stmt = (StatementClass *) hstmt;
	QResultClass	*res;
	StatementClass *col_stmt = NULL;
	PQExpBufferData		columns_query = {0};
	char		*escSchemaName = NULL, *escTableName = NULL;
	RETCODE		ret = SQL_ERROR, result;

	MYLOG(0, "entering...stmt=%p scnm=%p len=%d colType=%d scope=%d\n", stmt, szTableOwner, cbTableOwner, fColType, fScope);

	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

#define	return	DONT_CALL_RETURN_FROM_HERE???
	res = QR_Constructor();
	if (!res)
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for query.", func);
		goto cleanup;
	}
	SC_set_Result(stmt, res);
	extend_column_bindings(SC_get_ARDF(stmt), NUM_OF_SPECOLS_FIELDS);

	stmt->catalog_result = TRUE;
	QR_set_num_fields(res, NUM_OF_SPECOLS_FIELDS);
	QR_set_field_info_v(res, SPECOLS_SCOPE, "SCOPE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, SPECOLS_COLUMN_NAME, "COLUMN_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, SPECOLS_DATA_TYPE, "DATA_TYPE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, SPECOLS_TYPE_NAME, "TYPE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, SPECOLS_COLUMN_SIZE, "COLUMN_SIZE", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, SPECOLS_BUFFER_LENGTH, "BUFFER_LENGTH", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, SPECOLS_DECIMAL_DIGITS, "DECIMAL_DIGITS", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, SPECOLS_PSEUDO_COLUMN, "PSEUDO_COLUMN", PG_TYPE_INT2, 2);

	ret = SQL_SUCCESS;

cleanup:
#undef	return
	if (!SQL_SUCCEEDED(ret) && 0 >= SC_get_errornumber(stmt))
		SC_error_copy(stmt, col_stmt, TRUE);
	if (!PQExpBufferDataBroken(columns_query))
		termPQExpBuffer(&columns_query);
	if (escSchemaName)
		free(escSchemaName);
	if (escTableName)
		free(escTableName);
	stmt->status = STMT_FINISHED;
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	SC_set_current_col(stmt, -1);
	if (col_stmt)
		PGAPI_FreeStmt(col_stmt, SQL_DROP);
	MYLOG(0, "leaving  stmt=%p\n", stmt);
	return ret;
}


#define INDOPTION_DESC		0x0001	/* values are in reverse order */
#define IS_INDEX_UNIQUE(sz) (strchr((sz), 'U') != NULL)
#define IS_INDEX_PRIMARY(sz) (strchr((sz), 'P') != NULL)
#define IS_INDEX_CLUSTERED(sz) (strchr((sz), 'C') != NULL)
#define IS_INDEX_BTREE(sz) (strchr((sz), 'B') != NULL)
#define IS_INDEX_HASH(sz) (strchr((sz), 'H') != NULL)
#define IS_INDEX_OTHER(sz) ((strchr((sz), 'O') != NULL) || !(IS_INDEX_CLUSTERED(sz) || IS_INDEX_BTREE(sz) || IS_INDEX_HASH(sz)))
RETCODE		SQL_API
PGAPI_Statistics(HSTMT hstmt,
				 const SQLCHAR * szTableQualifier, /* OA X*/
				 SQLSMALLINT cbTableQualifier,
				 const SQLCHAR * szTableOwner, /* OA E*/
				 SQLSMALLINT cbTableOwner,
				 const SQLCHAR * szTableName, /* OA(R) E*/
				 SQLSMALLINT cbTableName,
				 SQLUSMALLINT fUnique,
				 SQLUSMALLINT fAccuracy)
{
	CSTR func = "PGAPI_Statistics";
	StatementClass *stmt = (StatementClass *) hstmt;
	ConnectionClass *conn;
	QResultClass	*res;
	PQExpBufferData		index_query = {0};
	RETCODE		ret = SQL_ERROR, result;
	char		*table_name = NULL, *escTableName = NULL;
	char		*szOptions = NULL;
	char		szTable_catalog[MAX_INFO_STRING],
	    		szTable_schema[MAX_INFO_STRING],
	    		szTable_name[MAX_INFO_STRING],
	    		szIndex_qualifier[MAX_INFO_STRING],
	    		szIndex_name[MAX_INFO_STRING],
	    		szType[MAX_INFO_STRING],
	    		szColumn_name[MAX_INFO_STRING],
	    		szFilter_condition[MAX_INFO_STRING],
	    		szAsc_or_desc[MAX_INFO_STRING];
	SQLSMALLINT iOrdinal_position;
	SQLINTEGER	iCardinality,
	          	iPages;
	SQLLEN		index_qualifier_len,
	      		type_len,
	      		cardinality_len,
	      		pages_len,
	      		filter_condition_len;
	SQLSMALLINT iIndex_type;
	TupleField	*tuple;
	StatementClass *indx_stmt = NULL;
	SQLSMALLINT	internal_asis_type = SQL_C_CHAR;

	MYLOG(0, "entering...stmt=%p scnm=%p len=%d\n", stmt, szTableOwner, cbTableOwner);

	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

	table_name = make_string(szTableName, cbTableName, NULL, 0);
	if (!table_name)
	{
		SC_set_error(stmt, STMT_INVALID_NULL_ARG, "The table name is required", func);
		return result;
	}
	conn = SC_get_conn(stmt);
#ifdef	UNICODE_SUPPORT
	if (CC_is_in_unicode_driver(conn))
		internal_asis_type = INTERNAL_ASIS_TYPE;
#endif /* UNICODE_SUPPORT */

	if (res = QR_Constructor(), !res)
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for PGAPI_Statistics result.", func);
		free(table_name);
		return SQL_ERROR;
	}
	SC_set_Result(stmt, res);

	/* the binding structure for a statement is not set up until */

	/*
	 * a statement is actually executed, so we'll have to do this
	 * ourselves.
	 */
	extend_column_bindings(SC_get_ARDF(stmt), NUM_OF_STATS_FIELDS);

	stmt->catalog_result = TRUE;
	/* set the field names */
	QR_set_num_fields(res, NUM_OF_STATS_FIELDS);
	QR_set_field_info_v(res, STATS_CATALOG_NAME, "TABLE_CAT", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, STATS_SCHEMA_NAME, "TABLE_SCHEM", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, STATS_TABLE_NAME, "TABLE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, STATS_NON_UNIQUE, "NON_UNIQUE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, STATS_INDEX_QUALIFIER, "INDEX_QUALIFIER", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, STATS_INDEX_NAME, "INDEX_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, STATS_TYPE, "TYPE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, STATS_ORDINAL_POSITION, "ORDINAL_POSITION", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, STATS_COLUMN_NAME, "COLUMN_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, STATS_ASC_OR_DESC, "ASC_OR_DESC", PG_TYPE_CHAR, 1);
	QR_set_field_info_v(res, STATS_CARDINALITY, "CARDINALITY", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, STATS_PAGES, "PAGES", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, STATS_FILTER_CONDITION, "FILTER_CONDITION", PG_TYPE_VARCHAR, MAX_INFO_STRING);

#define	return	DONT_CALL_RETURN_FROM_HERE???

	/* get a list of indexes on this table */
	result = PGAPI_AllocStmt(conn, (HSTMT *) &indx_stmt, 0);
	if (!SQL_SUCCEEDED(result))
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "PGAPI_AllocStmt failed in SQLStatistics for indices.", func);
		goto cleanup;

	}

	/* TableName cannot contain a string search pattern */
	escTableName = simpleCatalogEscape((SQLCHAR *) table_name, SQL_NTS, conn);
	if (fUnique == SQL_INDEX_UNIQUE)
		szOptions = "unique";
	initPQExpBuffer(&index_query);
	printfPQExpBuffer(&index_query,
		"dtm odbc sqlstatistics '%s' '%s' '%s' %s",
		NULL_TO_EMPTY_STRING(szTableQualifier),
		NULL_TO_EMPTY_STRING(szTableOwner),
		NULL_TO_EMPTY_STRING(escTableName),
		NULL_TO_EMPTY_STRING(szOptions));
	if (PQExpBufferDataBroken(index_query))
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Out of memory in PGAPI_Columns()", func);
		goto cleanup;
	}

	result = PGAPI_ExecDirect(indx_stmt, (SQLCHAR *) index_query.data, SQL_NTS, PODBC_RDONLY);
	if (!SQL_SUCCEEDED(result))
	{
		/*
		 * "Couldn't execute index query (w/SQLExecDirect) in
		 * SQLStatistics.";
		 */
		SC_full_error_copy(stmt, indx_stmt, FALSE);
		goto cleanup;
	}

	// Column 1: table_catalog - varchar
	result = PGAPI_BindCol(indx_stmt, 1, internal_asis_type,
						   szTable_catalog, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 2: table_schema - varchar
	result = PGAPI_BindCol(indx_stmt, 2, internal_asis_type,
						   szTable_schema, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 3: table_name - varchar
	result = PGAPI_BindCol(indx_stmt, 3, internal_asis_type,
						   szTable_name, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 4: index_qualifier - varchar (nullable)
	result = PGAPI_BindCol(indx_stmt, 4, internal_asis_type,
						   szIndex_qualifier, MAX_INFO_STRING, &index_qualifier_len);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 5: index_name - varchar
	result = PGAPI_BindCol(indx_stmt, 5, internal_asis_type,
						   szIndex_name, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 6: type - varchar (nullable)
	result = PGAPI_BindCol(indx_stmt, 6, internal_asis_type,
						   szType, MAX_INFO_STRING, &type_len);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 7: ordinal_position - smallint
	result = PGAPI_BindCol(indx_stmt, 7, SQL_C_SHORT,
					&iOrdinal_position, sizeof(iOrdinal_position), NULL);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 8: column_name - varchar
	result = PGAPI_BindCol(indx_stmt, 7, internal_asis_type,
					szColumn_name, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 9: asc_or_desc - char(1)
	result = PGAPI_BindCol(indx_stmt, 9, internal_asis_type,
					szAsc_or_desc, MAX_INFO_STRING, NULL);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 10: cardinality - integer(4)
	result = PGAPI_BindCol(indx_stmt, 10, SQL_C_LONG,
					&iCardinality, sizeof(iCardinality), &cardinality_len);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 11: pages - integer(4)
	result = PGAPI_BindCol(indx_stmt, 11, SQL_C_LONG,
					&iPages, sizeof(iPages), &pages_len);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	// Column 12: filter_condition - varchar (nullable)
	result = PGAPI_BindCol(indx_stmt, 12, SQL_C_DEFAULT,
					&szFilter_condition, MAX_INFO_STRING, &filter_condition_len);
	if (!SQL_SUCCEEDED(result))
		goto cleanup;

	while (SQL_SUCCEEDED(result = PGAPI_Fetch(indx_stmt)))
	{
		tuple = QR_AddNew(res);

		set_tuplefield_string(&tuple[STATS_CATALOG_NAME], szTable_catalog);
		set_tuplefield_string(&tuple[STATS_SCHEMA_NAME], szTable_schema);
		set_tuplefield_string(&tuple[STATS_TABLE_NAME], szTable_name);
		set_tuplefield_int2(&tuple[STATS_NON_UNIQUE], (Int2) !IS_INDEX_UNIQUE(szType));

		if (index_qualifier_len == SQL_NULL_DATA)
			set_tuplefield_null(&tuple[STATS_INDEX_QUALIFIER]);
		else
			set_tuplefield_string(&tuple[STATS_INDEX_QUALIFIER], szIndex_qualifier);
		set_tuplefield_string(&tuple[STATS_INDEX_NAME], szIndex_name);

		// Index type
		if (IS_INDEX_CLUSTERED(szType))
			iIndex_type = SQL_INDEX_CLUSTERED;
		// => For some reason, SQL_INDEX_BTREE is not defined anywhere
		else if(IS_INDEX_HASH(szType))
			iIndex_type = SQL_INDEX_HASHED;
		else if(IS_INDEX_OTHER(szType))
			iIndex_type = SQL_INDEX_OTHER;
		else
			iIndex_type = SQL_INDEX_OTHER;

		set_tuplefield_int2(&tuple[STATS_TYPE], (Int2) iIndex_type);
		set_tuplefield_int2(&tuple[STATS_ORDINAL_POSITION], (Int2) iOrdinal_position);
		set_tuplefield_string(&tuple[STATS_COLUMN_NAME], szColumn_name);
		MYLOG(0, "column name = '%s'\n", szColumn_name);
		set_tuplefield_string(&tuple[STATS_ASC_OR_DESC], szAsc_or_desc);

		if (cardinality_len == SQL_NULL_DATA)
			set_tuplefield_null(&tuple[STATS_CARDINALITY]);
		else
			set_tuplefield_int4(&tuple[STATS_CARDINALITY], iCardinality);

		if (pages_len == SQL_NULL_DATA)
			set_tuplefield_null(&tuple[STATS_PAGES]);
		else
			set_tuplefield_int4(&tuple[STATS_PAGES], iPages);

		if (filter_condition_len == SQL_NULL_DATA)
			set_tuplefield_null(&tuple[STATS_FILTER_CONDITION]);
		else
			set_tuplefield_string(&tuple[STATS_FILTER_CONDITION], szFilter_condition);
	}
	if (result != SQL_NO_DATA_FOUND)
	{
		/* "SQLFetch failed in SQLStatistics."; */
		SC_full_error_copy(stmt, indx_stmt, FALSE);
		goto cleanup;
	}

	ret = SQL_SUCCESS;

cleanup:
#undef	return
	/*
	 * also, things need to think that this statement is finished so the
	 * results can be retrieved.
	 */
	stmt->status = STMT_FINISHED;

	if (indx_stmt)
		PGAPI_FreeStmt(indx_stmt, SQL_DROP);
	/* These things should be freed on any error ALSO! */
	if (!PQExpBufferDataBroken(index_query))
		termPQExpBuffer(&index_query);
	if (table_name)
		free(table_name);
	if (escTableName)
		free(escTableName);

	/* set up the current tuple pointer for SQLFetch */
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	SC_set_current_col(stmt, -1);

	MYLOG(0, "leaving stmt=%p, ret=%d\n", stmt, ret);

	return ret;
}


RETCODE		SQL_API
PGAPI_ColumnPrivileges(HSTMT hstmt,
					   const SQLCHAR * szTableQualifier, /* OA X*/
					   SQLSMALLINT cbTableQualifier,
					   const SQLCHAR * szTableOwner, /* OA E*/
					   SQLSMALLINT cbTableOwner,
					   const SQLCHAR * szTableName, /* OA(R) E*/
					   SQLSMALLINT cbTableName,
					   const SQLCHAR * szColumnName, /* PV E*/
					   SQLSMALLINT cbColumnName,
					   UWORD flag)
{
	CSTR func = "PGAPI_ColumnPrivileges";
	StatementClass	*stmt = (StatementClass *) hstmt;
	RETCODE	ret = SQL_ERROR, result;
	char	*escSchemaName = NULL, *escTableName = NULL, *escColumnName = NULL;
	PQExpBufferData	column_query = {0};
	QResultClass	*res = NULL;

	MYLOG(0, "entering...\n");

	// This function code is never reached due SQLColumnPrivileges
	// is disabled in this driver

	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

	if (res = QR_Constructor(), !res)
	{
		SC_set_error(stmt, STMT_INTERNAL_ERROR, "Error creating result.", func);
		return SQL_ERROR;
	}
	SC_set_Result(stmt, res);

#define	return	DONT_CALL_RETURN_FROM_HERE???
	extend_column_bindings(SC_get_ARDF(stmt), NUM_OF_COLPRIV_FIELDS);

	stmt->catalog_result = TRUE;
	QR_set_num_fields(res, NUM_OF_COLPRIV_FIELDS);
	QR_set_field_info_v(res, COLPRIV_TABLE_CAT, "TABLE_CAT", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLPRIV_TABLE_SCHEM, "TABLE_SCHEM", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLPRIV_TABLE_NAME, "TABLE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLPRIV_COLUMN_NAME, "COLUMN_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLPRIV_GRANTOR, "GRANTOR", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLPRIV_GRANTEE, "GRANTEE", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLPRIV_PRIVILEGE, "PRIVILEGE", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, COLPRIV_IS_GRANTABLE, "IS_GRANTABLE", PG_TYPE_VARCHAR, MAX_INFO_STRING);

	ret = SQL_SUCCESS;
#undef return
	if (!SQL_SUCCEEDED(ret))
		QR_Destructor(res);
	/* set up the current tuple pointer for SQLFetch */
	stmt->status = STMT_FINISHED;
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	if (!PQExpBufferDataBroken(column_query))
		termPQExpBuffer(&column_query);
	if (escSchemaName)
		free(escSchemaName);
	if (escTableName)
		free(escTableName);
	if (escColumnName)
		free(escColumnName);
	return ret;
}


/*
 *	SQLPrimaryKeys()
 *
 *	Retrieve the primary key columns for the specified table.
 */
RETCODE		SQL_API
PGAPI_PrimaryKeys(HSTMT hstmt,
				  const SQLCHAR * szTableQualifier, /* OA X*/
				  SQLSMALLINT cbTableQualifier,
				  const SQLCHAR * szTableOwner, /* OA E*/
				  SQLSMALLINT cbTableOwner,
				  const SQLCHAR * szTableName, /* OA(R) E*/
				  SQLSMALLINT cbTableName,
				  OID	reloid)
{
	CSTR func = "PGAPI_PrimaryKeys";
	StatementClass *stmt = (StatementClass *) hstmt;
	QResultClass	*res;
	RETCODE		ret = SQL_ERROR, result;
	StatementClass *tbl_stmt = NULL;
	PQExpBufferData		tables_query = {0};
	char		*pktab = NULL;
	Int2		result_cols;
	char	*escSchemaName = NULL, *escTableName = NULL;

	MYLOG(0, "entering...stmt=%p scnm=%p len=%d\n", stmt, szTableOwner, cbTableOwner);

	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

	if (res = QR_Constructor(), !res)
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for PGAPI_PrimaryKeys result.", func);
		return SQL_ERROR;
	}
	SC_set_Result(stmt, res);

	/* the binding structure for a statement is not set up until
	 *
	 * a statement is actually executed, so we'll have to do this
	 * ourselves.
	 */
	result_cols = NUM_OF_PKS_FIELDS;
	extend_column_bindings(SC_get_ARDF(stmt), result_cols);

	stmt->catalog_result = TRUE;
	/* set the field names */
	QR_set_num_fields(res, result_cols);
	QR_set_field_info_v(res, PKS_TABLE_CAT, "TABLE_CAT", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PKS_TABLE_SCHEM, "TABLE_SCHEM", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PKS_TABLE_NAME, "TABLE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PKS_COLUMN_NAME, "COLUMN_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PKS_KEY_SQ, "KEY_SEQ", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, PKS_PK_NAME, "PK_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);

	/*
	* TODO: HQ-6509 Implement a real answer for SQLPrimaryKeys
	*/

	ret = SQL_SUCCESS;

#undef	return
	/*
	 * also, things need to think that this statement is finished so the
	 * results can be retrieved.
	 */
	stmt->status = STMT_FINISHED;

	if (!SQL_SUCCEEDED(ret) && 0 >= SC_get_errornumber(stmt))
		SC_error_copy(stmt, tbl_stmt, TRUE);

	if (tbl_stmt)
		PGAPI_FreeStmt(tbl_stmt, SQL_DROP);

	if (!PQExpBufferDataBroken(tables_query))
		termPQExpBuffer(&tables_query);
	if (pktab)
		free(pktab);
	if (escSchemaName)
		free(escSchemaName);
	if (escTableName)
		free(escTableName);
	/* set up the current tuple pointer for SQLFetch */
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	SC_set_current_col(stmt, -1);

	MYLOG(0, "leaving stmt=%p, ret=%d\n", stmt, ret);
	return ret;
}


RETCODE		SQL_API
PGAPI_ForeignKeys(HSTMT hstmt,
					  const SQLCHAR * szPkTableQualifier, /* OA X*/
					  SQLSMALLINT cbPkTableQualifier,
					  const SQLCHAR * szPkTableOwner, /* OA E*/
					  SQLSMALLINT cbPkTableOwner,
					  const SQLCHAR * szPkTableName, /* OA(R) E*/
					  SQLSMALLINT cbPkTableName,
					  const SQLCHAR * szFkTableQualifier, /* OA X*/
					  SQLSMALLINT cbFkTableQualifier,
					  const SQLCHAR * szFkTableOwner, /* OA E*/
					  SQLSMALLINT cbFkTableOwner,
					  const SQLCHAR * szFkTableName, /* OA(R) E*/
					  SQLSMALLINT cbFkTableName)
{
	CSTR func = "PGAPI_ForeignKeys";
	StatementClass *stmt = (StatementClass *) hstmt;
	QResultClass	*res;
	HSTMT		hpkey_stmt = NULL;
	StatementClass *tbl_stmt = NULL;
	RETCODE		ret = SQL_ERROR, result;
	PQExpBufferData		tables_query = {0};
	char		*pk_table_needed = NULL, *escPkTableName = NULL;
	char		*fk_table_needed = NULL, *escFkTableName = NULL;
	char	   *pkey_text = NULL,
			   *fkey_text = NULL;

	ConnectionClass *conn;
	BOOL		pkey_alloced, fkey_alloced;
	Int2		result_cols;


	MYLOG(0, "entering...stmt=%p\n", stmt);

	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

	if (res = QR_Constructor(), !res)
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for PGAPI_ForeignKeys result.", func);
		return SQL_ERROR;
	}
	SC_set_Result(stmt, res);

	/* the binding structure for a statement is not set up until */

	/*
	 * a statement is actually executed, so we'll have to do this
	 * ourselves.
	 */
	result_cols = NUM_OF_FKS_FIELDS;
	extend_column_bindings(SC_get_ARDF(stmt), result_cols);

	stmt->catalog_result = TRUE;
	/* set the field names */
	QR_set_num_fields(res, result_cols);
	QR_set_field_info_v(res, FKS_PKTABLE_CAT, "PKTABLE_CAT", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, FKS_PKTABLE_SCHEM, "PKTABLE_SCHEM", PG_TYPE_VARCHAR, MAX_INFO_STRING); //TODO: Make it NOT nullable
	QR_set_field_info_v(res, FKS_PKTABLE_NAME, "PKTABLE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING); //TODO: Make it NOT nullable
	QR_set_field_info_v(res, FKS_PKCOLUMN_NAME, "PKCOLUMN_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING); //TODO: Make it NOT nullable
	QR_set_field_info_v(res, FKS_FKTABLE_CAT, "FKTABLE_CAT", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, FKS_FKTABLE_SCHEM, "FKTABLE_SCHEM", PG_TYPE_VARCHAR, MAX_INFO_STRING); //TODO: Make it NOT nullable
	QR_set_field_info_v(res, FKS_FKTABLE_NAME, "FKTABLE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING); //TODO: Make it NOT nullable
	QR_set_field_info_v(res, FKS_FKCOLUMN_NAME, "FKCOLUMN_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING); //TODO: Make it NOT nullable
	QR_set_field_info_v(res, FKS_KEY_SEQ, "KEY_SEQ", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, FKS_UPDATE_RULE, "UPDATE_RULE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, FKS_DELETE_RULE, "DELETE_RULE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, FKS_FK_NAME, "FK_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING); //TODO: Make it NOT nullable
	QR_set_field_info_v(res, FKS_PK_NAME, "PK_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING); //TODO: Make it NOT nullable
	QR_set_field_info_v(res, FKS_DEFERRABILITY, "DEFERRABILITY", PG_TYPE_INT2, 2);

	/*
	 * also, things need to think that this statement is finished so the
	 * results can be retrieved.
	 */
	stmt->status = STMT_FINISHED;

	/* set up the current tuple pointer for SQLFetch */
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	SC_set_current_col(stmt, -1);

	conn = SC_get_conn(stmt);
	result = PGAPI_AllocStmt(conn, (HSTMT *) &tbl_stmt, 0);
	if (!SQL_SUCCEEDED(result))
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate statement for PGAPI_ForeignKeys result.", func);
		return SQL_ERROR;
	}

#define	return	DONT_CALL_RETURN_FROM_HERE???

	pk_table_needed = make_string(szPkTableName, cbPkTableName, NULL, 0);
	fk_table_needed = make_string(szFkTableName, cbFkTableName, NULL, 0);

	pkey_alloced = fkey_alloced = FALSE;

	initPQExpBuffer(&tables_query);
	/*
	 * dtm query.
	 */

	ret = SQL_SUCCESS;

#undef	return
	/*
	 * also, things need to think that this statement is finished so the
	 * results can be retrieved.
	 */
	stmt->status = STMT_FINISHED;

	if (!SQL_SUCCEEDED(ret) && 0 >= SC_get_errornumber(stmt))
		SC_error_copy(stmt, tbl_stmt, TRUE);

	if (!PQExpBufferDataBroken(tables_query))
		termPQExpBuffer(&tables_query);
	if (pkey_alloced)
		free(pkey_text);
	if (fkey_alloced)
		free(fkey_text);
	if (pk_table_needed)
		free(pk_table_needed);
	if (escPkTableName)
		free(escPkTableName);
	if (fk_table_needed)
		free(fk_table_needed);
	if (escFkTableName)
		free(escFkTableName);

	if (tbl_stmt)
		PGAPI_FreeStmt(tbl_stmt, SQL_DROP);
	if (hpkey_stmt)
		PGAPI_FreeStmt(hpkey_stmt, SQL_DROP);

	/* set up the current tuple pointer for SQLFetch */
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	SC_set_current_col(stmt, -1);

	MYLOG(0, "leaving stmt=%p, ret=%d\n", stmt, ret);
	return ret;
}

RETCODE		SQL_API
PGAPI_ProcedureColumns(HSTMT hstmt,
					   const SQLCHAR * szProcQualifier, /* OA X*/
					   SQLSMALLINT cbProcQualifier,
					   const SQLCHAR * szProcOwner, /* PV E*/
					   SQLSMALLINT cbProcOwner,
					   const SQLCHAR * szProcName, /* PV E*/
					   SQLSMALLINT cbProcName,
					   const SQLCHAR * szColumnName, /* PV X*/
					   SQLSMALLINT cbColumnName,
					   UWORD flag)
{
	CSTR func = "PGAPI_ProcedureColumns";
	StatementClass	*stmt = (StatementClass *) hstmt;
	PQExpBufferData		proc_query = {0};
	Int2		result_cols;
	QResultClass *res, *tres = NULL;
	RETCODE		ret = SQL_ERROR, result;

	MYLOG(0, "entering...\n");

	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

	if (res = QR_Constructor(), !res)
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for PGAPI_ProcedureColumns result.", func);
		goto cleanup;
	}
	SC_set_Result(stmt, res);

	/*
	 * the binding structure for a statement is not set up until
	 * a statement is actually executed, so we'll have to do this
	 * ourselves.
	 */
	result_cols = NUM_OF_PROCOLS_FIELDS;
	extend_column_bindings(SC_get_ARDF(stmt), result_cols);

	stmt->catalog_result = TRUE;
	/* set the field names */
	QR_set_num_fields(res, result_cols);
	QR_set_field_info_v(res, PROCOLS_PROCEDURE_CAT, "PROCEDURE_CAT", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCOLS_PROCEDURE_SCHEM, "PROCEDUR_SCHEM", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCOLS_PROCEDURE_NAME, "PROCEDURE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCOLS_COLUMN_NAME, "COLUMN_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCOLS_COLUMN_TYPE, "COLUMN_TYPE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, PROCOLS_DATA_TYPE, "DATA_TYPE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, PROCOLS_TYPE_NAME, "TYPE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCOLS_COLUMN_SIZE, "COLUMN_SIZE", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, PROCOLS_BUFFER_LENGTH, "BUFFER_LENGTH", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, PROCOLS_DECIMAL_DIGITS, "DECIMAL_DIGITS", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, PROCOLS_NUM_PREC_RADIX, "NUM_PREC_RADIX", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, PROCOLS_NULLABLE, "NULLABLE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, PROCOLS_REMARKS, "REMARKS", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCOLS_COLUMN_DEF, "COLUMN_DEF", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCOLS_SQL_DATA_TYPE, "SQL_DATA_TYPE", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, PROCOLS_SQL_DATETIME_SUB, "SQL_DATETIME_SUB", PG_TYPE_INT2, 2);
	QR_set_field_info_v(res, PROCOLS_CHAR_OCTET_LENGTH, "CHAR_OCTET_LENGTH", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, PROCOLS_ORDINAL_POSITION, "ORDINAL_POSITION", PG_TYPE_INT4, 4);
	QR_set_field_info_v(res, PROCOLS_IS_NULLABLE, "IS_NULLABLE", PG_TYPE_VARCHAR, MAX_INFO_STRING);

	ret = SQL_SUCCESS;
cleanup:
	if (tres)
		QR_Destructor(tres);
	if (!PQExpBufferDataBroken(proc_query))
		termPQExpBuffer(&proc_query);
	/*
	 * also, things need to think that this statement is finished so the
	 * results can be retrieved.
	 */
	stmt->status = STMT_FINISHED;
	/* set up the current tuple pointer for SQLFetch */
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	SC_set_current_col(stmt, -1);

	return ret;
}


RETCODE		SQL_API
PGAPI_Procedures(HSTMT hstmt,
				 const SQLCHAR * szProcQualifier, /* OA X*/
				 SQLSMALLINT cbProcQualifier,
				 const SQLCHAR * szProcOwner, /* PV E*/
				 SQLSMALLINT cbProcOwner,
				 const SQLCHAR * szProcName, /* PV E*/
				 SQLSMALLINT cbProcName,
				 UWORD flag)
{
	CSTR func = "PGAPI_Procedures";
	StatementClass *stmt = (StatementClass *) hstmt;
	PQExpBufferData		proc_query = {0};
	char	*escSchemaName = NULL, *escProcName = NULL;
	QResultClass *res;
	RETCODE		ret = SQL_ERROR, result;
	int result_cols;

	MYLOG(0, "entering... scnm=%p len=%d\n", szProcOwner, cbProcOwner);

	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

	if (res = QR_Constructor(), !res)
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for PGAPI_ProcedureColumns result.", func);
		goto cleanup;
	}
	SC_set_Result(stmt, res);

	result_cols = NUM_OF_PROCS_FIELDS;
	extend_column_bindings(SC_get_ARDF(stmt), result_cols);

	stmt->catalog_result = TRUE;
	/* set the field names */
	QR_set_num_fields(res, result_cols);
	QR_set_field_info_v(res, PROCS_PROCEDURE_CAT, "PROCEDURE_CAT", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCS_PROCEDURE_SCHEM, "PROCEDUR_SCHEM", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCS_PROCEDURE_NAME, "PROCEDURE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);

	// For these next 3 columns, The ODBC documentation mentions that the Data type is N/A
	// "Reserved for future use. Applications should not rely on the data returned in these result columns."
	QR_set_field_info_v(res, PROCS_NUM_INPUT_PARAMS, "NUM_INPUT_PARAMS", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCS_NUM_OUTPUT_PARAMS, "NUM_OUTPUT_PARAMS", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCS_NUM_RESULT_SETS, "NUM_RESULT_SETS", PG_TYPE_VARCHAR, MAX_INFO_STRING);

	QR_set_field_info_v(res, PROCS_REMARKS, "REMARKS", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, PROCS_PROCEDURE_TYPE, "PROCEDURE_TYPE", PG_TYPE_INT2, 2);

	ret = SQL_SUCCESS;
cleanup:
	stmt->status = STMT_FINISHED;
	extend_column_bindings(SC_get_ARDF(stmt), 8);
	if (escSchemaName)
		free(escSchemaName);
	if (escProcName)
		free(escProcName);
	if (!PQExpBufferDataBroken(proc_query))
		termPQExpBuffer(&proc_query);
	/* set up the current tuple pointer for SQLFetch */
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	SC_set_current_col(stmt, -1);

	return ret;
}


#define	ACLMAX	8


RETCODE		SQL_API
PGAPI_TablePrivileges(HSTMT hstmt,
					  const SQLCHAR * szTableQualifier, /* OA X*/
					  SQLSMALLINT cbTableQualifier,
					  const SQLCHAR * szTableOwner, /* PV E*/
					  SQLSMALLINT cbTableOwner,
					  const SQLCHAR * szTableName, /* PV E*/
					  SQLSMALLINT cbTableName,
					  UWORD flag)
{
	StatementClass *stmt = (StatementClass *) hstmt;
	CSTR func = "PGAPI_TablePrivileges";
	Int2		result_cols;
	PQExpBufferData		proc_query = {0};
	QResultClass	*res, *wres = NULL, *allures = NULL;
	char		(*useracl)[ACLMAX] = NULL;
	RETCODE		result, ret = SQL_ERROR;
	char		*escSchemaName = NULL, *escTableName = NULL;

	MYLOG(0, "entering... scnm=%p len-%d\n", szTableOwner, cbTableOwner);
	if (result = SC_initialize_and_recycle(stmt), SQL_SUCCESS != result)
		return result;

	/*
	 * a statement is actually executed, so we'll have to do this
	 * ourselves.
	 */
	result_cols = NUM_OF_TABPRIV_FIELDS;
	extend_column_bindings(SC_get_ARDF(stmt), result_cols);

	stmt->catalog_result = TRUE;
	/* set the field names */
	res = QR_Constructor();
	if (!res)
	{
		SC_set_error(stmt, STMT_NO_MEMORY_ERROR, "Couldn't allocate memory for query.", func);
		return SQL_ERROR;
	}
	SC_set_Result(stmt, res);
	QR_set_num_fields(res, result_cols);
	QR_set_field_info_v(res, TABPRIV_TABLE_CAT, "TABLE_CAT", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, TABPRIV_TABLE_SCHEM, "TABLE_SCHEM", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, TABPRIV_TABLE_NAME, "TABLE_NAME", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, TABPRIV_GRANTOR, "GRANTOR", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, TABPRIV_GRANTEE, "GRANTEE", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, TABPRIV_PRIVILEGE, "PRIVILEGE", PG_TYPE_VARCHAR, MAX_INFO_STRING);
	QR_set_field_info_v(res, TABPRIV_IS_GRANTABLE, "IS_GRANTABLE", PG_TYPE_VARCHAR, MAX_INFO_STRING);

	/*
	 * also, things need to think that this statement is finished so the
	 * results can be retrieved.
	 */
	stmt->status = STMT_FINISHED;
	/* set up the current tuple pointer for SQLFetch */
	stmt->currTuple = -1;
	SC_set_rowset_start(stmt, -1, FALSE);
	SC_set_current_col(stmt, -1);

	ret = SQL_SUCCESS;

#undef	return
	if (escSchemaName)
		free(escSchemaName);
	if (escTableName)
		free(escTableName);
	if (useracl)
		free(useracl);
	if (!PQExpBufferDataBroken(proc_query))
		termPQExpBuffer(&proc_query);
	if (wres)
		QR_Destructor(wres);
	if (allures)
		QR_Destructor(allures);
	return ret;
}

