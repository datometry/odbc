FROM centos:7

# CentOS7 EOL fix
RUN sed -i s/mirror.centos.org/vault.centos.org/g /etc/yum.repos.d/*.repo && \
    sed -i s/^#.*baseurl=http/baseurl=http/g /etc/yum.repos.d/*.repo && \
    sed -i s/^mirrorlist=http:\\/\\/mirrorlist.centos.org/#mirrorlist=http:\\/\\/mirrorlist.centos.org/g /etc/yum.repos.d/*.repo

RUN yum install -y epel-release && \
    yum -y install deltarpm zlib-devel readline-devel openssl-devel libxml2-devel wget gcc make && \
    yum -y install unixODBC-devel git m4 automake libtool postgresql-devel && \
    yum -y install vim sudo git gdb file patchelf

# Install polytools
ADD rpm_polytools /tmp/rpm_polytools
RUN yum install -y /tmp/rpm_polytools/*.rpm
ENV PATH="/opt/datometry/polytools/bin:${PATH}"

RUN mkdir -p /tmp/odbc
ADD . /tmp/odbc
WORKDIR /tmp/odbc
RUN sudo ./configure.sh
RUN make install

RUN patchelf --set-rpath /opt/datometry/polytools/lib /usr/local/lib/dtmodbca.so
RUN patchelf --set-rpath /opt/datometry/polytools/lib /usr/local/lib/dtmodbcw.so
RUN cat docker/odbcinst.ini >> /etc/odbcinst.ini

# bash and vim scripts
ADD docker/bashrc /root/.bashrc
ADD docker/vimrc /root/.vimrc
ADD docker/bashrc /home/postgres/.bashrc
