
option Explicit

Const msiOpenDatabaseModeTransact = 1
Const msiViewModifyUpdate = 2

Dim msiPath : msiPath = Wscript.Arguments(0)

Dim installer
Set installer = Wscript.CreateObject("WindowsInstaller.Installer")
Dim database
Set database = installer.OpenDatabase(msiPath, msiOpenDatabaseModeTransact)

Dim query
query = "Select * FROM File"
Dim view
Set view = database.OpenView(query)
view.Execute
Dim record

Set record = view.Fetch
Dim gFile, pos
Do
    Set record = view.Fetch
    If record Is Nothing Then Exit Do

    pos = InStr(record.StringData(3), "|")
    If pos > 0 Then
        gFile = Mid(record.StringData(3), pos + 1)
        WScript.echo record.StringData(3) & " -> " & gFile
        record.StringData(3) = gFile
        view.Modify msiViewModifyUpdate, record
    End If
Loop

database.Commit
Set view = Nothing
Wscript.Quit(0)

'   VBScript file
'
'   When the dll name of the driver is not of 8.3-format
'       the modification of the FileName is needed
'       https://en.wikipedia.org/wiki/8.3_filename
'
' This is to work-around a bug in the WiX Toolset, see
' https://github.com/wixtoolset/issues/issues/1422
'
' We remove the short name from the filename field in the File-table
' of the two DLLs that need to be registered as ODBC drivers. Strictly
' speaking, that makes the contents of the table invalid, because a short
' name is mandatory, but Windows Installer seems nevertheless install it
' just fine.
'
' Note:
' For some reason, I had trouble debugging with the comments at the start
' and/or within the script.