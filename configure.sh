#!/bin/sh
# all instructions to generate and execute
# configure

python render_version.py
libtoolize --force
aclocal
autoheader
automake --force-missing --add-missing
autoreconf -i
autoconf
./configure --with-libpq=/opt/datometry/polytools/lib
