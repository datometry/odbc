$ErrorActionPreference = "Stop"

$db_port=1025
$db_user="dtmadmin"
$db_name="adwtestdb"

$scr_path = Split-Path $MyInvocation.MyCommand.Path
Push-Location $scr_path
cd terraform
$ip=$(terraform output -raw vm_pub_ip)
$user=$(terraform output -raw vm_ssh_user)
$db_pass=$(terraform output -raw asdw_password)
Pop-Location
$key = "${scr_path}\terraform\newVM"

function runRemote {
  param (
    [Parameter()] [string]$cmd
  )
  Write-Host $cmd
  & ssh -o StrictHostKeyChecking=no -i $key $user@$ip sudo "$cmd"
  if ($LASTEXITCODE) { Throw "runRemote failed" }
}

runRemote "/usr/local/bin/docker-compose down"
runRemote "chown ${user}:${user} docker/dtm.ini"
& scp -i $key ${scr_path}\dtm.ini $user@$($ip):docker; if ($LASTEXITCODE) { Throw "scp failed" }

runRemote "/usr/local/bin/docker-compose up -d hyperq"
& $scr_path\run_tests.ps1 -db_host $ip -db_port $db_port -db_user $db_user -db_pass $db_pass -db_name $db_name -test_name "asdw"
runRemote "/usr/local/bin/docker-compose down"
