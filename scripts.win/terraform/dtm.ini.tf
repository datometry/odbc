resource "local_file" "dtm_ini" {
  filename = "${path.module}/../dtm.ini"
  content  = templatefile("${path.module}/dtm.ini.tpl", {
    database = "adwtestdb",
    address  = azurerm_mssql_server.this.fully_qualified_domain_name,
    username = "dtmadmin",
    password = random_string.asdw_password.result
  })
}
