data "http" "public_ip" {
  url = "https://publicip.eng.datometry.com"
}

locals {
  agent_public_ip = chomp(data.http.public_ip.response_body) 
}

output "agent_public_ip" {
  value = local.agent_public_ip
}
