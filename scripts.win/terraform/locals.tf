locals {
  vm_name = "hyperq-docker-vm"
  region = "West US"
  size = "Standard_DS2_v2"
  tags = {
    "BuildNumber" = var.build_number
    # "BuildTypeID" = var.build_type_id
    "Description" = var.description
    "Environment" = var.environment
    "Git Branch"  = var.git_branch
    "Git Commit"  = var.git_commit
    "Owner"       = var.owner
    "Purpose"     = var.purpose
    "Team"        = var.team
    "UsageType"   = var.usage_type
    "build_id"    = var.build_id
    "invoked_by"  = var.invoked_by
  }
}
