provider "azurerm" {
  features {
      resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

resource "azurerm_resource_group" "this" {
  name = "build-${var.build_id}"
  location = local.region
  tags = local.tags
  lifecycle {
    ignore_changes = [tags.provision_time]
  }  
}

resource "azurerm_virtual_network" "this" {
  name                = "${local.vm_name}-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
}

resource "azurerm_subnet" "this" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.this.name
  virtual_network_name = azurerm_virtual_network.this.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "this" {
  name                = "${local.vm_name}-pip"
  resource_group_name = azurerm_resource_group.this.name
  location            = azurerm_resource_group.this.location
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_network_interface" "this" {
  name                = "${local.vm_name}-nic"
  location            = local.region
  resource_group_name = azurerm_resource_group.this.name
  ip_configuration {
    name                          = "${local.vm_name}-ipcfg"
    subnet_id                     = azurerm_subnet.this.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.this.id
  }
  accelerated_networking_enabled = true
}

resource "azurerm_network_security_group" "this" {
  name                = "${local.vm_name}-nsg"
  location            = local.region
  resource_group_name = azurerm_resource_group.this.name
  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = local.agent_public_ip
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HQ"
    priority                   = 1025
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "1025"
    source_address_prefix      = local.agent_public_ip
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface_security_group_association" "this" {
  network_interface_id      = azurerm_network_interface.this.id
  network_security_group_id = azurerm_network_security_group.this.id
}

resource "azurerm_linux_virtual_machine" "this" {
  name = local.vm_name
  location = local.region
  resource_group_name = azurerm_resource_group.this.name
  network_interface_ids = [ azurerm_network_interface.this.id ]
  size = local.size
  os_disk {
    name                 = "${local.vm_name}-disk_os"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }
  source_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.7"
    version   = "latest"
  }
  admin_username = var.vm_ssh_user
  admin_ssh_key {
    username   = var.vm_ssh_user
    public_key = file(var.vm_ssh_key_file)
  }
}

resource "random_string" "asdw_password" {
  length      = 26
  min_lower   = 1
  min_numeric = 1
  min_upper   = 1
  special     = false
}

# === ###

resource "azurerm_mssql_server" "this" {
  name                         = "build-${var.build_id}"
  resource_group_name          = azurerm_resource_group.this.name
  location                     = local.region
  version                      = "12.0"
  administrator_login          = "dtmadmin"
  administrator_login_password = random_string.asdw_password.result
}

resource "azurerm_mssql_firewall_rule" "this" {
  name                = "db_firewallRule"
  server_id           = azurerm_mssql_server.this.id
  start_ip_address    = azurerm_linux_virtual_machine.this.public_ip_address
  end_ip_address      = azurerm_linux_virtual_machine.this.public_ip_address
}

resource "azurerm_mssql_database" "db" {
  name      = "adwtestdb"
  server_id = azurerm_mssql_server.this.id
  sku_name  = "DW100c"
}

# === ###

output "vm_pub_ip" {
  value = azurerm_linux_virtual_machine.this.public_ip_address
}
output "vm_ssh_user" {
  value = var.vm_ssh_user
}
output "asdw_password" {
  value     = azurerm_mssql_server.this.administrator_login_password
  sensitive = true
}
