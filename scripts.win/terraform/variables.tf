variable "vm_ssh_user"     { default =  "azureuser" }
variable "vm_ssh_key_file" { default =  "~/.ssh/id_rsa.pub" }

variable "build_number"  { default = "unknown" }
variable "description"   { default = "ODBC Driver Windows" }
variable "environment"   { default = "unknown" }
variable "git_branch"    { default = "unknown" }
variable "git_commit"    { default = "unknown" }
variable "owner"         { default = "unknown" }
variable "purpose"       { default = "Engineering Testing" }
variable "team"          { default = "Hyper-Q" }
variable "usage_type"    { default = "Engineering Automation" }
variable "build_id"      { default = "local"}
variable "invoked_by"    { default = "unknown"}
