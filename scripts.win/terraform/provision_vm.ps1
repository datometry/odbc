param (
  [Parameter()] [string]$ip=$(throw "-ip is required"),
  [Parameter()] [string]$user="azureuser",
  [Parameter()] [string]$key="id_rsa"
)
$ErrorActionPreference = "Stop"

$docker_comp_ver="2.26.1"
$aws_container_registry="213223045120.dkr.ecr.us-west-2.amazonaws.com"

$sshcmd="ssh -o StrictHostKeyChecking=no -i $key $user@$ip"
function runRemote {
  param (
    [Parameter()] [string]$cmd
  )
  Write-Host $cmd
  & ssh -o StrictHostKeyChecking=no -i $key $user@$ip sudo "$cmd"
  if ($LASTEXITCODE) { Throw "runRemote failed" }
}

$ssh_timeout=120/2
Write-Host "==== Waiting for SSH ===="
for ($t=0; $t -lt $ssh_timeout; $t++) {
  if (Test-NetConnection -ComputerName $ip -Port 22 -InformationLevel Quiet) { break }
  Start-Sleep -s 2
}
if ($t -eq $ssh_timeout) {
  Write-Host "==== Can't connect $ip:22 ===="
  exit 1
}

runRemote "yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo"
runRemote "yum install -y mc telnet docker-ce docker-ce-cli containerd.io docker-compose-plugin"
runRemote "curl -s -L `"https://github.com/docker/compose/releases/download/v$($docker_comp_ver)/docker-compose-`$(uname -s)-`$(uname -m)`" -o /usr/local/bin/docker-compose"
runRemote "chmod +x /usr/local/bin/docker-compose"
runRemote "curl -s -L `"https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip`" -o awscliv2.zip"
runRemote "unzip -q -o awscliv2.zip"
runRemote "./aws/install --update"

runRemote "systemctl stop docker"
runRemote "systemctl enable docker"
runRemote "systemctl start docker"

Invoke-Expression "scp -i $key -r ..\..\docker $user@$($ip):"; if ($LASTEXITCODE) { Throw "scp failed" }
Invoke-Expression "scp -i $key ..\..\Dockerfile.hyperq $user@$($ip):"; if ($LASTEXITCODE) { Throw "scp failed" }
Invoke-Expression "scp -i $key ..\docker-compose.yml $user@$($ip):"; if ($LASTEXITCODE) { Throw "scp failed" }
Invoke-Expression "scp -i $key ..\dk_key.txt $user@$($ip):"; if ($LASTEXITCODE) { Throw "scp failed" }
Invoke-Expression "scp -i $key -r ..\..\rpm_hyperq $user@$($ip):"; if ($LASTEXITCODE) { Throw "scp failed" }

runRemote "cat dk_key.txt | sudo docker login --username AWS --password-stdin $aws_container_registry"
runRemote "docker build -f Dockerfile.hyperq . -t hyperq"

# echo "==== 1 ===="
# runRemote "ls -lahR docker"
# echo "===="

# # for GP
# Invoke-Expression "scp -i $key ..\..\docker\dtm.ini $user@$($ip):docker"

# echo "==== 2 ===="
# runRemote "ls -lahR docker"
# echo "===="

# # for AZ
# Invoke-Expression "scp -i $key ..\dtm.ini $user@$($ip):docker"

# echo "==== 3 ===="
# runRemote "ls -lahR docker"
# echo "===="
