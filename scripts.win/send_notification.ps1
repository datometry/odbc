$ErrorActionPreference = "Stop"

Push-Location (Split-Path $MyInvocation.MyCommand.Path)
$version=$(cat ..\VERSION)

if ($Env:BRANCH -eq "master") {
  $slack_channel='#releases'
} else {
  $slack_channel='#testing'
}
if ($Env:BUILD_NUMBER -eq $null) {
  $build='0'
} else {
  $build=$Env:BUILD_NUMBER
}

$post_data=@"
{ `"channel`": `"$slack_channel`",
  `"username`": `"TeamCity`",
  `"text`": "",
  `"attachments`": [{
    `"mrkdwn_in`": [`"text`"],
    `"title`": `"Version $version.$build`",
    `"title_link`": `"https://downloads.datometry.com/odbc/windows/$version.$build`",
    `"pretext`": `"A new version of the Datometry ODBC Windows driver has been released!`",
    `"fallback`":`"ODBC $version.$build released`",
    `"color`":`"good`",
    `"fields`": [
      {
          `"title`": `"Version`",
          `"value`": `"$version.$build`",
          `"short`": true
      },
      {
          `"title`": `"Build #`",
          `"value`": `"$build`",
          `"short`": true
      },
      {
          `"title`": `"Windows`",
          `"value`": `"https://downloads.datometry.com/odbc/windows/$version.$build`",
          `"short`": false
      }
  ]
  }]
}
"@

Invoke-WebRequest -Method PUT -Body "$post_data" -Uri "$Env:SLACK_WEBHOOK_URL" -ContentType "application/json" -UseBasicParsing

Pop-Location
