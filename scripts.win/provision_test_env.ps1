$ErrorActionPreference = "Stop"

Push-Location (Split-Path $MyInvocation.MyCommand.Path)
$key="newVM"
cd terraform
$Env:TF_VAR_environment = $Env:ENVIRONMENT_NAME
$Env:TF_VAR_invoked_by = "TeamCity"
terraform init
if (-not (Test-Path -Path $key -PathType Leaf)) {
  ssh-keygen -b 2048 -t rsa -f $key -q -N '""'
  Copy-Item -Path $key -Destination "$key.bak"
  Icacls $key /c /t /Inheritance:d
  Icacls $key /c /t /Remove:g BUILTIN\Administrators Administrator
  Icacls $Key /c /t /Grant:r ${env:UserName}:F
}
terraform apply -var="vm_ssh_key_file=$key.pub" --auto-approve
if ($LASTEXITCODE) { Throw "terraform apply failed" }
./provision_vm.ps1 -ip $(terraform output -raw vm_pub_ip) -user $(terraform output -raw vm_ssh_user) -key $key
cd ..

Pop-Location
$app = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -match "Datometry Client Tools" }
if ($app) {
  $app.Uninstall()
  Write-Host "Unistalled: $app"
}
Start-Process -Wait -FilePath msiexec -ArgumentList /i, "installer\x64\$(Get-ChildItem installer\x64\dtm_dtu_*.x64.msi -Name)", /qn, /l, "install.log"
cat install.log
