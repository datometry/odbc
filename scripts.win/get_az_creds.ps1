$ErrorActionPreference = "Stop"

Push-Location (Split-Path $MyInvocation.MyCommand.Path)
$azCreds = aws --region us-west-2 secretsmanager get-secret-value --secret-id Teamcity/Azure --query SecretString --output text | ConvertFrom-Json
$tf_creds='provider.tf'
$dk_key='dk_key.txt'

if (Test-Path 'terraform') {
  $tf_creds="terraform\$tf_creds"
}
"provider `"azurerm`" {"                                    | Out-File $tf_creds -Encoding ascii
"  features {}"                                             | Out-File $tf_creds -Encoding ascii -Append
"  subscription_id = `"$($azCreds.AZURE_SUBSCRIPTION_ID)`"" | Out-File $tf_creds -Encoding ascii -Append
"  client_id       = `"$($azCreds.AZURE_CLIENT_ID)`""       | Out-File $tf_creds -Encoding ascii -Append
"  client_secret   = `"$($azCreds.AZURE_SECRET)`""          | Out-File $tf_creds -Encoding ascii -Append
"  tenant_id       = `"$($azCreds.AZURE_TENANT)`""          | Out-File $tf_creds -Encoding ascii -Append
"}" | Out-File $tf_creds -Encoding ascii -Append

aws ecr get-login-password --region us-west-2 | Out-File $dk_key -Encoding ascii
Pop-Location
