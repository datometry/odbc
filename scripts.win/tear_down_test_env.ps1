$ErrorActionPreference = "Stop"

$key="newVM"
Push-Location (Split-Path $MyInvocation.MyCommand.Path)
cd terraform
terraform destroy -var="vm_ssh_key_file=$key.pub" --auto-approve
if ($LASTEXITCODE) { Throw "terraform destroy failed" }
cd ..
Pop-Location
