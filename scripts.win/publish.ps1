$ErrorActionPreference = "Stop"

Push-Location (Split-Path $MyInvocation.MyCommand.Path)
$version=$(cat ..\VERSION)

if ($Env:BRANCH -eq "master") {
  $aws_bucket='downloads.datometry.com'
} else {
  $aws_bucket='test-downloads.datometry.com'
}
if ($Env:BUILD_NUMBER -eq $null) {
  $build='0'
} else {
  $build=$Env:BUILD_NUMBER
}

cd ..\installer\x64

"{"                                                            | Out-File metadata.json -Encoding ascii
"`"artifacts`": {"                                             | Out-File metadata.json -Encoding ascii -Append
"    `"msi`": {"                                               | Out-File metadata.json -Encoding ascii -Append
"        `"filename`": `"dtm_dtu_$version.$build.x64.msi`","   | Out-File metadata.json -Encoding ascii -Append
"        `"sha256`": `"`""                                     | Out-File metadata.json -Encoding ascii -Append
"    }"                                                        | Out-File metadata.json -Encoding ascii -Append
"},"                                                           | Out-File metadata.json -Encoding ascii -Append
"`"build`": `"$build`","                                       | Out-File metadata.json -Encoding ascii -Append
"`"license`": `"`","                                           | Out-File metadata.json -Encoding ascii -Append
"`"release_epoch`": `"`","                                     | Out-File metadata.json -Encoding ascii -Append
"`"release_time`": `"`","                                      | Out-File metadata.json -Encoding ascii -Append
"`"version`": `"$version.$build`""                             | Out-File metadata.json -Encoding ascii -Append
"}"                                                            | Out-File metadata.json -Encoding ascii -Append

aws s3 cp dtm_dtu_$version.$build.x64.msi s3://$aws_bucket/odbc/windows/$version.$build/
aws s3 cp metadata.json s3://$aws_bucket/odbc/windows/$version.$build/
aws s3 cp metadata.json s3://$aws_bucket/odbc/windows/

Pop-Location
