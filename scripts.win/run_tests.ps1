param(
  [Parameter()] [string]$db_host=$(throw "-db_host is required."),
  [Parameter()] [string]$db_port=$(throw "-db_port is required."),
  [Parameter()] [string]$db_user=$(throw "-db_user is required."),
  [Parameter()] [string]$db_pass=$(throw "-db_pass is required."),
  [Parameter()] [string]$db_name=$(throw "-db_name is required."),
  [Parameter()] [string]$test_name
)
$ErrorActionPreference = "Stop"

$db_timeout=120/2
$dtm_odbc_drv_name="Datometry Unicode(x64)"
$msvc="C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvars64.bat"
$xml_folder="test_results_xml"
$out_folder="test_results_out"

Remove-OdbcDsn -Name psqlodbc_test_dsn -DsnType System -WarningAction Ignore
Add-OdbcDsn -Name psqlodbc_test_dsn `
  -DriverName $dtm_odbc_drv_name `
  -Platform 64-bit `
  -DsnType System `
  -SetPropertyValue @("Server=$db_host", "Port=$db_port", "UserName=$db_user", "Password=$db_pass", "Database=$db_name")

cd test
Write-Host "###### Waiting for Hyper-Q to start ######"
for ($t=0; $t -lt $db_timeout; $t++) {
  if (Test-NetConnection -ComputerName $db_host -Port $db_port -InformationLevel Quiet) { break }
  Start-Sleep -s 2
}
if ($t -eq $db_timeout) {
  Write-Host "######## Can't connect to Hyper-Q ########"
  exit 1
}
Write-Host "############ Hyper-Q is ready ############"

If(Test-Path $xml_folder) { Remove-Item $xml_folder -Recurse }
New-Item -Name $xml_folder -ItemType "directory"
"cd $pwd"                                                           | Out-File tmp.cmd -Encoding ascii
"call `"$msvc`""                                                    | Out-File tmp.cmd -Encoding ascii -Append
"set CMOCKA_PATH=C:\tools\vcpkg\packages\cmocka_x64-windows"        | Out-File tmp.cmd -Encoding ascii -Append
"copy /Y %CMOCKA_PATH%\bin\cmocka.dll ."                            | Out-File tmp.cmd -Encoding ascii -Append
"set CL=/I%CMOCKA_PATH%\include /link %CMOCKA_PATH%\lib\cmocka.lib" | Out-File tmp.cmd -Encoding ascii -Append
"set PATH=%PATH%;C:\tools\msys64\mingw64\bin"                       | Out-File tmp.cmd -Encoding ascii -Append
"set CMOCKA_MESSAGE_OUTPUT=xml"                                     | Out-File tmp.cmd -Encoding ascii -Append
"set CMOCKA_XML_FILE=$xml_folder\%%g.xml"                           | Out-File tmp.cmd -Encoding ascii -Append
"nmake /f win.mak installcheck"                                     | Out-File tmp.cmd -Encoding ascii -Append
cmd.exe /c tmp.cmd
del tmp.cmd

if ($test_name -ne "") {
  Get-ChildItem $xml_folder -Recurse  | Where-Object { -not $_.PSIsContainer } | ForEach-Object {
    (Get-Content $xml_folder\$_ -Raw) -replace "<testsuite name=`"", "<testsuite name=`"$test_name-" | Out-File $xml_folder\$_ -encoding ASCII
  }
  Move-Item -Path $xml_folder -Destination ..\$xml_folder-$test_name -Force
  Move-Item -Path results -Destination ..\$out_folder-$test_name -Force
} else {
  Move-Item -Path $xml_folder -Destination ..\$xml_folder -Force
  Move-Item -Path results -Destination ..\$out_folder -Force
}

cd ..
