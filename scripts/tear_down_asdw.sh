#!/usr/bin/env bash
set -e

cd scripts/terraform
terraform destroy -auto-approve
cd ../..
docker-compose down
