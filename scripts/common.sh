#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

if [ -z "${BUILD_NUMBER:-}" ]; then
  export BUILD_NUMBER='0'
fi

if [ -z "${BRANCH:-}" ]; then
  export BRANCH='LOCAL'
fi

# VERSION=$(cat ${SCRIPT_DIR}/../VERSION).${BUILD_NUMBER}
VERSION=$(cat ${SCRIPT_DIR}/../VERSION)

# if [ "${BRANCH}" == "master" ]; then
#   PACKAGE_PREFIX="dtmodbc"
# else
#   PACKAGE_PREFIX="dtmodbc-${BRANCH}"
# fi
PACKAGE_PREFIX="dtmodbc"

# RELEASE=1
