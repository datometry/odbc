#!/usr/bin/env bash
set -e

cd scripts/terraform
passwd=$(terraform output -raw password)
cd ../..

docker-compose run --rm dtmodbc bash /checkout/scripts/run_tests.sh --host hyperq --port 1025 --username dtmadmin --password ${passwd} --database adwtestdb --tests_name asdw --ignore-failures
