#!/bin/bash

if [ "$#" -lt 2 ]; then
  echo "Send Slack release notify"
  echo "usage: ${0} <Main Title> <Folder> <Field Title> <Rel.Path> (<Field Title> <Rel.Path>)"
  echo "SLACK_WEBHOOK_URL, SLACK_CHANNEL and DOWNLOAD_URL env variables are required"
  exit 1
fi

regex="^.*/(.+)-(.+)/$"
[[ $(ls -d $2/$4/*/) =~ $regex ]]
version=${BASH_REMATCH[1]}

generate_post_data() {
  printf '{\n'
  printf '"channel": "%s",\n' ${SLACK_CHANNEL}
  printf '"username": "TeamCity",\n'
  printf '"attachments": [{\n'
  printf '  "mrkdwn_in": ["text"],\n'
  printf '  "title": "%s %s Released",\n' "$1" ${version}
  printf '  "text": "A new version of the Datometry %s has been released, pushed to S3, and is available for download.",\n' "$1"
  printf '  "color":"good",\n'
  printf '  "fields": [\n'
  for ((i=3; i<=$#; i+=2)); do
    printf '       {\n'
    printf '           "title": "%s",\n' "${!i}"
    nxt=$((i+1))
    printf '           "value": "%s/%s/%s",\n' ${DOWNLOAD_URL} ${!nxt} $(basename $(ls -d $2/${!nxt}/*/))
    printf '           "short": false\n'
    printf '       },\n'
  done
  printf '  ]\n'
  printf '}]\n'
  printf '}\n'
}

generate_post_data "$@"
curl -H 'Content-type: application/json' -d "$(generate_post_data "$@")" ${SLACK_WEBHOOK_URL}
