#!/bin/bash
set -euo pipefail

if [ "${BRANCH-}" == "master" ]; then
  aws_bucket='downloads.datometry.com'
else
  aws_bucket='test-downloads.datometry.com'
fi

docker run --rm -v ${PWD}:/checkout -w /checkout \
-e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY --entrypoint scripts/_aws_upload.sh \
amazon/aws-cli release s3://${aws_bucket}/odbc

docker run -v ${PWD}:/checkout -w /checkout \
-e PACKAGECLOUD_TOKEN --entrypoint package_cloud \
cdrx/packagecloud push datometry/dtmodbc/el/7 $(find release/linux -name '*.rpm')
