#!/usr/bin/env bash
set -e

WAIT_FOR_HOST=postgres WAIT_FOR_PORT=5432 docker-compose build hyperq

cd scripts/terraform
terraform init
terraform apply -auto-approve
cd ../..

docker-compose up -d hyperq
