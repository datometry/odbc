version = "3.2.55"


[endpoints]

"endpoint".port = 1025
"endpoint".database = "${database}"
"endpoint".app_protocol = dtm_pgv3
"endpoint".type = teradata
"endpoint".version = "140000"
"endpoint".is_case_sensitive = false


[gateways]

"gateway".name = "gateway_msadw"
"gateway".is_odbc_gateway = true
"gateway".connection_string = "Driver=ODBC Driver 17 for SQL Server;Server=__HOST__;Port=__PORT__;Database=__DATABASE__;Uid=__USER__;Pwd=__PASSWORD__"
"gateway".host = "${address}"
"gateway".port = 1433
"gateway".tls = prefer
"gateway".ipv = 4
"gateway".type = msadw
"gateway".version = "100000"
"gateway".bulk_load_utility = bcp
"gateway".bulk_load_chunk_size = 100MB
"gateway".bulk_load_max_errors = 2000000000
"gateway".bulk_load_detect_encoding = false
"gateway".locale = "en_US.CP1252"



[pools]

"defpool".gateway = ["gateway_msadw"]
"defpool".name = "pool_msadw"
"defpool".backlog = 10
"defpool".capacity = 100
"defpool".active = 30
"defpool".tx_sync = shared


[policies]

"defpolicy".protocol = all
"defpolicy".database = all
"defpolicy".user = all
"defpolicy".ip = "0.0.0.0"
"defpolicy".ip_prefix = 0
"defpolicy".pool = "pool_msadw"
"defpolicy".auth_method = passthru
