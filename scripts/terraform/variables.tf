variable "build_number"     { default = "UNSET" }
variable "build_type_id"    { default = "UNSET"}
variable "build_id"         { default = "none" }
variable "invoking_user"    { default = "unset"}
variable "environment"      { default = "odbc"}
variable "adw_address"      { default = "UNSET"}
variable "azure_region"     { default = "West US 2" }
variable "owner"            { default = "UNKNOWN"}
variable "branch_name"      { default = "UNKNOWN" }
variable "git_commit"       { default = "UNKNOWN" }
variable "team_project"     { default = "unset" }
variable "build_definition" { default = "unset" }
variable "job_attempt"      { default = "0" }
variable "job_name"         { default = "local" }
