locals {
  invoked_by           = var.invoking_user == "unset" ? "CI" : var.invoking_user
  local_name_prefix    = "${var.invoking_user}-${var.build_definition}-local"
  ci_name_prefix       = "build-${var.build_id}-${var.build_type_id}"
  name_prefix          = var.build_id == "0" ? "build-${var.invoking_user}-local" : "build-${var.build_id}"
}
