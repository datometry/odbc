provider "azurerm" {
  features {
      resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

resource "azurerm_resource_group" "terraform-test" {
  name     = local.name_prefix
  location = var.azure_region
  tags = {
    "Owner"                 = var.owner
    "Team"                  = "Hyper-Q"
    "Purpose"               = "Engineering Testing"
    "Description"           = "ODBC testing"
    "Environment"           = var.environment
    "BranchName"            = var.branch_name
    "GitCommit"             = var.git_commit
    "BuildNumber"           = var.build_number
    "BuildDefinition"       = var.build_definition
    "JobName"               = var.job_name
    "TeamProject"           = var.team_project
    "BuildID"               = var.build_id
    "InvokedBy"             = local.invoked_by
  }
}

resource "azurerm_mssql_firewall_rule" "terraform-test" {
  name                = "db_firewallRule"
  server_id           = azurerm_mssql_server.terraform-test.id
  start_ip_address    = local.agent_public_ip
  end_ip_address      = local.agent_public_ip
}

resource "azurerm_mssql_server" "terraform-test" {
  name                         = random_string.asdw_srv_name.result
  resource_group_name          = azurerm_resource_group.terraform-test.name
  location                     = var.azure_region
  version                      = "12.0"
  administrator_login          = "dtmadmin"
  administrator_login_password = random_string.password.result
  tags = {
    "Owner"                 = var.owner
    "Team"                  = "Hyper-Q"
    "Purpose"               = "Engineering Testing"
    "Description"           = "ODBC testing"
    "Environment"           = var.environment
    "BranchName"            = var.branch_name
    "GitCommit"             = var.git_commit
    "BuildNumber"           = var.build_number
    "BuildDefinition"       = var.build_definition
    "JobName"               = var.job_name
    "TeamProject"           = var.team_project
    "BuildID"               = var.build_id
    "InvokedBy"             = local.invoked_by
  }
}
resource "random_string" "asdw_srv_name" {
  length      = 16
  min_lower   = 1
  min_numeric = 1
  upper       = false
  special     = false 
}

resource "azurerm_mssql_database" "terraform-test" {
  name                             = "adwtestdb"
  server_id                        = azurerm_mssql_server.terraform-test.id
  sku_name                         = "DW100c"
  tags = {
    "Owner"                 = var.owner
    "Team"                  = "Hyper-Q"
    "Purpose"               = "Engineering Testing"
    "Description"           = "ODBC testing"
    "Environment"           = var.environment
    "BranchName"            = var.branch_name
    "GitCommit"             = var.git_commit
    "BuildNumber"           = var.build_number
    "BuildDefinition"       = var.build_definition
    "JobName"               = var.job_name
    "TeamProject"           = var.team_project
    "BuildID"               = var.build_id
    "InvokedBy"             = local.invoked_by
  }
}

output "password" {
  value     = random_string.password.result
  sensitive = true
}
