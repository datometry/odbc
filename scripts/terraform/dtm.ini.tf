resource "local_file" "dtm_ini" {
  filename = "${path.module}/../../docker/dtm.ini"
  content  = templatefile("${path.module}/templates/asdw/dtm.ini.tpl", {
      database = "adwtestdb",
      address = azurerm_mssql_server.terraform-test.fully_qualified_domain_name,
      username = "dtmadmin",
      password = random_string.password.result
    }
  )
}
