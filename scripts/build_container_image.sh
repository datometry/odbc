#!/usr/bin/env bash
set -e

DEV_CONATINER_IMAGE_TAG=dtmodbc:dev
BUILD_CONTAINER_IMAGE_TAG=dtmodbc:build

echo "###### Building the development container image ######"
docker build -t ${DEV_CONATINER_IMAGE_TAG} .

echo "###### Building the build/test container image ######"
docker build -t ${BUILD_CONTAINER_IMAGE_TAG} -f Dockerfile.build .
