#!/usr/bin/env bash
set -e

# This script runs the ODBC tests. It passes arguments to the Makefile to direct
# the tests to the hyper-q instance configured in the docker composition.

while (( "$#" )); do
  case "$1" in
    -h|--host)
      HOST=$2
      shift 2
      ;;
    -P|--port)
      PORT=$2
      shift 2
      ;;
    -u|--user|--username)
      USERNAME=$2
      shift 2
      ;;
    -p|--pass|--password)
      PASSWORD=$2
      shift 2
      ;;
    -d|--db|--database)
      DATABASE=$2
      shift 2
      ;;
    --ignore-failures)
      IGNORE_FAILURES=yes
      shift 1
      ;;
    -n|--testsname|--tests_name)
      TESTS_NAME=$2
      shift 2
      ;;
    --) # end argument parsing
      shift
      break
      ;;
    *) # preserve positional arguments
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
  esac
done

set -ex

echo "###### Install test dependencies ######"
yum install -y -q epel-release
yum install -y make gcc unixODBC-devel perl-Test-Harness libcmocka-devel

echo "###### Waiting for Hyper-Q to start ######"
wait-for-it.sh -h ${HOST} -p ${PORT} -t 120 -s -- echo "Hyper-Q available on port ${PORT}"

pushd /tmp/odbc/test
rm -f odbc.ini odbcinst.ini

export LD_LIBRARY_PATH="/usr/local/pgsql/lib"

set +e
make installcheck odbc_ini_extras="Port=${PORT} Username=${USERNAME} Password=${PASSWORD} Servername=${HOST} Database=${DATABASE} Driver='Datometry Unicode'"
exit_code=$?
if [ -f regression.diffs ]; then
  echo "### Found regression.diffs ###"
  cp regression.diffs /checkout
  cp -r results /checkout/test_results
  mkdir -p /checkout/test_logs
  cp /tmp/psqlodbc_*.log /checkout/test_logs/
  cp /tmp/mylog_*.log /checkout/test_logs/
fi
if [ ! -z ${TESTS_NAME} ]; then
  sed -i "s/<testsuite name=\"/<testsuite name=\"${TESTS_NAME}-/g" test_results_xml/*.xml
  mv test_results_xml test_results_xml-${TESTS_NAME}
  cp -r test_results_xml-${TESTS_NAME} /checkout
else
  cp -r test_results_xml /checkout
fi

# If IGNORE_FAILURES is empty, then the `--ignore-failures' option has not been set.
if [ -z ${IGNORE_FAILURES} ]; then
  exit ${exit_code}
else
  echo "!!!!!! Ignoring exit code of tests due to --ignore-failures option !!!!!!"
  exit 0
fi
