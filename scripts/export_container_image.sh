#!/usr/bin/env bash

# Export the "build" container image as a file so that is can be kept as an
# artifact. This is also how the container image is shipped from one build step
# to another.

set -e

if [ -z "${1}" ]; then
  echo "USAGE: ${0} <centos:6|centos:7>"
  exit 1
fi

PLATFORM=${1}

BUILD_CONTAINER_IMAGE_TAG=dtmodbc:build
CONTAINER_IMAGE_OUTFILE=dtmodbc_build.${PLATFORM}.container

echo "###### Exporting ${BUILD_CONTAINER_IMAGE_TAG} to ${CONTAINER_IMAGE_OUTFILE} ######"
docker image save ${BUILD_CONTAINER_IMAGE_TAG} | gzip > ${CONTAINER_IMAGE_OUTFILE}
