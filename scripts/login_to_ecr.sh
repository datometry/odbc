#!/usr/bin/env bash

# Login to AWS ECR Docker container registry using the awscli tool.

set -e

eval $(aws ecr get-login --no-include-email --region us-west-2)
