#!/bin/bash

if [ "$#" -lt 2 ]; then
  echo "Create metadata.json for files in version directory"
  echo "usage: ${0} <LocalPath> <ext> (<ext>)"
  exit 1
fi

regex="^.*/(.+)-(.+)/$"
[[ $(ls -d $1/*/) =~ $regex ]]
version=${BASH_REMATCH[1]}
build=${BASH_REMATCH[2]}
metadata_file="metadata.json"

printf '{\n'                   > $metadata_file
printf '    "artifacts": {\n' >> $metadata_file
for ((i=2; i<=$#; i++)); do
  file=$(ls $1/${version}-${build}/*.${!i})
  if [ -z "${file}" ]; then continue; fi
  printf '        "%s": {\n' ${!i/./_}                                                >> $metadata_file
  printf '            "filename": "%s",\n' $(basename ${file})                        >> $metadata_file
  printf '            "sha256": "%s"\n' $(shasum -a 256 ${file} | awk '{ print $1 }') >> $metadata_file
  if [ $i -eq $# ]; then
    printf '        }\n' >> $metadata_file
  else
    printf '        },\n' >> $metadata_file
  fi
done
printf '    },\n'                                                     >> $metadata_file
printf '    "build": "%s",\n' ${build}                                >> $metadata_file
printf '    "license": "",\n'                                         >> $metadata_file
printf '    "release_epoch": "%s",\n' $(date +%s)                     >> $metadata_file
printf '    "release_time": "%s",\n' $(date -u +"%Y-%m-%dT%H:%M:%SZ") >> $metadata_file
printf '    "version": "%s"\n' ${version}-${build}                    >> $metadata_file
printf '}\n'                                                          >> $metadata_file
