#!/bin/bash
set -euo pipefail

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
source ${SCRIPT_DIR}/common.sh

if [ "${BRANCH-}" == "master" ]; then
  SLACK_CHANNEL='#releases'
else
  SLACK_CHANNEL='#testing'
fi

generate_post_data() {
  cat <<EOF
{ "channel": "${SLACK_CHANNEL}",
  "username": "TeamCity",
  "text": "",
  "attachments": [{
    "mrkdwn_in": ["text"],
    "title": "Version ${VERSION}",
    "title_link": "https://downloads.datometry.com/odbc/linux/rhel/7/${VERSION}",
    "pretext": "A new version of the Datometry ODBC Linux driver has been released!",
    "fallback":"ODBC ${VERSION} released",
    "color":"good",
    "fields": [
      {
          "title": "Version",
          "value": "${VERSION}",
          "short": true
      },
      {
          "title": "Build #",
          "value": "${BUILD_NUMBER}",
          "short": true
      },
      {
          "title": "RHEL 7",
          "value": "https://downloads.datometry.com/odbc/linux/rhel/7/${VERSION}",
          "short": false
      }
  ]
  }]
}
EOF
}

set -e

curl -H 'Content-type: application/json' -d "$(generate_post_data)" ${SLACK_WEBHOOK_URL}
