#!/usr/bin/env bash
set -e

docker-compose run --rm dtmodbc bash /checkout/scripts/run_tests.sh --host hyperq --port 1025 --username gpadmin --password gpadmin --database gpadmin --tests_name postgres --ignore-failures
