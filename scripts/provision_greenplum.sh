#!/usr/bin/env bash
set -e

source scripts/login_to_ecr.sh
docker-compose pull --quiet postgres

WAIT_FOR_HOST=postgres WAIT_FOR_PORT=5432 docker-compose build hyperq

docker-compose up --no-build -d postgres hyperq
