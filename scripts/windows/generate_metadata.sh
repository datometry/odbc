#!/usr/bin/env bash

OUTPUT_FILE=${1}
VERSION=${2}
BUILD_ID=${3}
MSI_FILE=${4}

cat <<EOF > ${OUTPUT_FILE}
{
"artifacts": {
    "msi": {
        "filename": "$(basename ${MSI_FILE})",
        "sha256": "$(shasum -a 256 ${MSI_FILE} | awk '{ print $1 }')"
    }
},
"build": "${BUILD_ID}",
"license": "",
"release_epoch": "$(date +%s)",
"release_time": "$(date -u +"%Y-%m-%dT%H:%M:%SZ")",
"version": "${VERSION}.${BUILD_ID}"
}
EOF
