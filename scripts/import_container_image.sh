#!/usr/bin/env bash

# Import the "build" container image that was exported using `export_container_image.sh`.

set -e

if [ -z "${1}" ]; then
  echo "USAGE: ${0} <container_image_file>"
  exit 1
fi

FILE=${1}

BUILD_CONTAINER_IMAGE_TAG=dtmodbc:build

echo "###### Importing ${FILE} as ${BUILD_CONTAINER_IMAGE_TAG} ######"
cat "${FILE}" | gunzip | docker image load
