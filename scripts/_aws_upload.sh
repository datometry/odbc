#!/bin/bash

if [ "$#" -ne 2 ]; then
  echo "Recursive directory uplaod to AWS S3"
  echo "usage: ${0} <LocalPath> <S3Uri>"
  exit 1
fi

uplDir () {
  local ph=$1
  local s3ph=$2
  cd ${ph}
  for i in *; do
    if [ -f $i ]; then
      # echo "upl $i to ${s3ph}/"
      aws s3 cp $i ${s3ph}/ --content-disposition "attachment;filename=$i"
    elif [ -d $i ]; then
      uplDir $i ${s3ph}/$i
    fi
  done
  cd ..
}

uplDir $1 $2
