#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

source ${SCRIPT_DIR}/common.sh

# Create linux metadata
cd release/linux/rhel/7/${VERSION}
ZIP_FILE=$(ls *.zip)
TAR_GZ_FILE=$(ls *.tar.gz)
RPM_FILE=$(ls *.rpm)

cat <<EOF > metadata.json
{
"artifacts": {
    "zip": {
        "filename": "$(basename ${ZIP_FILE})",
        "sha256": "$(shasum -a 256 ${ZIP_FILE} | awk '{ print $1 }')"
    },
    "rpm": {
        "filename": "$(basename ${RPM_FILE})",
        "sha256": "$(shasum -a 256 ${RPM_FILE} | awk '{ print $1 }')"
    },
    "tar_gz": {
        "filename": "$(basename ${TAR_GZ_FILE})",
        "sha256": "$(shasum -a 256 ${TAR_GZ_FILE} | awk '{ print $1 }')"
    }
},
"build": "${BUILD_NUMBER}",
"license": "",
"release_epoch": "$(date +%s)",
"release_time": "$(date -u +"%Y-%m-%dT%H:%M:%SZ")",
"version": "${VERSION}"
}
EOF
cp metadata.json ../
