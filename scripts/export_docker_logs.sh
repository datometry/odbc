#!/usr/bin/env bash

# Export docker compose logs to a file so that the CI system can keep as an artifact.

docker-compose logs -t --no-color > docker-compose.log
