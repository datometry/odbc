#!/usr/bin/env bash
set -e

if [ "${BRANCH:=local}" == "master" ]; then
  PACKAGE_PREFIX="dtmodbc"
else
  PACKAGE_PREFIX="dtmodbc-${BRANCH}"
fi
PACKAGE_VERSION=$(cat VERSION)

RELEASE_DIR="release"
mkdir -p ${RELEASE_DIR}

echo "=== Building version: ${PACKAGE_VERSION}-${BUILD_NUMBER:=0} ==="

mkdir -p /tmp/dtmodbc
cd /tmp/dtmodbc
mkdir BUILD RPMS SOURCES SPECS SRPMS
rpmbuild -v -bb --clean \
  --define "odbc_lib_path /usr/local/lib" \
  --define "name ${PACKAGE_PREFIX}" \
  --define "version ${PACKAGE_VERSION}" \
  --define "release ${BUILD_NUMBER}" \
  /checkout/dtmodbc.spec
cp RPMS/x86_64/*.rpm /checkout/${RELEASE_DIR}

echo "${PACKAGE_VERSION}-${BUILD_NUMBER:=0}" > /checkout/${RELEASE_DIR}/ver.txt
