Repo taken from Postgresql ODBC 11.00.0000
Commit: e0e512f5c18750e747164d9110bcf6752d013126
tag: REL-11_00_0000
Date:   Sat Nov 17 22:18:33 2018 +0900
git: https://git.postgresql.org/gitweb/?p=psqlodbc.git
url: https://odbc.postgresql.org

To execute the test suite:
su - postgres
cd ODBC_DIR
./start-postgres.sh
logout
cd test
make installcheck

To sync this repo to the original psqlODBC handle a git remote.
'How' It is described in this link:
https://gist.github.com/sangeeths/9467061
