# This is a spec file for DTMODBC

%define _topdir               /tmp/dtmodbc

%define buildroot             %{_topdir}/%{name}-root

BuildRoot:      %{buildroot}
Summary:        Datometry ODBC Driver
License:        LGPLv2+
Name:           %{name}
Version:        %{version}
Release:        %{release}
Prefix:         /opt/datometry/%{name}-%{release}
Group:          Development/Tools

Requires: polytools-libs >= 2.4.0

%description
Datometry ODBC Driver

%install
mkdir -p $RPM_BUILD_ROOT/opt/datometry/%{name}-%{version}-%{release}
cp -r %{odbc_lib_path} $RPM_BUILD_ROOT/opt/datometry/%{name}-%{version}-%{release}
ln -sf /opt/datometry/%{name}-%{version}-%{release} ${RPM_BUILD_ROOT}/opt/datometry/dtmodbc
cp /checkout/scripts/odbcinst.ini ${RPM_BUILD_ROOT}/opt/datometry/%{name}-%{version}-%{release}/odbcinst.ini

%files
/opt/datometry/%{name}-%{version}-%{release}
/opt/datometry/dtmodbc
