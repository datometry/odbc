/*
 * This test is used to check the output and processing of the catalog
 * functions listed as below:
 * - SQLGetTypeInfo         HQ-6692
 * - SQLTables              HQ-6504
 * - SQLColumns             HQ-6505
 * - SQLSpecialColumns      HQ-6507
 * - SQLStatistics          HQ-6508
 * - SQLPrimaryKeys         HQ-6509
 * - SQLForeignKeys         HQ-6510
 * - SQLProcedureColumns    HQ-6512
 * - SQLTablePrivileges     HQ-6513
 * - SQLColumnPrivileges    HQ-6506
 * - SQLProcedures          HQ-6511
 * - SQLGetInfo             HQ-6514
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

int
main(int argc, char **argv)
{
	int			rc;
	HSTMT		hstmt = SQL_NULL_HSTMT;
	/* Cases where output is limited to relevant information only */
	SQLSMALLINT sql_tab_privileges_ids[6] = {1, 2, 3, 4, 6, 7};

	// sql_column_ids: No database name(1)
	SQLSMALLINT sql_column_ids[17] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18};

	// sql_table_ids: No database name(1), no remarks(5)
	SQLSMALLINT sql_table_ids[3] = {2, 3, 4};

	// sql_statistics_ids: No database name(1), no ordinal position(8)
	SQLSMALLINT sql_statistics_ids[11] = {2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13};

	SQLSMALLINT sql_pro_column_ids[] = {1, 2, 3, 4, 5, 6, 7};
	char		buf[1000];
	SQLSMALLINT answer;
	SQLSMALLINT	len;

	test_connect();

	rc = SQLAllocHandle(SQL_HANDLE_STMT, conn, &hstmt);
	if (!SQL_SUCCEEDED(rc))
	{
		print_diag("failed to allocate stmt handle", SQL_HANDLE_DBC, conn);
		exit(1);
	}


	rc = SQLExecDirect(hstmt, (SQLCHAR *) "drop index indexitirili on odbctest_tequila;", SQL_NTS);
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "drop index indexorobombon  on odbctest_tequila;", SQL_NTS);
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "drop table odbctest_types;", SQL_NTS);
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "drop table odbctest_tequila;", SQL_NTS);


	rc = SQLExecDirect(hstmt, (SQLCHAR *) "create table odbctest_types(c1 smallint, c2 integer, c3 bigint, c4 real, c5 float, c6 char, c7 varchar(100), c8 date format 'YYYY-MM-DD' NOT NULL DEFAULT DATE '2000-01-01', c9 time, c10 timestamp, c11 numeric(11,6), c12 varbyte(20), c13 integer not null default 11);", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "create multiset table odbctest_tequila(c1 smallint, c2 integer);", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	// ADW DDL cannot create unique index, that's why we don't check for errors.
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "Create unique Index indexitirili(c1) on odbctest_tequila;", SQL_NTS);
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "Create Index indexorobombon(c2) on odbctest_tequila;", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	/****************************/
	/* Check for SQLGetTypeInfo */
	/*         HQ-6692          */
	/****************************/
	printf("Check for SQLGetTypeInfo\n");
	rc = SQLGetTypeInfo(hstmt, SQL_VARCHAR);
	CHECK_STMT_RESULT(rc, "SQLGetTypeInfo failed", hstmt);
	print_result_meta(hstmt);
	print_result(hstmt);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/***********************/
	/* Check for SQLTables */
	/***********************/
	printf("Check for SQLTables\n");
	rc = SQLTables(hstmt,
					NULL, 0,
					(SQLCHAR *) SQL_ALL_SCHEMAS, SQL_NTS,
					(SQLCHAR *) "odbctest_%", SQL_NTS,
					(SQLCHAR *) "", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLTables failed", hstmt);
	print_result_meta(hstmt);
	print_result_series(hstmt, sql_table_ids, 3, -1);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	rc = SQLTables(hstmt,
					NULL, 0,
					(SQLCHAR *) SQL_ALL_SCHEMAS, SQL_NTS,
					(SQLCHAR *) "odbctest_%", SQL_NTS,
					(SQLCHAR *) "'TABLES'", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLTables failed", hstmt);
	print_result_meta(hstmt);
	print_result_series(hstmt, sql_table_ids, 3, -1);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);


	/************************/
	/* Check for SQLColumns */
	/************************/
	printf("Check for SQLColumns\n");
	rc = SQLColumns(hstmt,
					NULL, 0,
					NULL, 0,
					(SQLCHAR *) "odbctest_types", SQL_NTS,
					NULL, 0);
	CHECK_STMT_RESULT(rc, "SQLColumns failed", hstmt);
	print_result_meta(hstmt);
	/*
	 * Print only the 6 first columns, we do not want for example
	 * to get the OID in output, and this information looks to be
	 * enough.
	 */
	print_result_series(hstmt, sql_column_ids, 17, -1);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/*********************************/
	/* Check for SQLColumnPrivileges */
	/*            HQ-6506            */
	/*********************************/
	printf("Check for SQLColumnPrivileges\n");
	rc = SQLColumnPrivileges(hstmt,
							 NULL, 0,
							 (SQLCHAR *) "public", SQL_NTS,
							 (SQLCHAR *) "testtab1", SQL_NTS,
							 (SQLCHAR *) "id", SQL_NTS);
	CHECK_EXPECTED_STMT_RESULT(rc, "SQLColumnPrivileges failed", hstmt);
	// print_result_meta(hstmt);
	// print_result(hstmt);
	// rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	// CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/*******************************/
	/* Check for SQLSpecialColumns */
	/*           HQ-6507           */
	/*******************************/
	printf("Check for SQLSpecialColumns\n");
	rc = SQLSpecialColumns(hstmt, SQL_ROWVER,
						   NULL, 0,
						   (SQLCHAR *) "public", SQL_NTS,
						   (SQLCHAR *) "testtab1", SQL_NTS,
						   SQL_SCOPE_SESSION,
						   SQL_NO_NULLS);
	CHECK_STMT_RESULT(rc, "SQLSpecialColumns failed", hstmt);
	print_result_meta(hstmt);
	print_result(hstmt);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/*
	 * Check for SQLStatistics. It is important to note that this function
	 * returns statistics like the number of pages used and the number of
	 * index scans.
	 */
	/***************************/
	/* Check for SQLStatistics */
	/*         HQ-6508         */
	/***************************/
	printf("Check for SQLStatistics\n");
	rc = SQLStatistics(hstmt,
					   NULL, 0,
					   (SQLCHAR *) "", SQL_NTS,
					   (SQLCHAR *) "odbctest_tequila", SQL_NTS,
					   SQL_INDEX_ALL,
					   0);
	CHECK_STMT_RESULT(rc, "SQLStatistics failed", hstmt);
	print_result_meta(hstmt);
	print_result_series(hstmt, sql_statistics_ids, 11, -1);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/****************************/
	/* Check for SQLPrimaryKeys */
	/*          HQ-6509         */
	/****************************/
	printf("Check for SQLPrimaryKeys\n");
	rc = SQLPrimaryKeys(hstmt,
						NULL, 0,
						(SQLCHAR *) "public", SQL_NTS,
						(SQLCHAR *) "testtab1", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLPrimaryKeys failed", hstmt);
	print_result_meta(hstmt);
	print_result(hstmt);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/****************************/
	/* Check for SQLForeignKeys */
	/*         HQ-6510          */
	/****************************/
	printf("Check for SQLForeignKeys\n");
	rc = SQLForeignKeys(hstmt,
						NULL, 0,
						(SQLCHAR *) "public", SQL_NTS,
						(SQLCHAR *) "testtab1", SQL_NTS,
						NULL, 0,
						(SQLCHAR *) "public", SQL_NTS,
						(SQLCHAR *) "testtab_fk", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLForeignKeys failed", hstmt);
	print_result_meta(hstmt);
	print_result(hstmt);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/***************************/
	/* Check for SQLProcedures */
	/*         HQ-6511         */
	/***************************/
	printf("Check for SQLProcedures\n");
	rc = SQLProcedures(hstmt,
					   NULL, 0,
					   (SQLCHAR *) "public", SQL_NTS,
					   (SQLCHAR *) "simple_add", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLProcedures failed", hstmt);
	print_result_meta(hstmt);
	print_result(hstmt);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/*********************************/
	/* Check for SQLProcedureColumns */
	/*            HQ-6512            */
	/*********************************/
	printf("Check for SQLProcedureColumns\n");
	rc = SQLProcedureColumns(hstmt,
							 NULL, 0,
							 (SQLCHAR *) "public", SQL_NTS,
							 (SQLCHAR *) "simple_add", SQL_NTS,
							 NULL, 0);
	CHECK_STMT_RESULT(rc, "SQLProcedureColumns failed", hstmt);
	print_result_meta(hstmt);
	print_result(hstmt);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);
	// /*
	//  * Check for SQLProcedureColumns for a function
	//  * 	returning normal type
	//  */
	// rc = SQLProcedureColumns(hstmt, NULL, 0,
	// 			 NULL, 0,
	// 			 (SQLCHAR *) "set_byte", SQL_NTS,
	// 			 NULL, 0);
	// CHECK_STMT_RESULT(rc, "SQLProcedureColumns failed", hstmt);
	// print_result_series(hstmt, sql_pro_column_ids, sizeof(sql_pro_column_ids) / sizeof(sql_pro_column_ids[0]), -1);
	// rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	// CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);
	// /*
	//  * Check for SQLProcedureColumns for a function
	//  * 	returning composite type
	//  */
	// rc = SQLProcedureColumns(hstmt, NULL, 0,
	// 			 (SQLCHAR *) "public", SQL_NTS,
	// 			 (SQLCHAR *) "getfoo", SQL_NTS,
	// 			 NULL, 0);
	// CHECK_STMT_RESULT(rc, "SQLProcedureColumns failed", hstmt);
	// print_result_series(hstmt, sql_pro_column_ids, sizeof(sql_pro_column_ids) / sizeof(sql_pro_column_ids[0]), -1);
	// rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	// CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	//  * Check for SQLProcedureColumns for a function
	//  * 	returning setof composite type

	// rc = SQLProcedureColumns(hstmt, NULL, 0,
	// 			 (SQLCHAR *) "public", SQL_NTS,
	// 			 (SQLCHAR *) "getboo", SQL_NTS,
	// 			 NULL, 0);
	// CHECK_STMT_RESULT(rc, "SQLProcedureColumns failed", hstmt);
	// print_result_series(hstmt, sql_pro_column_ids, sizeof(sql_pro_column_ids) / sizeof(sql_pro_column_ids[0]), -1);
	// rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	// CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);
	// /*
	//  * Check for SQLProcedureColumns for a function
	//  * 	returning table
	//  */
	// rc = SQLProcedureColumns(hstmt, NULL, 0,
	// 			 (SQLCHAR *) "public", SQL_NTS,
	// 			 (SQLCHAR *) "tbl_arg", SQL_NTS,
	// 			 NULL, 0);
	// CHECK_STMT_RESULT(rc, "SQLProcedureColumns failed", hstmt);
	// print_result_series(hstmt, sql_pro_column_ids, sizeof(sql_pro_column_ids) / sizeof(sql_pro_column_ids[0]), -1);
	// rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	// CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);
	// /*
	//  * Check for SQLProcedureColumns for a function with OUT
	//  * 	parameters returning setof
	//  *	This is equivalent to tbl_arg.
	//  */
	// rc = SQLProcedureColumns(hstmt, NULL, 0,
	// 			 (SQLCHAR *) "public", SQL_NTS,
	// 			 (SQLCHAR *) "set_of", SQL_NTS,
	// 			 NULL, 0);
	// CHECK_STMT_RESULT(rc, "SQLProcedureColumns failed", hstmt);
	// print_result_series(hstmt, sql_pro_column_ids, sizeof(sql_pro_column_ids) / sizeof(sql_pro_column_ids[0]), -1);
	// rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	// CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/********************************/
	/* Check for SQLTablePrivileges */
	/*           HQ-6513            */
	/********************************/
	printf("Check for SQLTablePrivileges\n");
	rc = SQLTablePrivileges(hstmt,
							NULL, 0,
							(SQLCHAR *) "public", 0,
							(SQLCHAR *) "testtab1", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLTablePrivileges failed", hstmt);
	print_result_meta(hstmt);
	print_result_series(hstmt, sql_tab_privileges_ids, 6, 5);
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/*******************/
	/* Test SQLGetInfo */
	/*     HQ-6514     */
	/*******************/
	printf("Check for SQLGetInfo\n");
	rc = SQLGetInfo(conn, SQL_TABLE_TERM, buf, sizeof(buf), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("Term for \"table\": %s\n", buf);

	rc = SQLGetInfo(conn, SQL_DBMS_NAME, buf, sizeof(buf), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("DBMS name: %s\n", buf);

	rc = SQLGetInfo(conn, SQL_IDENTIFIER_QUOTE_CHAR, buf, sizeof(buf), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("Identifier char: %s\n", buf);

	rc = SQLGetInfo(conn, SQL_SEARCH_PATTERN_ESCAPE, buf, sizeof(buf), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("Search pattern escape: %s\n", buf);

	rc = SQLGetInfo(conn, SQL_LIKE_ESCAPE_CLAUSE, buf, sizeof(buf), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("Like escape clause: %s\n", buf);

	rc = SQLGetInfo(conn, SQL_IDENTIFIER_CASE, &answer, sizeof(answer), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("Are identifiers case sensitive?: %d\n", answer);

	rc = SQLGetInfo(conn, SQL_QUOTED_IDENTIFIER_CASE, &answer, sizeof(answer), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("Are \"quoted\" identifiers case-sensitive?: %d\n", answer);

	rc = SQLGetInfo(conn, SQL_MAX_CATALOG_NAME_LEN, &answer, sizeof(answer), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("SQL_MAX_CATALOG_NAME_LEN: %d\n", answer);

	rc = SQLGetInfo(conn, SQL_MAX_SCHEMA_NAME_LEN, &answer, sizeof(answer), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("SQL_MAX_SCHEMA_NAME_LEN: %d\n", answer);

	rc = SQLGetInfo(conn, SQL_MAX_TABLE_NAME_LEN, &answer, sizeof(answer), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("SQL_MAX_TABLE_NAME_LEN: %d\n", answer);

	rc = SQLGetInfo(conn, SQL_MAX_COLUMN_NAME_LEN, &answer, sizeof(answer), &len);
	CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
	printf("SQL_MAX_COLUMN_NAME_LEN: %d\n", answer);

	/****
	 * Misc extra tests.
	 */

	/*
	 * Older versions of the driver had a bug in handling table-types lists
	 * longer than 32 entries. Check for that.
	 */
	/***********************/
	/* Check for SQLTables */
	/***********************/
	printf("Check for extra SQLTables test\n");
	rc = SQLTables(hstmt, "", SQL_NTS,
				   NULL, 0,
				   "odbctest_%", SQL_NTS,
				   "1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,TABLE", SQL_NTS);

	CHECK_STMT_RESULT(rc, "SQLTables failed", hstmt);
	print_result_series(hstmt, sql_table_ids, 3, -1);

	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/* Clean up */
	test_disconnect();

	return 0;
}
