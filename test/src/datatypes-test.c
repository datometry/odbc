#include <stdio.h>
#include <stdlib.h>

#include "common.h"

int main(int argc, char **argv)
{
    SQLRETURN rc;
    HSTMT hstmt = SQL_NULL_HSTMT;
    char param1[20] = "123456789012345678";
    SQLLEN cbParam1;
    SQLSMALLINT colcount;
    SQLSMALLINT dataType;
    SQLULEN paramSize;
    SQLSMALLINT decDigits;
    SQLSMALLINT nullable;
    SQLUSMALLINT supported;

    test_connect();

    rc = SQLAllocHandle(SQL_HANDLE_STMT, conn, &hstmt);
    if (!SQL_SUCCEEDED(rc))
    {
        print_diag("failed to allocate stmt handle", SQL_HANDLE_DBC, conn);
        exit(1);
    }

    /* SQL_LONGVARCHAR test */
    
    /* Prepare a statement */
    rc = SQLPrepare(hstmt, (SQLCHAR *) "SELECT ?", SQL_NTS);
    CHECK_STMT_RESULT(rc, "SQLPrepare failed", hstmt);

    /* bind param  */
    cbParam1 = SQL_NTS;
    rc = SQLBindParameter(hstmt, 1, SQL_PARAM_INPUT,
                          SQL_C_CHAR,   /* value type */
                          SQL_LONGVARCHAR, /* param type */
                          20,           /* column size */
                          0,            /* dec digits */
                          param1,       /* param value ptr */
                          0,            /* buffer len */
                          &cbParam1     /* StrLen_or_IndPtr */);
    CHECK_STMT_RESULT(rc, "SQLBindParameter failed", hstmt);

    /* Execute */
    rc = SQLExecute(hstmt);
    CHECK_STMT_RESULT(rc, "SQLExecute failed", hstmt);

    /* Fetch result */
    print_result(hstmt);

    rc = SQLFreeStmt(hstmt, SQL_CLOSE);
    CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

    /* Clean up */
    test_disconnect();

    return 0;
}
