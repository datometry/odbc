/*
 * Tests for the existing behaviors of rollback on errors:
 * 0 -> Do nothing and let the application do it
 * 1 -> Rollback the entire transaction
 * 2 -> Rollback only the statement
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

HSTMT hstmt = SQL_NULL_HSTMT;

static void
error_rollback_init(char *options)
{
	SQLRETURN rc;

	/* Error if initialization is already done */
	if (hstmt != SQL_NULL_HSTMT)
	{
		printf("Initialization already done, leaving...\n");
		exit(1);
	}

	test_connect_ext(options);
	rc = SQLAllocStmt(conn, &hstmt);
	if (!SQL_SUCCEEDED(rc))
	{
		print_diag("failed to allocate stmt handle", SQL_HANDLE_DBC, conn);
		exit(1);
	}

	// Since the volatile table could not be created, we have to be sure
	// that the table does not exist when the test starts. HQ-6739
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "DROP TABLE errortab", SQL_NTS);

	/* Create a table to use */
	// HQ-6739: Original test included a volatile table.
	//          It was changed to normal table because errors.
	rc = SQLExecDirect(hstmt,
			   (SQLCHAR *) "CREATE TABLE errortab (i integer)",
			   SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	// The original version of the test was creating the table within a transaction
	// But ADW can't handle a table creation like that.
	// Hyperq has to adapt this at some point.
	// HQ-6788

	/* Disable autocommit */
	rc = SQLSetConnectAttr(conn,
						   SQL_ATTR_AUTOCOMMIT,
						   (SQLPOINTER)SQL_AUTOCOMMIT_OFF,
						   SQL_IS_UINTEGER);
}

static void
error_rollback_clean(void)
{
	SQLRETURN rc;

	/* Clean up everything */
	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);
	test_disconnect();
	hstmt = SQL_NULL_HSTMT;
}

static void
error_rollback_exec_success(int arg)
{
	SQLRETURN rc;
	char buf[100];

	printf("Executing query that will succeed\n");

	/* Now execute the query */
	snprintf(buf, sizeof(buf), "INSERT INTO errortab VALUES (%d)", arg);
	rc = SQLExecDirect(hstmt, (SQLCHAR *) buf, SQL_NTS);

	/* Print error if any, but do not exit */
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);
}

static void
error_rollback_exec_begin_transaction()
{
	SQLRETURN rc;
	char buf[100];

	printf("Requesting a query with BEGIN TRANSACTION\n");

	/* Now execute the query */
	snprintf(buf, sizeof(buf), "BEGIN TRANSACTION");
	rc = SQLExecDirect(hstmt, (SQLCHAR *) buf, SQL_NTS);

	/* Print error if any */
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);
}

/* Runs a query that's expected to fail */
static void
error_rollback_exec_failure(int arg)
{
	SQLRETURN rc;
	char buf[100];

	printf("Executing query that will fail\n");

	snprintf(buf, sizeof(buf), "INSERT INTO errortab VALUES ('fail%d')", arg);

	/* Now execute the query */
	rc = SQLExecDirect(hstmt, (SQLCHAR *) buf, SQL_NTS);
	if (SQL_SUCCEEDED(rc))
	{
		printf("SQLExecDirect should have failed but it succeeded\n");
		exit(1);
	}

	/* Print error, it is expected */
	print_diag("Failed to execute statement", SQL_HANDLE_STMT, hstmt);
}

void
error_rollback_print(void)
{
	SQLRETURN rc;

	/* Create a table to use */
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "SELECT i FROM errortab order by i", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	/* Show results */
	print_result(hstmt);
}

int
main(int argc, char **argv)
{
	SQLRETURN rc;

	/*
	 * The Protocol connection parameter won't affect the behavior of dtmODBC driver.
	 * It will always roll back the entire transaction on an error.
	 * Just as hyperq behaves.
	 */
	printf("Test for rollback protocol 0\n");
	error_rollback_init("Protocol=7.4-0");

	/* Insert a row correctly */
	error_rollback_exec_success(1);

	/* Now trigger an error, the row previously inserted will disappear */
	error_rollback_exec_failure(1);

	/*
	 * Now rollback the transaction block, it is the responsibility of
	 * application.
	 */
	printf("Rolling back with SQLEndTran\n");
	rc = SQLEndTran(SQL_HANDLE_DBC, conn, SQL_ROLLBACK);
	CHECK_STMT_RESULT(rc, "SQLEndTran failed", hstmt);

	/* Insert row correctly now */
	error_rollback_exec_success(1);

	/* Not yet committed... */
	rc = SQLEndTran(SQL_HANDLE_DBC, conn, SQL_COMMIT);
	CHECK_STMT_RESULT(rc, "SQLEndTran failed", hstmt);

	/* Print result */
	error_rollback_print();

	/* Clean up */
	error_rollback_clean();

	/*
	 * Test 2: rollback protocol 1
	 * In case of an error rollback the entire transaction.
	 */
	printf("Test for rollback protocol 1\n");
	error_rollback_init("Protocol=7.4-1");

	/*
	 * Insert a row, trigger an error, and re-insert a row. Only one
	 * row should be visible here.
	 */
	error_rollback_exec_success(1);
	error_rollback_exec_failure(1);
	error_rollback_exec_success(1);
	error_rollback_print();

	/* Clean up */
	error_rollback_clean();

	/*
	 * Test 3: rollback protocol 2
	 * Not anymore - transaction level rollback.
	 * All transaction should rollback on an error.
	 */
	printf("Test for rollback protocol 2\n");
	error_rollback_init("Protocol=7.4-2");

	/*
	 * Do a bunch of insertions and failures.
	 */
	error_rollback_exec_success(1);
	error_rollback_exec_success(2);
	rc = SQLEndTran(SQL_HANDLE_DBC, conn, SQL_COMMIT);
	CHECK_STMT_RESULT(rc, "SQLEndTran failed", hstmt);
	printf("Commit!\n");
	error_rollback_exec_success(3);
	error_rollback_exec_success(4);
	error_rollback_exec_success(5);
	error_rollback_exec_failure(-1);
	error_rollback_exec_success(6);
	error_rollback_exec_success(7);

	error_rollback_print();

	/* Clean up */
	error_rollback_clean();

	/*
	* Test 4: A TD begin transaction command should not be duplicated
	*/
	printf("Test preventing duplicated Teradata begin command\n");
	error_rollback_init("");

	/* If the begin transaction command was not detected, we'll get into 
	 * level 2 nested transaction state. We should be only in level 1
	 */
	error_rollback_exec_begin_transaction();
	error_rollback_exec_success(42);

	printf("Commit!\n");
	rc = SQLEndTran(SQL_HANDLE_DBC, conn, SQL_COMMIT);
	CHECK_STMT_RESULT(rc, "SQLEndTran failed", hstmt);

	/* If the begin transaction command was not detected, we would be rolling
	 * back all commands
	 */
	printf("Rollback!\n");
	rc = SQLEndTran(SQL_HANDLE_DBC, conn, SQL_ROLLBACK);
	CHECK_STMT_RESULT(rc, "SQLEndTran failed", hstmt);

	error_rollback_print();

	/* Clean up */
	error_rollback_clean();

	return 0;
}
