#include <stdio.h>
#include <stdlib.h>

#include "common.h"

int main(int argc, char **argv)
{
	SQLRETURN rc;
	HSTMT hstmt = SQL_NULL_HSTMT;
	SQLLEN rowcount;

	test_connect();

	rc = SQLAllocHandle(SQL_HANDLE_STMT, conn, &hstmt);
	if (!SQL_SUCCEEDED(rc))
	{
		print_diag("failed to allocate stmt handle", SQL_HANDLE_DBC, conn);
		exit(1);
	}

	/* Create a table to test with */
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "CREATE TABLE testtbl(t varchar(40));", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	/**** A simple query against the table, fetch column info ****/

	rc = SQLExecDirect(hstmt, "SELECT * FROM testtbl", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	/* Get column metadata */
	print_result_meta(hstmt);

	rc = SQLFreeStmt(hstmt, SQL_CLOSE);
	CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

	/*****************/
	/* LOCKING TABLE */
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "locking testtbl for write select 1 from testtbl;", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	printf("locking!\n");

	rc = SQLRowCount(hstmt, &rowcount);
	CHECK_STMT_RESULT(rc, "SQLRowCount failed", hstmt);

	printf("# of rows: %d\n", (int) rowcount);


	/**************/
	/* LOCK TABLE */
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "lock table testtbl for write;", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	printf("lock!\n");

	/* Test SQLRowCount() */
	rc = SQLRowCount(hstmt, &rowcount);
	CHECK_STMT_RESULT(rc, "SQLRowCount failed", hstmt);

	printf("# of rows: %d\n", (int) rowcount);

	/*******************/
	/* ALTER THE TABLE */
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "ALTER TABLE testtbl add c varchar(10);", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	printf("Table altered!\n");

	/* Test SQLRowCount() */
	rc = SQLRowCount(hstmt, &rowcount);
	CHECK_STMT_RESULT(rc, "SQLRowCount failed", hstmt);

	printf("# of rows: %d\n", (int) rowcount);


	/* Run the query again, check if the metadata was updated */
	rc = SQLExecDirect(hstmt, "SELECT * FROM testtbl", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	/* Get column metadata */
	print_result_meta(hstmt);

	/* Destroy the table */
	rc = SQLExecDirect(hstmt, (SQLCHAR *) "DROP TABLE testtbl;", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	/* Clean up */
	test_disconnect();

	return 0;
}
