#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

int main(int argc, char **argv)
{
    int rc;
    HSTMT hstmt = SQL_NULL_HSTMT;

    test_connect();

    rc = SQLAllocHandle(SQL_HANDLE_STMT, conn, &hstmt);
    if (!SQL_SUCCEEDED(rc))
    {
        print_diag("failed to allocate stmt handle", SQL_HANDLE_DBC, conn);
        exit(1);
    }

    rc = SQLExecDirect(hstmt, (SQLCHAR *) "SELECT avg(id) from byteatab where t like 'foo%';", SQL_NTS);
    CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);
    print_result(hstmt);

    rc = SQLFreeStmt(hstmt, SQL_CLOSE);
    CHECK_STMT_RESULT(rc, "SQLFreeStmt failed", hstmt);

    /* Clean up */
    test_disconnect();

    return 0;
}
