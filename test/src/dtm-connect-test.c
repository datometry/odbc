/*
 * Test functions related to establishing a connection.
 */
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

static void
test_datometry_dbms()
{
    int         rc;
    char        buf[1000];
    SQLSMALLINT len;
    HSTMT       hstmt = SQL_NULL_HSTMT;

    test_connect_ext("Driver=Datometry Unicode");

    rc = SQLAllocHandle(SQL_HANDLE_STMT, conn, &hstmt);
    if (!SQL_SUCCEEDED(rc))
    {
        print_diag("failed to allocate stmt handle", SQL_HANDLE_DBC, conn);
        exit(1);
    }

    printf("Check for SQLGetInfo\n");
    rc = SQLGetInfo(conn, SQL_DBMS_NAME, buf, sizeof(buf), &len);
    CHECK_STMT_RESULT(rc, "SQLGetInfo failed", hstmt);
    printf("DBMS name: %s\n", buf);
}

int main(int argc, char **argv)
{
    /* the common test_connect() function uses SQLDriverConnect */
    test_connect_ext("Driver=Datometry Unicode");
    test_disconnect();

    test_datometry_dbms();
    test_disconnect();

    return 0;
}
