#include <stdio.h>
#include <stdlib.h>

#include "common.h"

int main(int argc, char **argv)
{
	SQLRETURN rc;
	HSTMT hstmt = SQL_NULL_HSTMT;
	const char * createProcedureQuery= "\
		REPLACE PROCEDURE CheckParams (IN input_param VARCHAR(50), INOUT inout_param VARCHAR(50), OUT out_param VARCHAR(50)) \
		SQL SECURITY INVOKER  \
		BEGIN  \
			SET out_param=inout_param; \
			SET inout_param=input_param; \
		END;";
	char *param1, *param2, param3[50];
	SQLLEN cbParam1, cbParam2, cbParam3;

	test_connect();

	rc = SQLAllocHandle(SQL_HANDLE_STMT, conn, &hstmt);
	if (!SQL_SUCCEEDED(rc))
	{
		print_diag("failed to allocate stmt handle", SQL_HANDLE_DBC, conn);
		exit(1);
	}

	/* Create a procedure with output parameters */
	rc = SQLExecDirect(hstmt, (SQLCHAR *) createProcedureQuery, SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLExecDirect failed", hstmt);

	// /**** Call the procedure ****/

	rc = SQLPrepare(hstmt, (SQLCHAR *) "CALL CheckParams (? ,?, ?);", SQL_NTS);
	CHECK_STMT_RESULT(rc, "SQLPrepare failed", hstmt);

	/* bind params  */
	param1 = "Hello";
	param2 = "World";
	cbParam1 = cbParam2 = cbParam3 = SQL_NTS;
	rc = SQLBindParameter(hstmt, 1, SQL_PARAM_INPUT,
						  SQL_C_CHAR,	/* value type */
						  SQL_VARCHAR,	/* param type */
						  50,/* column size */
						  0,			/* dec digits */
						  param1,		/* param value ptr */
						  0,			/* buffer len */
						  &cbParam1		/* StrLen_or_IndPtr */);
	CHECK_STMT_RESULT(rc, "SQLBindParameter failed", hstmt);
	rc = SQLBindParameter(hstmt, 2, SQL_PARAM_INPUT_OUTPUT,
						  SQL_C_CHAR,	/* value type */
						  SQL_VARCHAR,	/* param type */
						  50,/* column size */
						  0,			/* dec digits */
						  param2,		/* param value ptr */
						  0,			/* buffer len */
						  &cbParam2		/* StrLen_or_IndPtr */);
	CHECK_STMT_RESULT(rc, "SQLBindParameter failed", hstmt);
	rc = SQLBindParameter(hstmt, 3, SQL_PARAM_OUTPUT,
						  SQL_C_CHAR,	/* value type */
						  SQL_VARCHAR,	/* param type */
						  50,/* column size */
						  0,			/* dec digits */
						  param3,		/* param value ptr */
						  0,			/* buffer len */
						  &cbParam3		/* StrLen_or_IndPtr */);
	CHECK_STMT_RESULT(rc, "SQLBindParameter failed", hstmt);

	/* Execute */
	rc = SQLExecute(hstmt);
	CHECK_STMT_RESULT(rc, "SQLExecute failed", hstmt);

	/* Print meta and results */
	print_result_meta(hstmt);
	print_result(hstmt);

	/* Clean up */
	test_disconnect();

	return 0;
}
