/*
 * Test functions related to establishing a connection.
 */
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

static void
test_SQLConnect()
{
	SQLRETURN ret;
	SQLCHAR *dsn = (SQLCHAR *) get_test_dsn();

	SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);

	SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (void *) SQL_OV_ODBC3, 0);

	SQLAllocHandle(SQL_HANDLE_DBC, env, &conn);

	printf("Connecting with SQLConnect...");

	ret = SQLConnect(conn, dsn, SQL_NTS, NULL, 0, NULL, 0);
	if (SQL_SUCCEEDED(ret)) {
		printf("connected\n");
	} else {
		print_diag("SQLConnect failed.", SQL_HANDLE_DBC, conn);
		return;
	}
}

/*
 * Test that attributes can be set *before* establishing a connection. (We
 * used to have a bug where it got reset when the per-DSN options were read.)
 */
static void
test_setting_attribute_before_connect()
{
	SQLRETURN	ret;
	SQLCHAR		str[1024];
	SQLSMALLINT strl;
	SQLCHAR		dsn[1024];
	SQLULEN		value;

	snprintf(dsn, sizeof(dsn), "DSN=%s", get_test_dsn());

	SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);
	SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (void *) SQL_OV_ODBC3, 0);

	SQLAllocHandle(SQL_HANDLE_DBC, env, &conn);

	printf("Testing that autocommit persists SQLDriverConnect...\n");

	/* Disable autocommit */
	SQLSetConnectAttr(conn,
					  SQL_ATTR_AUTOCOMMIT,
					  (SQLPOINTER) SQL_AUTOCOMMIT_OFF,
					  0);

	/* Connect */
	ret = SQLDriverConnect(conn, NULL, dsn, SQL_NTS,
						   str, sizeof(str), &strl,
						   SQL_DRIVER_COMPLETE);
	if (SQL_SUCCEEDED(ret)) {
		printf("connected\n");
	} else {
		print_diag("SQLDriverConnect failed.", SQL_HANDLE_DBC, conn);
		return;
	}

	/*** Test that SQLGetConnectAttr says that it's still disabled. ****/
	value = 0;
	ret = SQLGetConnectAttr(conn,
							SQL_ATTR_AUTOCOMMIT,
							&value,
							0, /* BufferLength, ignored for an int attribute */
							NULL);
	CHECK_CONN_RESULT(ret, "SQLGetConnectAttr failed", conn);

	if (value == SQL_AUTOCOMMIT_ON)
		printf("autocommit is on (should've been off!)\n");
	else if (value == SQL_AUTOCOMMIT_OFF)
		printf("autocommit is still off (correct).\n");
	else
		printf("unexpected autocommit value: %lu\n", (unsigned long) value);


	printf("Turning autocommit back on\n");
	SQLSetConnectAttr(conn,
					  SQL_ATTR_AUTOCOMMIT,
					  (SQLPOINTER) SQL_AUTOCOMMIT_ON,
					  0);

	value = 0;
	ret = SQLGetConnectAttr(conn,
							SQL_ATTR_AUTOCOMMIT,
							&value,
							0, /* BufferLength, ignored for an int attribute */
							NULL);
	CHECK_CONN_RESULT(ret, "SQLGetConnectAttr failed", conn);

	if (value == SQL_AUTOCOMMIT_ON)
		printf("autocommit is now on (correct).\n");
	else if (value == SQL_AUTOCOMMIT_OFF)
		printf("autocommit is off (should've been on!)\n");
	else
		printf("unexpected autocommit value: %lu\n", (unsigned long) value);

	/*
	 * Altough autocommit can be off. The feature is not currently working.
	 * Further commit/rollback tests are disabled
	 */
	test_disconnect();
}

int main(int argc, char **argv)
{
	/* the common test_connect() function uses SQLDriverConnect */
	test_connect();
	test_disconnect();

	test_SQLConnect();
	test_disconnect();

	test_setting_attribute_before_connect();

	return 0;
}
